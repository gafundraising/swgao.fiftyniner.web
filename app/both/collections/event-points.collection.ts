import {IEventPoints} from "../models/program-info.model";
export const EventPointsCollection = new Mongo.Collection<IEventPoints>("event_points");

if (Meteor.isServer) {
    EventPointsCollection.rawCollection().createIndex({userId:1});
    EventPointsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    EventPointsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

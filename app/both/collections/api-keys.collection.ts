import {IAPIKey} from "../models/api-key.model";
export const APIKeysCollection = new Mongo.Collection<IAPIKey>('api_keys');

if (Meteor.isServer) {
    APIKeysCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    APIKeysCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

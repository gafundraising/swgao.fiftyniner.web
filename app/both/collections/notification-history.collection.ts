import {IPushNotification} from "../models/push-notification.model";
export const NotificationHistoryCollection = new Mongo.Collection<IPushNotification>('notification_history');

if (Meteor.isServer) {
    NotificationHistoryCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    NotificationHistoryCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

import {IMyListContact} from "../models/my-list-contact.model";
export const MyListContactsCollection = new Mongo.Collection<IMyListContact>("my_contacts");

if (Meteor.isServer) {
    MyListContactsCollection.rawCollection().createIndex({userId: 1});
    MyListContactsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    MyListContactsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

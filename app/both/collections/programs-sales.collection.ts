import {IProgramsSales} from "../models/programs-sales.model";
export const ProgramsSalesCollection = new Mongo.Collection<IProgramsSales>("programs_sales");

if (Meteor.isServer) {
    ProgramsSalesCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    ProgramsSalesCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

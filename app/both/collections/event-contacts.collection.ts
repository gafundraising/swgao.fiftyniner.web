import {IEventContact} from "../models/event-contact.model";
export const EventContactsCollection = new Mongo.Collection<IEventContact>("event_contacts");

if (Meteor.isServer) {
    EventContactsCollection.rawCollection().createIndex({programId:1});
    EventContactsCollection.rawCollection().createIndex({userId:1, programId:1});
    EventContactsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    EventContactsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

import {IParticipantTotals} from "../models/participant-totals.model";
export const ParticipantTotalsCollection = new Mongo.Collection<IParticipantTotals>("participant_totals");

if (Meteor.isServer) {
    ParticipantTotalsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    ParticipantTotalsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

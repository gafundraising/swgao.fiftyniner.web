import {IProgramGroup} from "../models/program-group.model";
export const ProgramGroupsCollection = new Mongo.Collection<IProgramGroup>("program_groups");

if (Meteor.isServer) {
    ProgramGroupsCollection.rawCollection().createIndex({programId:1});
    ProgramGroupsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    ProgramGroupsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

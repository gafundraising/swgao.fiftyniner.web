import {IProgramScripts} from "../models/program-info.model";
export const MyScriptsCollection = new Mongo.Collection<IProgramScripts>("my_scripts");

if (Meteor.isServer) {
    MyScriptsCollection.rawCollection().createIndex({userId:1});
    MyScriptsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    MyScriptsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

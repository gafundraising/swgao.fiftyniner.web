import {IEventTotals} from "../models/event-totals.model";
export const EventTotalsCollection = new Mongo.Collection<IEventTotals>("event_totals");

if (Meteor.isServer) {
    EventTotalsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    EventTotalsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}

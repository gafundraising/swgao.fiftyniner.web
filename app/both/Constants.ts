export class Constants {
    public static EMPTY_STRING = "";

    public static SESSION: any = {
        LANGUAGE: "language",
        LOADING: "isLoading",
        PLATFORM_READY: "platformReady",
        TRANSLATIONS_READY: "translationsReady",
        PATH: "path",
        URL_PARAMS: "urlParams",
        INCORRECT_PASSWORD: "incorrectPassword",
        FORGOT_PASSWORD: "forgotPassword",
        CREATE_ACCOUNT: "createAccount",
        RESET_PASSWORD: "resetPassword",
        REGISTERED_ERROR: "registeredError",
        NOT_REGISTERED_ERROR: "notRegisteredError",
        RESET_PASSWORD_ERROR: "resetPasswordError",
        RESET_PASSWORD_ERROR_MESSAGE: "resetPasswordErrorMessage",
        RESET_PASSWORD_TOKEN: "resetPasswordToken",
        WAS_PASSWORD_RESET: "wasPasswordReset",
        EMAIL: "email",
        MOBILE_PHONE: "mobilePhone",
        PROGRAM_ID: "programId",
        INVALID_KICKOFF_CODE: "invalidKickoffCode",
        SHOW_LEADERBOARD_MENU_ITEM: "showLeaderboardMenuItem",
        SORT_PROGRAMS_DESCENDING: "sortProgramsDescending",
        SORT_CATEGORY: "sortCategory",
        SELECTED_FILTER: "selectedFilter"
    };

    public static DEVICE: any = {
        IOS: "iOS",
        ANDROID: "Android"
    };

    public static STYLE: any = {
        IOS: "ios",
        MD: "md"
    };

    public static METEOR_ERRORS: any = {
        SIGN_IN: "sign-in",
        ACCOUNT_NOT_FOUND: "account-not-found",
        NO_PASSWORD: "User has no password set",
        USER_NOT_FOUND: "User not found",
        INCORRECT_PASSWORD: "Incorrect password",
        ALREADY_EXISTS: 'already-exists',
        EMAIL_EXISTS: "Email already exists.",
        USERNAME_EXISTS: "Username already exists.",
        TOKEN_EXPIRED: "Token expired",
        FINGERPRINT_NOT_ENABLED: "fingerprint-not-enabled",
        ACCESS_DENIED: "access-denied",
        INVALID_KICKOFF_CODE: "invalid-kickoff-code",
        INVALID_PARAMS: "invalid-params",
        TIMEDOUT: "ETIMEDOUT"
    };

    public static ADD_IMAGE_PLACEHOLDER_URI: string = "/images/add_image_camera_photo.png";
    public static GAO_LOGO: string = "/images/gao_logo.png";
    public static APP_ICON: string = "/images/blitz_icon.png";
    public static APP_LOGO: string = "/images/blitz_logo.png";
    public static IMAGE_URI_PREFIX: string = "data:image/jpeg;base64,";

    public static PUBLICATIONS: any = {
        MANAGED_PROGRAMS: "ManagedPrograms",
        MANAGERS: "Managers",
        EVENT_TOTALS: "EventTotals",
        PROGRAM_PARTICIPANTS: "ProgramParticipants",
        PARTICIPANT_TOTALS: "ParticipantTotals",
        PARTICIPANT_EVENT_CONTACTS: "ParticipantEventContacts",
        MY_LIST_CONTACTS: "MyListContacts",
        SALES_REPS: "SalesReps",
        EVENT_POINTS: "EventPoints",
        MY_SCRIPTS: "MyScripts",
        PROGRAMS_SALES: "ProgramsSales",
        PROGRAMS_COMMITTED_SALES: "ProgramsCommittedSales",
        PROGRAM_GROUPS: "ProgramGroups"
    };

    public static ROLES: any = {
        ADMIN: "administrator",
        MEMBER: "member",
        PROGRAM_MANAGEMENT: "program-management"
    };

    public static EVENT_POINTS: any = {
        CALL: "call",
        TEXT: "text",
        EMAIL: "email",
        PAID: "paid",
        COMMITMENT: "commitment",
        CONTACT_LIST_GOAL: "contactListGoal",
        CONTACT_GOAL: "contactGoal",
        REGISTRATION: "registration",
        SALES_GOAL: "salesGoalAward"
    };

    public static SCRIPT_KEYS: any = {
        CALL_TO_ACTION: "callToAction",
        CALL: "call",
        TEXT: "text",
        EMAIL: "email",
        ORDER_CONFIRMATION: "orderConfirmation"
    };

    public static EVENT_CONTACT_STATUS: any = {
        CALLED: "Called",
        LEFT_MESSAGE: "Left Voice Message",
        SENT_TEXT: "Sent Text Message",
        DECLINED: "Declined Commitment",
        RECEIVED_COMMITMENT: "Received Commitment",
        PAID: "Paid"
    };

    public static ADMIN_USERNAME: string = "fiftynineradmin";

    public static ROUTES: any = {
        RESET_PASSWORD: "resetPassword",
        LEADERBOARD: "leaderboard"
    };

    public static COMPETITION_TYPE: any = {
        INDIVIDUAL: "individual",
        GROUP: "group"
    };

    public static ENVIRONMENT: any = {
        DEVELOPMENT: "DEVELOPMENT",
        TEST: "TEST",
        PRODUCTION: "PRODUCTION"
    };

    public static PUSH_NOTIFICATION_ACTIONS: any = {
        MESSAGE: "message",
        AWARD: "award"
    };

    public static PUSH_NOTIFICATION_TYPE: any = {
        PROGRAM: "program",
        TEAM: "team",
        PARTICIPANT: "participant"
    };

    public static AWARD_KEYS: any = {
        REGISTRATION: "registration",
        CONTACT_LIST: "contactList",
        COMMUNICATION: "communication",
        SALES: "sales"
    };

    public static DIVISION_CODE: any = {
        GA: "20",
        GS: "21",
        QSPCA: "40",
        BDCCA: "41",
        BDCUS: "42"
    };

    public static EMAIL_STRINGS: any = {
        from: {
            ga: " <school-sale@gafundraising.com>",
            qsp: " <customerservice@qsp.ca>"
        },
        subject: {
            ga: " Great American ",
            qsp: " QSP "
        },
        customerService: {
            ga: "<div style='margin: 10px 0'><a href='tel:18002511542'>1-800-251-1542</a></div>" +
            "<div style='margin: 10px 0'><a href='mailto:greatamerican@gafundraising.com'>greatamerican@gafundraising.com</a></div>",
            qsp: "<div style='margin: 10px 0'><a href='tel:18006672536'>1-800-667-2536</a></div>" +
            "<div style='margin: 10px 0'><a href='mailto:customerservice@qsp.ca'>customerservice@qsp.ca</a></div>"
        }
    }
}
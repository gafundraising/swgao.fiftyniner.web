import {Constants} from "./Constants";
import {IProgramInfo} from "./models/program-info.model";

export class Utils {
    public static removeNonDigits(phoneNumber:string):string {
        var regex = /^\d*$/;
        var value:string = phoneNumber;
        if (value && !regex.test(value)) {
            var result = value.replace(/[^0-9]/g, Constants.EMPTY_STRING);
            phoneNumber = result;
        }
        return phoneNumber;
    }

    public static capitalizeFirstLetter(string):string {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    public static generatePasswordResetTokenForUser(user:Meteor.User):string {
        var token = Random.secret();
        var when = new Date();
        var tokenRecord = {
            token: token,
            email: user.emails[0].address,
            when: when,
            reason: 'reset'
        };
        Meteor.users.update(user._id, {
            $set: {
                "services.password.reset": tokenRecord
            }
        });
        (Meteor as any)._ensure(user, 'services', 'password').reset = tokenRecord;
        console.log("token: " + token);
        return token;
    }

    public static getWelcomeEmailHtml(data:{
        user:Meteor.User, 
        token?:string,
        program:IProgramInfo,
        salesRep:Meteor.User
    }):string {
        let isEnrolled:boolean = (data.token ? false : true);

        var href:string = Meteor.absoluteUrl();

        if (data.token) {
            href += Constants.ROUTES.RESET_PASSWORD + "?token=" + data.token;
        }

        var userLang = "en";
        var emailHtml = Constants.EMPTY_STRING;
        var buttonStyle:string = "display: inline-block; padding: 0px 30px; margin: 20px auto; font-size: 30px; font-weight:bold; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; line-height: 1.5; text-align: center; white-space: pre-wrap; vertical-align: middle; -ms-touch-action: manipulation; touch-action: manipulation; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; background-image: none; border: 1px solid transparent; border-radius: 4px; color: #fff; background-color: #337ab7; border-color: #2e6da4; text-decoration: none;";

        var customerServiceString = Constants.EMAIL_STRINGS.customerService.ga;
        if (data.program.contract.divisionCode === Constants.DIVISION_CODE.QSPCA) {
            customerServiceString = Constants.EMAIL_STRINGS.customerService.qsp;
        }

        if (userLang === "en") {
            emailHtml = "<div style='margin: 16px auto; width: 200px;'>" +
                "<a href='#'><img style='width: 200px;' src='" + "https://gametime.gafundraising.com/" + "images/blitz_logo_200h.png'/></a>" +
                "</div>" +
                "<div style='padding: 16px;'>" +
                "<h2>Dear " + data.user.profile.name.given + ", </h2>" +
                "<h3 style='margin: 10px 0px;'>" +
                "<strong>Congratulations</strong>, a <strong>" + Meteor.settings.public["appName"] + "</strong> program has been created for " + data.program.name + ", and is now ready for you to complete your setup." +
                "</h3><br>" +
                "<h4>To complete your setup, please do the following:</h4>" +
                "<ul>" +
                (isEnrolled ? "<h4><li>Sign in with the password you previously set</li></h4>" : "<h4><li>Set your password</li></h4>") +
                "<h4><li>Add an image for your group</li></h4>" +
                "<h4><li>Confirm the date for your event</li></h4>" +
                "<h4><li>Confirm the point values for the actions</li></h4>" +
                "</ul>" +
                "<h3>Click the button below to navigate to the <strong>" + Meteor.settings.public["appName"] + "</strong> website and finish your setup.</h3>" +
                "<a href='" + href + "' style='" + buttonStyle + "'>" +
                "<div>It's <strong>" + Meteor.settings.public["appName"] + "</strong>, Let's Go!</div>" +
                "</a><br><br><br>" +
                "<br><br>" +
                "<div style='display: table-row; padding: 20px'>" +
                "<div style='width: 96px; height: 96px; display: table-cell;'>" +
                "<a href='#'><img style='width: 96px; height: 96px;' src='" + "https://gametime.gafundraising.com/" + "images/blitz_icon_96.png'/></a>" +
                "</div>" +
                "<h1 style='display: table-cell; vertical-align: middle; padding: 16px;'>" + Meteor.settings.public["appName"] + " Mobile App</h1>" +
                "</div>" +
                "<br><div><a href='https://mobile.gametime.gafundraising.com' style='" + buttonStyle + "'>Download</a></div>" +
                "<h5>Your program's participant registration code is: </h5>" +
                "<h3>" + data.program.contract.contractNumber + "</h3>" +
                "<br><p>If you have any questions, please don’t hesitate to contact your sales representative:</p>" +
                "<p>" +
                "<h4><strong>" + data.salesRep.profile.name.display + "</strong></h4>" +
                "<div><a href='mailto:" + data.salesRep.emails[0].address + "'>" + data.salesRep.emails[0].address + "</a></div>" +
                "<br></p>" +
                "<div>or contact our awesome customer service team!</div>" +
                "<p>" + customerServiceString +
                "</p><br><br>" +
                "</div>";
        }

        return emailHtml;
    }
}
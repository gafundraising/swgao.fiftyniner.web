import {Constants} from "../Constants";
export interface IProgramGroup {
    _id?:string,
    programId?:string,
    name?:string,
    imageUri?:string,
    members?:Array<Meteor.User>
}

export class ProgramGroup implements IProgramGroup {
    public _id:string;
    public programId:string;
    public name:string = Constants.EMPTY_STRING;
    public imageUri:string;
    public members:Array<Meteor.User>;
    
    constructor(group?:IProgramGroup) {
        if (group) {
            this._id = group._id;
            this.programId = group.programId;
            this.name = group.name;
            this.imageUri = group.imageUri;
            if (group.hasOwnProperty("members") && Array.isArray(group.members)) {
                let members:Array<Meteor.User> = [];
                group.members.forEach((user:Meteor.User) => {
                    members.push(user);
                });
                this.members = members;
            }
        }
    }
}
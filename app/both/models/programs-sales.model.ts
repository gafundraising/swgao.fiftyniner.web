export interface IProgramsSales {
    _id?:string,
    commitmentSales?:number,
    purchasedSales?:number
}
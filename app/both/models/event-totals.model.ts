export interface IEventTotals {
    _id?:string,
    participants?:number,
    contacts?:number,
    commitments?:number,
    commitmentSales?:number,
    purchases?:number,
    purchasedSales?:number
    commitmentProfit?:number,
    purchasedProfit?:number
}

export class EventTotals implements IEventTotals {
    public _id:string;
    public participants:number = 0;
    public contacts:number = 0;
    public commitments:number = 0;
    public commitmentSales:number = 0;
    public purchases:number = 0;
    public purchasedSales:number = 0;
    public commitmentProfit:number = 0;
    public purchasedProfit:number = 0;

    constructor(eventTotalsJson?:IEventTotals) {
        if (eventTotalsJson) {
            this._id = eventTotalsJson._id;
            if (eventTotalsJson.hasOwnProperty("participants")) {
                this.participants = eventTotalsJson.participants;
            }
            if (eventTotalsJson.hasOwnProperty("contacts")) {
                this.contacts = eventTotalsJson.contacts;
            }
            if (eventTotalsJson.hasOwnProperty("commitments")) {
                this.commitments = eventTotalsJson.commitments;
            }
            if (eventTotalsJson.hasOwnProperty("commitmentSales")) {
                this.commitmentSales = eventTotalsJson.commitmentSales;
            }
            if (eventTotalsJson.hasOwnProperty("purchases")) {
                this.purchases = eventTotalsJson.purchases;
            }
            if (eventTotalsJson.hasOwnProperty("purchasedSales")) {
                this.purchasedSales = eventTotalsJson.purchasedSales;
            }
            if (eventTotalsJson.hasOwnProperty("commitmentProfit")) {
                this.commitmentProfit = eventTotalsJson.commitmentProfit;
            }
            if (eventTotalsJson.hasOwnProperty("purchasedProfit")) {
                this.purchasedProfit = eventTotalsJson.purchasedProfit;
            }
        }
    }
}
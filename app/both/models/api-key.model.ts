export interface IAPIKey {
    _id?:string,
    owner?:string,
    key?:string
}

export class APIKey implements IAPIKey {
    public _id:string;
    public owner:string;
    public key:string;
    
    constructor(apiKeyJson?:IAPIKey) {
        if (apiKeyJson) {
            this._id = apiKeyJson._id;
            this.owner = apiKeyJson.owner;
            this.key = apiKeyJson.key;
        }
    }
}
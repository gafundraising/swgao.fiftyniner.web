export interface IPushNotification {
    _id?:string,
    from?:string,
    userIds?:Array<string>,
    action?:string,
    title?:string,
    message?:string,
    sentAt?:string,
    receivedAt?:Array<{userId:string, time:string}>,
    payload?:any
}

export interface IProfile {
    name?: string;
    salesRepId?:string;
    onlineIds?:any;
    programs?:any;
    groups?:any;
    picture?: {_id:string, url:string};
}
import {Constants} from "../Constants";
import * as moment from 'moment';

export interface IMyListContact {
    _id?:string,
    userId?:string,
    name?:{
        given?:string,
        family?:string,
        display?:string
    },
    phone?:string,
    email?:string,
    created?:string,
    hidden?:boolean
}

export class MyListContact implements IMyListContact {
    public _id:string;
    public userId:string;
    public name:{
        given?:string,
        family?:string,
        display?:string
    } = {
        given: Constants.EMPTY_STRING,
        family: Constants.EMPTY_STRING,
        display: Constants.EMPTY_STRING
    };
    public phone:string = Constants.EMPTY_STRING;
    public email:string = Constants.EMPTY_STRING;
    public created:string = moment().toISOString();
    public hidden:boolean;

    constructor(myListContactJson?:IMyListContact) {
        if (myListContactJson) {
            this._id = myListContactJson._id;
            this.userId = myListContactJson.userId;
            if (myListContactJson.hasOwnProperty("name")) {
                this.name.given = myListContactJson.name.given;
                this.name.family = myListContactJson.name.family;
                if (!this.name.given) {
                    this.name.given = Constants.EMPTY_STRING;
                }
                if (!this.name.family) {
                    this.name.family = Constants.EMPTY_STRING;
                }
                this.name.display = this.name.given + " " + this.name.family;
            }
            if (myListContactJson.hasOwnProperty("phone")) {
                this.phone = myListContactJson.phone;
            }
            if (myListContactJson.hasOwnProperty("email")) {
                this.email = myListContactJson.email;
            }
            if (myListContactJson.hasOwnProperty("created")) {
                this.created = myListContactJson.created;
            }
            if (myListContactJson.hasOwnProperty("hidden")) {
                this.hidden = myListContactJson.hidden;
            }
        }
    }
}
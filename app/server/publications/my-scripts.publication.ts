import {Constants} from "../../both/Constants";
import {MyScriptsCollection} from "../../both/collections/my-scripts.collection";

Meteor.publish(Constants.PUBLICATIONS.MY_SCRIPTS, function () {
    return MyScriptsCollection.find({userId: this.userId});
});
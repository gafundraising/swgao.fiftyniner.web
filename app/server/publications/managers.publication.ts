import {Constants} from "../../both/Constants";
Meteor.publish(Constants.PUBLICATIONS.MANAGERS, function (programId:string) {
    let queryKey:string = "roles." + programId;
    let query:any = {};
    query[queryKey] = {
        $in: [Constants.ROLES.ADMIN, Constants.ROLES.PROGRAM_MANAGEMENT]
    };
    return Meteor.users.find(query, {fields: {profile: 1, emails: 1, roles: 1}});
});
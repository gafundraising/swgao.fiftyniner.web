import {Constants} from "../../both/Constants";
import {EventContactsCollection} from "../../both/collections/event-contacts.collection";
declare var ReactiveAggregate;

Meteor.publish(Constants.PUBLICATIONS.EVENT_TOTALS, function (programId:string) {
    ReactiveAggregate(this, EventContactsCollection, [{
        $match: {
            programId: programId
        }
    }, {
        $sort: {
            updated: 1
        }
    }, {
        $project: {
            userId: 1,
            programId: 1,
            eventPoints: 1,
            commitment: 1,
            price: 1,
            purchased: 1,
            updated: 1,
            purchasedSales: {
                $sum: {
                    $map: {
                        input: "$purchases",
                        as: "purchase",
                        in: {
                            $sum: [{
                                $ifNull: ["$$purchase.price", 0]
                            }]
                        }
                    }
                }
            }
        }
    }, {
        $group: {
            _id: {
                userId: "$userId",
                programId: "$programId"
            },
            contacts: {
                $sum: 1
            },
            commitments: {
                $sum: "$commitment"
            },
            commitmentSales: {
                $sum: {
                    $multiply: ["$commitment", "$price"]
                }
            },
            totalPurchases: {
                $sum: "$purchased"
            },
            purchasedSales: {
                $sum: "$purchasedSales"
            }
        }
    }, {
        $group: {
            _id: "$_id.programId",
            participants: {
                $sum: 1
            },
            contacts: {
                $sum: "$contacts"
            },
            commitments: {
                $sum: "$commitments"
            },
            commitmentSales: {
                $sum: "$commitmentSales"
            },
            totalPurchases: {
                $sum: "$totalPurchases"
            },
            purchasedSales: {
                $sum: "$purchasedSales"
            }
        }
    }, {
        $project: {
            participants: "$participants",
            contacts: "$contacts",
            commitments: "$commitments",
            commitmentSales: "$commitmentSales",
            purchases: "$totalPurchases",
            purchasedSales: "$purchasedSales"
        }
    }], {
        // and send the results to another collection called below
        clientCollection: "event_totals"
    });
});
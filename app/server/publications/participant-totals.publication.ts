import {Constants} from "../../both/Constants";
import {EventContactsCollection} from "../../both/collections/event-contacts.collection";
declare var ReactiveAggregate;

Meteor.publish(Constants.PUBLICATIONS.PARTICIPANT_TOTALS, function (programId:string) {
    ReactiveAggregate(this, EventContactsCollection, [{
        $match: {
            programId: programId
        }
    }, {
        $sort: {
            updated: 1
        }
    }, {
        $project: {
            userId: 1,
            programId: 1,
            eventPoints: 1,
            commitment: 1,
            price: 1,
            purchased: 1,
            updated: 1,
            purchasedSales: {
                $sum: {
                    $map: {
                        input: "$purchases",
                        as: "purchase",
                        in: {
                            $sum: [{
                                $ifNull: ["$$purchase.price", 0]
                            }]
                        }
                    }
                }
            }
        }
    }, {
        $group: {
            _id: "$userId",
            programId: {$first: "$programId"},
            callPoints: {
                $sum: "$eventPoints.call"
            },
            textPoints: {
                $sum: "$eventPoints.text"
            },
            paidPoints: {
                $sum: "$eventPoints.paid"
            },
            commitmentPoints: {
                $sum: "$eventPoints.commitment"
            },
            contacts: {
                $sum: 1
            },
            commitments: {
                $sum: "$commitment"
            },
            commitmentSales: {
                $sum: {
                    $multiply: ["$commitment", "$price"]
                }
            },
            totalPurchases: {
                $sum: "$purchased"
            },
            purchasedSales: {
                $sum: "$purchasedSales"
            },
            lastUpdated: {
                $last: "$updated"
            }
        }
    }, {
        $project: {
            programId: "$programId",
            points: {$add: ["$callPoints", "$textPoints", "$paidPoints", "$commitmentPoints"]},
            contacts: "$contacts",
            commitments: "$commitments",
            commitmentSales: "$commitmentSales",
            purchases: "$totalPurchases",
            purchasedSales: "$purchasedSales",
            lastUpdated: "$lastUpdated"
        }
    }], {
        // and send the results to another collection called below
        clientCollection: "participant_totals"
    });
});
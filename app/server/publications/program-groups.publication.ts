import {Constants} from "../../both/Constants";
import {ProgramGroupsCollection} from "../../both/collections/program-groups.collection";
Meteor.publish(Constants.PUBLICATIONS.PROGRAM_GROUPS, function (programId:string) {
    return ProgramGroupsCollection.find({programId:programId});
});
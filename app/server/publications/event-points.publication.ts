import {Constants} from "../../both/Constants";
import {EventPointsCollection} from "../../both/collections/event-points.collection";

Meteor.publish(Constants.PUBLICATIONS.EVENT_POINTS, function () {
    return EventPointsCollection.find({userId: this.userId});
});
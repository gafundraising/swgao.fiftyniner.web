import {Constants} from "../../both/Constants";
import {ProgramsCollection} from "../../both/collections/programs.collection";

declare var Roles;

Meteor.publish(Constants.PUBLICATIONS.MANAGED_PROGRAMS, function () {
    var user:Meteor.User = Meteor.users.findOne({_id: this.userId});
    var query:any = {
        $or: [{
            "contract.cancelDate": {
                $exists: false
            }
        }, {
            "contract.cancelDate": {
                $eq: Constants.EMPTY_STRING
            }
        }]
    };

    if (user && user.username !== Constants.ADMIN_USERNAME) {
        var managedProgramIds:Array<string> = [];
        if (user && user["roles"]) {
            var programIds:Array<string> = Object.keys(user["roles"]) || [];
            programIds.forEach((programId:string) => {
                if (Roles.userIsInRole(user._id,
                        [Constants.ROLES.ADMIN, Constants.ROLES.PROGRAM_MANAGEMENT], programId)) {
                    managedProgramIds.push(programId);
                }
            });
        }
        query["_id"] = {$in: managedProgramIds};
    }

    return ProgramsCollection.find(query);
});
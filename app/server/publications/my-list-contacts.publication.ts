import {Constants} from "../../both/Constants";
import {MyListContactsCollection} from "../../both/collections/my-list-contacts";

Meteor.publish(Constants.PUBLICATIONS.MY_LIST_CONTACTS, function (programId:string) {
    let queryKey:string = "roles." + programId;
    let query:any = {};
    query[queryKey] = {
        $in: [Constants.ROLES.MEMBER]
    };
    let programParticipants:Array<Meteor.User> = Meteor.users.find(query, {
        fields: {
            _id: 1
        }
    }).fetch();
    let participantIds:Array<string> = programParticipants.map((participant:Meteor.User) => {
        return participant._id;
    });
    return MyListContactsCollection.find({userId: {$in: participantIds}});
});
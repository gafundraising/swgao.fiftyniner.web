import {Constants} from "../../both/Constants";
import {EventContactsCollection} from "../../both/collections/event-contacts.collection";
declare var ReactiveAggregate;

Meteor.publish(Constants.PUBLICATIONS.PROGRAMS_SALES, function (managedProgramIds:Array<string>) {
    let pipeline:Array<any> = [{
        $project: {
            userId: 1,
            programId: 1,
            commitment: 1,
            price: 1,
            purchasedSales: {
                $sum: {
                    $map: {
                        input: "$purchases",
                        as: "purchase",
                        in: {
                            $sum: [{
                                $ifNull: ["$$purchase.price", 0]
                            }]
                        }
                    }
                }
            }
        }
    }, {
        $group: {
            _id: {
                userId: "$userId",
                programId: "$programId"
            },
            commitmentSales: {
                $sum: {
                    $multiply: ["$commitment", "$price"]
                }
            },
            purchasedSales: {
                $sum: "$purchasedSales"
            }
        }
    }, {
        $group: {
            _id: "$_id.programId",
            commitmentSales: {
                $sum: "$commitmentSales"
            },
            purchasedSales: {
                $sum: "$purchasedSales"
            }
        }
    }, {
        $project: {
            commitmentSales: "$commitmentSales",
            purchasedSales: "$purchasedSales"
        }
    }];
    if (managedProgramIds && managedProgramIds.length > 0) {
        let match = {
            $match: {
                programId: {
                    $in: managedProgramIds
                }
            }
        };
        pipeline.unshift(match);
    }
    ReactiveAggregate(this, EventContactsCollection, pipeline, {
        // and send the results to another collection called below
        clientCollection: "programs_sales"
    });
});
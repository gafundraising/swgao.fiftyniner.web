import {Constants} from "../../both/Constants";
import {EventContactsCollection} from "../../both/collections/event-contacts.collection";
Meteor.publish(Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS, function (data:{
    participantId:string,
    programId:string
}) {
    return EventContactsCollection.find({userId: data.participantId, programId: data.programId});
});
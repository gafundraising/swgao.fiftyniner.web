import {Constants} from "../../both/Constants";
Meteor.publish(Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS, function (programId:string) {
    let queryKey:string = "roles." + programId;
    let query:any = {};
    query[queryKey] = {
        $in: [Constants.ROLES.MEMBER]
    };
    return Meteor.users.find(query, {
        fields: {
            profile: 1,
            emails: 1,
            roles: 1
        }
    });
});
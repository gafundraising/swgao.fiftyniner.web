import {Constants} from "../../both/Constants";
Meteor.publish(Constants.PUBLICATIONS.SALES_REPS, function () {
    var user:Meteor.User = Meteor.users.findOne({_id: this.userId});
    if (user && user.username === Constants.ADMIN_USERNAME) {
        return Meteor.users.find({
            "profile.salesRepId": {$exists: true, $ne: Constants.EMPTY_STRING}
        }, {
            fields: {emails: 1, profile: 1}
        });
    }
});
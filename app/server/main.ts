import { Main } from "./imports/server-main/main";
import './imports/server-main/routes.ts';

const mainInstance = new Main();
mainInstance.start();

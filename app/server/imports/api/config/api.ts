import {APIKeysCollection} from "../../../../both/collections/api-keys.collection";
import {IAPIKey} from "../../../../both/models/api-key.model";
import {ProgramInfo} from "../../../../both/models/program-info.model";
import {IGAOContract, IGAOPartner} from "../../../../both/models/gao-contract";
import {IPurchase} from "../../../../both/models/event-contact.model";

export class API {
    private static SCHEME_ERRORS:any = {
        STRING: "Invalid type. Must be a String.",
        NUMBER: "Invalid type. Must be a Number.",
        MISSING: "Missing value."
    };
    private resources:any = {
        contract: {
            push: true
        },
        payment: {
            push: true
        }
    };

    constructor(private params:any, private request:any, private response:any) {
    }

    public processRequest():void {
        this.response.setHeader('Access-Control-Allow-Origin', '*');

        if (this.request.method === "OPTIONS") {
            this.response.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            this.response.setHeader('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS');
            this.response.end('Set OPTIONS.');
        } else {
            this.handleRequest(this.params.resource, this.params.action, this.request.method);
        }
    }

    private handleRequest(resource, action, method):void {
        if (!this.resources[resource] || (this.resources[resource] && !this.resources[resource][action])) {
            return this.utility.response(this, 404, {message: "Resource not found"});
        }

        let connection:IConnection = this.connection();
        if (!connection.error) {
            this.methods[resource][action][method](this, connection);
        } else {
            this.utility.response(this, 401, connection);
        }
    }

    private connection():IConnection {
        let requestContents:any = this.utility.getRequestContents(this.request);
        if (this.request.method === "GET") {
            return {error: 401, message: "This application does not accept GET requests."};
        }

        let apiKey = requestContents.api_key;
        let apiKeyOwner:string = this.authentication(apiKey);

        if (apiKeyOwner) {
            // API Key no longer needed, remove from data and return
            delete requestContents.api_key;
            return {owner: apiKeyOwner, data: requestContents};
        } else {
            return {error: 401, message: "Invalid API key."};
        }
    }

    private authentication(key:string):string {
        let owner:string;
        let apiKey:IAPIKey = APIKeysCollection.findOne({"key": key}, {fields: {"owner": 1}});
        if (apiKey) {
            owner = apiKey.owner;
        }
        return owner;
    }

    private utility:any = {
        getRequestContents: function (request) {
            switch (request.method) {
                case "GET":
                    return request.query;
                case "POST":
                case "PUT":
                case "DELETE":
                    return request.body;
            }
        },
        response: function (context:any, statusCode:number, data:IConnection) {
            context.response.setHeader('Content-Type', 'application/json');
            context.response.statusCode = statusCode;
            context.response.end(JSON.stringify(data));
        },
        hasData: function (data) {
            return Object.keys(data).length > 0;
        },
        validate: function (data, pattern) {
            return Match.test(data, pattern);
        }
    };

    private methods:any = {
        'contract': {
            'push': {
                POST: function (context, connection) {
                    let data:IGAOContract = connection.data;
                    let hasData = context.utility.hasData(data);
                    let validData = context.validateContractData(context, data, true);

                    if (hasData && validData) {
                        console.log("Contract data valid!");
                        // context.utility.response(context, 200, {
                        //     success: true,
                        //     message: "Contract data valid!"
                        // });
                        Meteor.call('upsertContract', connection.data, (error, program:ProgramInfo) => {
                            if (error) {
                                console.error("Error loading contract: ", error);
                                context.utility.response(context, 500, {
                                    error: error,
                                    message: "Error loading contract."
                                });
                            } else {
                                context.utility.response(context, 200, {
                                    success: true,
                                    message: "Successfully loaded contract.",
                                    programId: program._id
                                });
                            }
                        });
                    } else {
                        let invalidData:any;
                        let message:string;
                        if (!hasData) {
                            message = "No contract data found";
                        } else if (!validData) {
                            console.log("Invalid data scheme!");
                            message = "Invalid data scheme";
                            invalidData = context.getInvalidContractData(context, data, true);
                        }
                        context.utility.response(context, 422, {
                            error: 422,
                            message: message,
                            invalidData: invalidData
                        });
                    }
                },
                PUT: function (context, connection) {
                    let data:IGAOContract = connection.data;
                    let hasData = context.utility.hasData(data);
                    let validData = context.validateContractData(context, data, false);

                    if (hasData && validData) {
                        // console.log("Contract data valid!");
                        // context.utility.response(context, 200, {
                        //     success: true,
                        //     message: "Contract data valid!"
                        // });
                        Meteor.call('updateContract', connection.data, (error, programId:string) => {
                            if (error) {
                                console.error("Error updating contract: ", error);
                                context.utility.response(context, 500, {
                                    error: error,
                                    message: "Error updating contract."
                                });
                            } else {
                                context.utility.response(context, 200, {
                                    success: true,
                                    message: "Successfully updated contract.",
                                    programId: programId
                                });
                            }
                        });
                    } else {
                        let invalidData:any;
                        let message:string;
                        if (!hasData) {
                            message = "No contract data found";
                        } else if (!validData) {
                            console.log("Invalid data scheme!");
                            message = "Invalid data scheme";
                            invalidData = context.getInvalidContractData(context, data, false);
                        }
                        context.utility.response(context, 422, {
                            error: 422,
                            message: message,
                            invalidData: invalidData
                        });
                    }
                }
            }
        },
        'payment': {
            'push': {
                POST: function (context, connection) {
                    let data:any = connection.data;
                    let hasData = context.utility.hasData(connection.data);
                    let validData = context.utility.validate(connection.data, {
                        gtid: String,
                        purchases: Array
                    });

                    if (validData) {
                        validData = Match.Where(context.validatePurchases(context, data.purchases)).condition;
                    }

                    if (hasData && validData) {
                        // context.utility.response(context, 200, {
                        //     success: true,
                        //     message: "Valid data scheme!"
                        // });
                        Meteor.call('receiveEventContactPayment', connection.data, (error, result) => {
                            if (error) {
                                console.error("Error receiving payment notification: ", error);
                                context.utility.response(context, 500, {
                                    error: error,
                                    message: "Error saving payment notification."
                                });
                            } else {
                                context.utility.response(context, 200, {
                                    success: true,
                                    message: "Successfully received payment notification."
                                });
                            }
                        });
                    } else {
                        let invalidData:any;
                        let message:string;
                        if (!hasData) {
                            message = "No payment data found";
                        } else if (!validData) {
                            console.log("Invalid data scheme!");
                            message = "Invalid data scheme";
                            invalidData = context.getInvalidPaymentData(data, context);
                        }
                        context.utility.response(context, 422, {
                            error: 422,
                            message: message,
                            invalidData: invalidData
                        });
                    }
                }
            }
        }
    };

    private getContractSchemes(isRequired:boolean):any {
        let groupScheme:any = {
            number: (isRequired ? Number : Match.Optional(Number)),
            name: (isRequired ? String : Match.Optional(String))
        };
        let partnerScheme:any = {
            number: (isRequired ? Number : Match.Optional(Number)),
            name: (isRequired ? String : Match.Optional(String)),
            phone: Match.Optional(String),
            email: Match.Optional(String)
        };
        let productScheme:any = {
            label: (isRequired ? String : Match.Optional(String)),
            imageUrl: (isRequired ? String : Match.Optional(String)),
            price: (isRequired ? Number : Match.Optional(Number))
        };
        let contractScheme:any = {
            contractNumber: Number,
            contractId: Match.Optional(Number),
            shopUrl: (isRequired ? String : Match.Optional(String)),
            startDate: (isRequired ? String : Match.Optional(String)),
            cancelDate: Match.Optional(String),
            group: (isRequired ? groupScheme : Match.Optional(groupScheme)),
            salesRep: (isRequired ? partnerScheme : Match.Optional(partnerScheme)),
            sponsor: (isRequired ? partnerScheme : Match.Optional(partnerScheme)),
            product: (isRequired ? productScheme : Match.Optional(productScheme)),
            divisionCode: Match.Optional(String),
            languageCode: Match.Optional(String),
            profitPercent: Match.Optional(Match.Where((value) => {
                check(value, Number);
                return value >= 0 && value <= 1;
            }))
        };
        return {
            group: groupScheme,
            partner: partnerScheme,
            product: productScheme,
            contract: contractScheme
        };
    };

    private validateContractData(context:any, data:any, isRequired:boolean):boolean {
        let contractSchemes:any = context.getContractSchemes(isRequired);
        return context.utility.validate(data, contractSchemes.contract);
    }

    private validatePartnerScheme(context:any, partner:IGAOPartner, isRequired:boolean):any {
        let valid:boolean = true;
        let invalidData:any;
        let pattern:any;
        let isFieldValid:boolean;
        let contractSchemes:any = context.getContractSchemes(isRequired);

        pattern = contractSchemes.contract.salesRep;
        isFieldValid = context.utility.validate(partner, pattern);
        if (partner && !isFieldValid) {
            console.log("Invalid partner");
            valid = false;
            let message:string;
            invalidData = {};
            if (partner) {
                pattern = contractSchemes.partner.number;
                isFieldValid = context.utility.validate(partner.number, pattern);
                if (!isFieldValid) {
                    console.log("Invalid partner number");
                    if (!partner.number) {
                        message = API.SCHEME_ERRORS.MISSING;
                    } else {
                        message = API.SCHEME_ERRORS.NUMBER;
                    }
                    invalidData.number = message;
                }

                pattern = contractSchemes.partner.name;
                isFieldValid = context.utility.validate(partner.name, pattern);
                if (!isFieldValid) {
                    console.log("Invalid partner name");
                    if (!partner.name) {
                        message = API.SCHEME_ERRORS.MISSING;
                    } else {
                        message = API.SCHEME_ERRORS.STRING;
                    }
                    invalidData.name = message;
                }

                pattern = contractSchemes.partner.phone;
                isFieldValid = context.utility.validate(partner.phone, pattern);
                if (!isFieldValid) {
                    console.log("Invalid partner phone");
                    if (!partner.phone) {
                        message = API.SCHEME_ERRORS.MISSING;
                    } else {
                        message = API.SCHEME_ERRORS.STRING;
                    }
                    invalidData.phone = message;
                }

                pattern = contractSchemes.partner.email;
                isFieldValid = context.utility.validate(partner.email, pattern);
                if (!isFieldValid) {
                    console.log("Invalid partner email");
                    if (!partner.email) {
                        message = API.SCHEME_ERRORS.MISSING;
                    } else {
                        message = API.SCHEME_ERRORS.STRING;
                    }
                    invalidData.email = message;
                }
            }
        }
        return {
            valid: valid,
            invalidData: invalidData
        };
    }

    private getInvalidContractData(context:any, data:any, isRequired:boolean):any {
        let allContractSchemeFieldsValid:boolean = true;

        let invalidData:any = {};
        let invalidMessage:string;

        let contractSchemes:any = context.getContractSchemes(isRequired);
        let pattern:any;
        let isFieldValid:boolean;

        // Find the invalid data
        pattern = contractSchemes.contract.contractNumber;
        isFieldValid = context.utility.validate(data.contractNumber, pattern);
        if (!isFieldValid) {
            console.log("Invalid contract number");
            allContractSchemeFieldsValid = false;
            if (!data.contractNumber) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                invalidMessage = API.SCHEME_ERRORS.NUMBER;
            }
            invalidData.contractNumber = invalidMessage;
        }

        pattern = contractSchemes.contract.contractId;
        isFieldValid = context.utility.validate(data.contractId, pattern);
        if (!isFieldValid) {
            console.log("Invalid contract Id");
            allContractSchemeFieldsValid = false;
            if (!data.contractId) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                invalidMessage = API.SCHEME_ERRORS.NUMBER;
            }
            invalidData.contractId = invalidMessage;
        }

        pattern = contractSchemes.contract.shopUrl;
        isFieldValid = context.utility.validate(data.shopUrl, pattern);
        if (!isFieldValid) {
            console.log("Invalid shopUrl");
            allContractSchemeFieldsValid = false;
            if (!data.shopUrl) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                invalidMessage = API.SCHEME_ERRORS.STRING;
            }
            invalidData.shopUrl = invalidMessage;
        }

        pattern = contractSchemes.contract.startDate;
        isFieldValid = context.utility.validate(data.startDate, pattern);
        if (!isFieldValid) {
            console.log("Invalid startDate");
            allContractSchemeFieldsValid = false;
            if (!data.startDate) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                invalidMessage = API.SCHEME_ERRORS.STRING;
            }
            invalidData.startDate = invalidMessage;
        }

        pattern = contractSchemes.contract.cancelDate;
        isFieldValid = context.utility.validate(data.cancelDate, pattern);
        if (!isFieldValid) {
            console.log("Invalid cancelDate");
            allContractSchemeFieldsValid = false;
            invalidMessage = API.SCHEME_ERRORS.STRING;
            invalidData.cancelDate = invalidMessage;
        }

        pattern = contractSchemes.contract.group;
        isFieldValid = context.utility.validate(data.group, pattern);
        if (!isFieldValid) {
            console.log("Invalid group");
            allContractSchemeFieldsValid = false;
            if (!data.group) {
                invalidData.group = API.SCHEME_ERRORS.MISSING
            } else {
                invalidData.group = {};
                pattern = contractSchemes.group.number;
                isFieldValid = context.utility.validate(data.group.number, pattern);
                if (!isFieldValid) {
                    console.log("Invalid group number");
                    if (!data.group.number && isRequired) {
                        invalidMessage = API.SCHEME_ERRORS.MISSING;
                    } else {
                        invalidMessage = API.SCHEME_ERRORS.NUMBER;
                    }
                    invalidData.group.number = invalidMessage;
                }

                pattern = contractSchemes.group.name;
                isFieldValid = context.utility.validate(data.group.name, pattern);
                if (!isFieldValid) {
                    console.log("Invalid group name");
                    if (!data.group.name && isRequired) {
                        invalidMessage = API.SCHEME_ERRORS.MISSING;
                    } else {
                        invalidMessage = API.SCHEME_ERRORS.STRING;
                    }
                    invalidData.group.name = invalidMessage;
                }
            }
        }

        let validatePartner:any = context.validatePartnerScheme(context, data.salesRep, isRequired);
        if (!validatePartner.valid) {
            allContractSchemeFieldsValid = false;
            if (!data.salesRep) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                if (!validatePartner.valid) {
                    console.log("Invalid sales rep data");
                    allContractSchemeFieldsValid = false;
                    invalidMessage = validatePartner.invalidData;
                }
            }
            invalidData.salesRep = invalidMessage;
        }

        validatePartner = context.validatePartnerScheme(context, data.sponsor, isRequired);
        if (!validatePartner.valid) {
            allContractSchemeFieldsValid = false;
            if (!data.sponsor) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                if (!validatePartner.valid) {
                    console.log("Invalid sponsor data");
                    allContractSchemeFieldsValid = false;
                    invalidMessage = validatePartner.invalidData;
                }
            }
            invalidData.sponsor = invalidMessage;
        }

        pattern = contractSchemes.contract.product;
        isFieldValid = context.utility.validate(data.product, pattern);
        if (!isFieldValid) {
            console.log("Invalid product");
            allContractSchemeFieldsValid = false;
            if (!data.product) {
                invalidData.product = API.SCHEME_ERRORS.MISSING;
            } else {
                invalidData.product = {};
                pattern = contractSchemes.product.label;
                isFieldValid = context.utility.validate(data.product.label, pattern);
                if (!isFieldValid) {
                    console.log("Invalid product label");
                    if (!data.product.label && isRequired) {
                        invalidMessage = API.SCHEME_ERRORS.MISSING;
                    } else {
                        invalidMessage = API.SCHEME_ERRORS.STRING;
                    }
                    invalidData.product.label = invalidMessage;
                }

                pattern = contractSchemes.product.imageUrl;
                isFieldValid = context.utility.validate(data.product.imageUrl, pattern);
                if (!isFieldValid) {
                    console.log("Invalid product imageUrl");
                    if (!data.product.imageUrl && isRequired) {
                        invalidMessage = API.SCHEME_ERRORS.MISSING;
                    } else {
                        invalidMessage = API.SCHEME_ERRORS.STRING;
                    }
                    invalidData.product.imageUrl = invalidMessage;
                }

                pattern = contractSchemes.product.price;
                isFieldValid = context.utility.validate(data.product.price, pattern);
                if (!isFieldValid) {
                    console.log("Invalid product price");
                    if (!data.product.price && isRequired) {
                        invalidMessage = API.SCHEME_ERRORS.MISSING;
                    } else {
                        invalidMessage = API.SCHEME_ERRORS.NUMBER;
                    }
                    invalidData.product.price = invalidMessage;
                }
            }
        }

        pattern = contractSchemes.contract.divisionCode;
        isFieldValid = context.utility.validate(data.divisionCode, pattern);
        if (!isFieldValid) {
            console.log("Invalid divisionCode");
            allContractSchemeFieldsValid = false;
            if (!data.divisionCode) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                invalidMessage = API.SCHEME_ERRORS.STRING;
            }
            invalidData.divisionCode = invalidMessage;
        }

        pattern = contractSchemes.contract.languageCode;
        isFieldValid = context.utility.validate(data.languageCode, pattern);
        if (!isFieldValid) {
            console.log("Invalid languageCode");
            allContractSchemeFieldsValid = false;
            if (!data.languageCode) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                invalidMessage = API.SCHEME_ERRORS.STRING;
            }
            invalidData.languageCode = invalidMessage;
        }

        pattern = contractSchemes.contract.profitPercent;
        isFieldValid = context.utility.validate(data.profitPercent, pattern);
        if (!isFieldValid) {
            console.log("Invalid profit percent");
            allContractSchemeFieldsValid = false;
            invalidMessage = "Invalid value. Must be a Number between 0 and 1.";
            invalidData.profitPercent = invalidMessage;
        }

        if (allContractSchemeFieldsValid) {
            console.log("Required fields are valid. Data contains fields not in scheme.");
            invalidData.extraFields = "Payload contains extraneous fields";
        }

        return invalidData;
    }

    private getInvalidPaymentData(data:any, context:any):any {
        let allRequiredFieldsValid:boolean = true;
        let invalidData:any = {};
        let invalidMessage:string;

        // Find the invalid data
        if (!context.utility.validate(data.gtid, String)) {
            console.log("Invalid contract ID");
            allRequiredFieldsValid = false;
            if (!data.gtid) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                invalidMessage = API.SCHEME_ERRORS.STRING;
            }
            invalidData.gtid = invalidMessage;
        }

        if (!context.utility.validate(data.orderNumber, Match.Optional(Number))) {
            console.log("Invalid order number.");
            allRequiredFieldsValid = false;
            if (!data.orderNumber) {
                invalidMessage = API.SCHEME_ERRORS.MISSING;
            } else {
                invalidMessage = API.SCHEME_ERRORS.NUMBER;
            }
            invalidData.orderNumber = invalidMessage;
        }

        if (!data.purchases) {
            invalidData.purchases = API.SCHEME_ERRORS.MISSING;
            allRequiredFieldsValid = false;
        } else if (!context.utility.validate(data.purchases, Array)) {
            invalidData.purchases = "Purchases is not an array";
            allRequiredFieldsValid = false;
        } else if (data.purchases.length === 0) {
            invalidData.purchases = "No purchases in array";
            allRequiredFieldsValid = false;
        } else {
            invalidData.purchases = {};
            invalidData.purchases.message = "Array contains an object with an invalid data scheme.";
            data.purchases.forEach((purchase:IPurchase) => {
                if (!context.utility.validate(purchase.productName, Match.Optional(String))) {
                    allRequiredFieldsValid = false;
                    if (!purchase.productName) {
                        invalidData.purchases.productName = API.SCHEME_ERRORS.MISSING;
                    } else {
                        invalidData.purchases.productName = API.SCHEME_ERRORS.STRING;
                    }
                }
                if (!context.utility.validate(purchase.quantity, Number)) {
                    allRequiredFieldsValid = false;
                    if (!purchase.quantity) {
                        invalidData.purchases.quantity = API.SCHEME_ERRORS.MISSING;
                    } else {
                        invalidData.purchases.quantity = API.SCHEME_ERRORS.NUMBER;
                    }
                }
                if (!context.utility.validate(purchase.price, Match.Optional(Number))) {
                    allRequiredFieldsValid = false;
                    if (!purchase.price) {
                        invalidData.purchases.price = API.SCHEME_ERRORS.MISSING;
                    } else {
                        invalidData.purchases.price = API.SCHEME_ERRORS.NUMBER;
                    }
                }
            });
        }


        if (allRequiredFieldsValid) {
            console.log("Required fields are valid. Data contains fields not in scheme.");
            invalidData.extraFields = "Payload contains extraneous fields";
        }

        return invalidData;
    }

    private validatePurchases(context, purchases:Array<IPurchase>):boolean {
        let isValid:boolean = true;
        if (!purchases || purchases.length < 1) {
            isValid = false;
        } else {
            purchases.forEach((purchase:IPurchase) => {
                if (!context.utility.validate(purchase.productName, Match.Optional(String))) {
                    isValid = false;
                }
                if (!context.utility.validate(purchase.quantity, Number)) {
                    isValid = false;
                }
                if (!context.utility.validate(purchase.price, Match.Optional(Number))) {
                    isValid = false;
                }
            });
        }
        return isValid;
    }
}

interface IConnection {
    owner?:string,
    data?:any,
    error?:number,
    message?:string,
    invalidData?:any,
    success?:boolean,
    programId?:string
}
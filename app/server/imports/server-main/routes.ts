import {API} from "../api/config/api";
declare var Picker;
var bodyParser:any = Npm.require('body-parser');

// Add two middleware calls. The first attempting to parse the request body as
// JSON data and the second as URL encoded data.
Picker.middleware(bodyParser.json());
Picker.middleware(bodyParser.urlencoded({extended: false}));

// var postRoutes = Picker.filter(function (req, res) {
//     // you can write any logic you want.
//     // but this callback does not run inside a fiber
//     // at the end, you must return either true or false
//     return req.method == "POST" || req.method == "PUT";
// });
//
// var getRoutes = Picker.filter(function (req, res) {
//     return req.method == "GET";
// });

var allReqMethods = Picker.filter(function (req, res) {
    return true;
});

// // GET routes
// getRoutes.route('/api/v1/:resource/:action', function (params, req, res, next) {
//     var api:API = new API(params, req, res);
//     api.processRequest();
// });
//
// // POST routes
// postRoutes.route('/api/v1/:resource/:action', function (params, req, res, next) {
//     var api:API = new API(params, req, res);
//     api.processRequest();
// });

allReqMethods.route('/api/v1/:resource/:action', function (params, req, res, next) {
    var api:API = new API(params, req, res);
    api.processRequest();
});
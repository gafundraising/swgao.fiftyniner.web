import {OauthServiceConfig} from "../../lib/oauthConfig";
import {MeteorMethods} from "./methods";
import {IAPIKey} from "../../../both/models/api-key.model";
import {APIKeysCollection} from "../../../both/collections/api-keys.collection";
import {Constants} from "../../../both/Constants";
import {ProgramsCollection} from "../../../both/collections/programs.collection";
import {IProgramInfo} from "../../../both/models/program-info.model";

declare var process;
declare var Roles;

export class Main {
    start():void {
        this.printSettings();
        this.initData();

        // var oauthProviderConfig = new OauthServiceConfig();
        // oauthProviderConfig.initOauthServices();

        var meteorMethods = new MeteorMethods();
        meteorMethods.init();
    }

    initData():void {
        var admin:any = {
            name: {
                given: "Fiftyniner",
                family: "Admin",
                display: "Fiftyniner Admin"
            },
            username: Constants.ADMIN_USERNAME,
            email: "admin@gafundraising.com",
            roles: ['admin'],
            password: "F1ftyn1n3r@dm1n"
        };

        if (!Meteor.users.findOne({username: Constants.ADMIN_USERNAME})) {
            var userId = Accounts.createUser({
                username: admin.username,
                email: admin.email,
                password: admin.password,
                profile: {
                    name: admin.name
                }
            });

            var newKey = Random.hexString(32);

            var apiKey:IAPIKey = {
                owner: userId,
                key: newKey
            };

            var apiKeyId:string = APIKeysCollection.insert(apiKey, function (error, result) {
                if (error) {
                    console.log("Error inserting API Key: " + error.reason);
                } else {
                    console.log("Successfully created API Key for admin.");
                }
            });

            if (admin.roles.length > 0) {
                Roles.addUsersToRoles(userId, admin.roles);
            }
        } else {
            Meteor.users.update({
                username: Constants.ADMIN_USERNAME
            }, {
                $set: {
                    "profile.name": admin.name,
                    emails: [{
                        address: admin.email
                    }]
                }
            });
        }

        // Convert all products URLs from http to https
        ProgramsCollection.find({"contract.product.imageUrl":/^http:/}).forEach((program:IProgramInfo) => {
            var httpsProductImageUrl:string = program.contract.product.imageUrl.replace(/http:/, "https:");
            ProgramsCollection.update({
                _id: program._id
            }, {
                $set: {
                    "contract.product.imageUrl": httpsProductImageUrl
                }
            });
        });
    }

    printSettings():void {
        console.log("process.env.ROOT_URL: " + process.env.ROOT_URL);
        console.log("process.env.MOBILE_DDP_URL: " + process.env.MOBILE_DDP_URL);
        console.log("process.env.MOBILE_ROOT_URL: " + process.env.MOBILE_ROOT_URL);
        console.log("process.env.METEOR_ENV: " + process.env.METEOR_ENV);
        console.log("process.env.METEOR_SETTINGS: " + process.env.METEOR_SETTINGS);
        if (!process.env.METEOR_SETTINGS) {
            console.log("No METEOR_SETTINGS found.  Please restart the app with the METEOR_SETTINGS environment variable set.")
        }
    }
}

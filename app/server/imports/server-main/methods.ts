import {Constants} from "../../../both/Constants";
import {IEventPoints, IProgramInfo, IProgramScripts, ProgramInfo} from "../../../both/models/program-info.model";
import {ProgramsCollection} from "../../../both/collections/programs.collection";
import {IEventContact, IPurchase} from "../../../both/models/event-contact.model";
import {EventContactsCollection} from "../../../both/collections/event-contacts.collection";
import * as moment from "moment";
import {APIKeysCollection} from "../../../both/collections/api-keys.collection";
import {IGAOContract} from "../../../both/models/gao-contract";
import {Utils} from "../../../both/Utils";
import {EmailRequestObject} from "../../models/email-request.model";
import {MyScriptsCollection} from "../../../both/collections/my-scripts.collection";
import {EventPointsCollection} from "../../../both/collections/event-points.collection";
import {IProgramGroup} from "../../../both/models/program-group.model";
import {ProgramGroupsCollection} from "../../../both/collections/program-groups.collection";
import {NotificationHistoryCollection} from "../../../both/collections/notification-history.collection";
import {IPushNotification} from "../../../both/models/push-notification.model";
import {MyListContactsCollection} from "../../../both/collections/my-list-contacts";
import {IProgramsSales} from "../../../both/models/programs-sales.model";

var Future = Npm.require('fibers/future');

declare var console;
declare var Accounts;
declare var Roles;
declare var Push;

export class MeteorMethods {

    public init(): void {
        Meteor.methods({
            'updateAccountInfo': (updatedUser: Meteor.User) => {
                let user: Meteor.User = this.checkForUser();
                let userEmails: Array<any> = user.emails;
                if (updatedUser.emails) {
                    if (!user.emails || (user.emails && !user.emails[0]) || user.emails[0].address !== updatedUser.emails[0].address) {
                        console.log("Update email address");
                        if (updatedUser.emails[0].address) {
                            console.log("Check for existing email address: ", updatedUser.emails[0].address);
                            let existingUser: Meteor.User = Accounts.findUserByEmail(updatedUser.emails[0].address);
                            if (existingUser) {
                                throw new Meteor.Error(Constants.METEOR_ERRORS.ALREADY_EXISTS,
                                    Constants.METEOR_ERRORS.EMAIL_EXISTS);
                            }
                            userEmails = [{address: updatedUser.emails[0].address, verified: false}];
                        } else {
                            userEmails = [];
                        }
                    }
                }

                if (user.username !== updatedUser.username) {
                    console.log("Update username");
                    let existingUser: Meteor.User = Accounts.findUserByUsername(updatedUser.username);
                    if (existingUser) {
                        throw new Meteor.Error(Constants.METEOR_ERRORS.ALREADY_EXISTS,
                            Constants.METEOR_ERRORS.USERNAME_EXISTS);
                    }
                }

                let future = new Future();
                Meteor.users.update(user._id, {
                    $set: {
                        "profile.name.given": updatedUser.profile.name.given,
                        "profile.name.family": updatedUser.profile.name.family,
                        "profile.name.display": updatedUser.profile.name.given + " " + updatedUser.profile.name.family,
                        "profile.picture": updatedUser.profile.picture,
                        "emails": userEmails,
                        "username": updatedUser.username
                    }
                }, {
                    multi: false,
                    upsert: false
                }, (error, result) => {
                    if (error) {
                        console.error("Error updating account info: ", error);
                        future.throw(error);
                    } else {
                        console.log("Successfully updated account info.");
                        future.return(result);
                    }
                });

                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            'saveProgramInfo': (updatedProgramInfo: IProgramInfo) => {
                let user: Meteor.User = this.checkForUser();

                if (!updatedProgramInfo._id) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid program ID");
                }

                this.checkIsProgramManager(user, updatedProgramInfo._id);

                let future = new Future();
                ProgramsCollection.update(updatedProgramInfo._id, updatedProgramInfo, {
                    multi: false,
                    upsert: false
                }, (error, result) => {
                    if (error) {
                        console.log("Error saving programInfo: ", error);
                        future.throw(error);
                    } else {
                        future.return(updatedProgramInfo);
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            'addProgramManager': (data: {
                programId: string,
                email: string
            }) => {
                let user: Meteor.User = this.checkForUser();
                this.checkIsProgramManager(user, data.programId);
                let manager: Meteor.User = Accounts.findUserByEmail(data.email);
                if (!manager) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "User not found");
                } else {
                    Roles.addUsersToRoles(manager._id, [Constants.ROLES.PROGRAM_MANAGEMENT], data.programId);
                    return {
                        addedProgramManager: true,
                        message: "Successfully added user as program manager."
                    };
                }
            },
            'inviteProgramManager': (data: {
                programId: string,
                email: string,
                profile: {
                    name: {
                        given: string,
                        family: string,
                        display: string
                    }
                }
            }) => {
                let self = this;
                let user: Meteor.User = self.checkForUser();
                this.checkIsProgramManager(user, data.programId);

                if (!data.email || !data.programId || !data.profile || !data.profile.name) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.INVALID_PARAMS, "Invalid Params");
                }
                let manager: Meteor.User = Accounts.findUserByEmail(data.email);
                let managerId: string;
                let createdManagerAccount: boolean = true;
                if (manager) {
                    managerId = manager._id;
                    createdManagerAccount = false;
                } else {
                    data.profile.name.display = data.profile.name.given + " " + data.profile.name.family;
                    managerId = Accounts.createUser({
                        email: data.email,
                        profile: data.profile
                    });
                }
                Roles.addUsersToRoles(managerId, [Constants.ROLES.PROGRAM_MANAGEMENT], data.programId);

                // Send invitation email
                manager = Meteor.users.findOne({_id: managerId});

                let token: string = null;

                if (!manager.emails[0].verified) {
                    console.log("Sending enrollment email to: ", manager.emails[0].address);
                    token = Utils.generatePasswordResetTokenForUser(manager);
                }

                let program: IProgramInfo = ProgramsCollection.findOne({_id: data.programId});
                let salesRep: Meteor.User = Accounts.findUserByEmail(program.contract.salesRep.email);

                let fromEmailAddress: string = Constants.EMAIL_STRINGS.from.ga;
                if (program.contract.divisionCode === Constants.DIVISION_CODE.QSPCA) {
                    fromEmailAddress = Constants.EMAIL_STRINGS.from.qsp;
                }
                let emailOptions: any = {
                    from: Meteor.settings.public["appName"] + fromEmailAddress,
                    subject: "Your " + Meteor.settings.public["appName"] + " program is ready!",
                    recipientName: manager.profile.name.display
                };

                emailOptions.to = manager.emails[0].address;
                emailOptions.html = Utils.getWelcomeEmailHtml({
                    user: manager,
                    token: token,
                    program: program,
                    salesRep: salesRep
                });

                let future = new Future();
                self.sendProgramReadyEmail(emailOptions, (error, result) => {
                    if (error) {
                        future.throw(error);
                    } else {
                        result.addedProgramManager = true;
                        result.createdManagerAccount = createdManagerAccount;
                        future.return(result);
                    }
                });

                try {
                    return future.wait();
                } catch (error) {
                    console.error("Send Invitation Email Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }
            },
            'removeProgramManager': (data: {
                userId: string,
                programId: string
            }) => {
                let self = this;
                let user: Meteor.User = self.checkForUser();
                this.checkIsProgramManager(user, data.programId);

                let manager: Meteor.User = Meteor.users.findOne({_id: data.userId});
                if (!manager) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "User not found");
                }
                let roles: Array<string> = [];
                if (manager["roles"] && manager["roles"][data.programId]) {
                    roles = manager["roles"][data.programId] || [];
                    let managementIndex: number = roles.indexOf(Constants.ROLES.PROGRAM_MANAGEMENT);
                    if (managementIndex !== -1) {
                        roles.splice(managementIndex, 1);
                    }
                    Roles.setUserRoles(manager._id, roles, data.programId);
                    return {
                        success: true,
                        message: "Successfully removed program manager."
                    };
                } else {
                    return {
                        success: false,
                        message: "User is not a program manager of this program."
                    };
                }
            },
            'sponsorUpdateEventContact': (eventContact: IEventContact) => {
                let self = this;
                let sponsor: Meteor.User = self.checkForUser();  // throws errors
                if (sponsor.username !== Constants.ADMIN_USERNAME) {
                    if (!Roles.userIsInRole(sponsor._id,
                        [Constants.ROLES.ADMIN, Constants.ROLES.PROGRAM_MANAGEMENT],
                        eventContact.programId)) {
                        throw new Meteor.Error(Constants.METEOR_ERRORS.ACCESS_DENIED, "Access Denied");
                    }
                }

                let participant: Meteor.User = Meteor.users.findOne({_id: eventContact.userId});
                if (!participant) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid Participant ID");
                }

                let program: IProgramInfo = ProgramsCollection.findOne({_id: eventContact.programId});
                if (!program) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid Program ID");
                }

                eventContact.updated = moment().toISOString();
                delete eventContact.contact;

                let future = new Future();

                EventContactsCollection.update({_id: eventContact._id}, eventContact, {
                    multi: false,
                    upsert: false
                }, (error, result) => {
                    if (error) {
                        future.throw(error);
                    } else {
                        future.return(result);

                        self.checkSalesGoalAward(participant, program);
                    }
                });

                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            'refreshApiKey': () => {
                let user: Meteor.User = this.checkForUser(); // throws errors
                if (user) {
                    let newKey = Random.hexString(32);

                    try {
                        let keyId = APIKeysCollection.update({owner: user._id}, {
                            $set: {
                                key: newKey
                            }
                        });
                        return keyId;
                    } catch (exception) {
                        return exception;
                    }
                }
            },
            'upsertContract': (contract: IGAOContract) => {
                let self = this;
                let future = new Future();
                let program: IProgramInfo = ProgramsCollection.findOne({
                    "contract.contractNumber": contract.contractNumber
                });
                let isNewProgram: boolean = (program ? false : true);
                if (isNewProgram) {
                    if (contract.contractId) {
                        let existingProgramWithSameContractId: IProgramInfo = ProgramsCollection.findOne({
                            "contract.contractId": contract.contractId
                        });
                        // Copy existing program's settings
                        program = new ProgramInfo(existingProgramWithSameContractId);
                    }
                    program = new ProgramInfo();
                }
                program.contract = contract;

                if (!program.name) {
                    program.name = contract.group.name;
                }

                if (!program.eventDate) {
                    program.eventDate = contract.startDate;
                }

                let salesRepPhoneNumber: string = program.contract.salesRep.phone;
                let salesRepEmail: string = program.contract.salesRep.email;

                let salesRepByEmail: Meteor.User;
                if (program.contract.salesRep.email) {
                    salesRepByEmail = Accounts.findUserByEmail(salesRepEmail);
                }

                let salesRepByUsername: Meteor.User;
                if (program.contract.salesRep.phone) {
                    salesRepPhoneNumber = Utils.removeNonDigits(salesRepPhoneNumber);
                    salesRepByUsername = Accounts.findUserByUsername(salesRepPhoneNumber);
                }

                let salesRep: Meteor.User;
                let setEmail: boolean = false;
                let setUsername: boolean = false;
                if (salesRepByEmail) {
                    console.log("Found user account for sales rep by email.");
                    salesRep = salesRepByEmail;
                    if (!salesRepByUsername) {
                        setUsername = true;
                    }
                    if (salesRepByUsername && salesRepByUsername._id !== salesRepByEmail._id) {
                        console.log("Found another user account for sales rep by phone number.");
                    }
                } else if (!salesRepByEmail && salesRepByUsername) {
                    console.log("User account by email not found.  Found user account for sales rep by phone number.");
                    salesRep = salesRepByUsername;
                    setEmail = true;
                }

                //let sponsor:Meteor.User = Accounts.findUserByEmail(program.contract.sponsor.email);

                let newUsers: Array<Meteor.User> = [];
                if (!salesRep && (salesRepPhoneNumber || salesRepEmail)) {
                    console.log("Creating sales rep user account.");
                    let nameSplit: Array<string> = program.contract.salesRep.name.split(" ");
                    let newUser: any = {
                        password: program.contract.salesRep.number,
                        profile: {
                            name: {
                                given: nameSplit[0],
                                family: nameSplit[1],
                                display: program.contract.salesRep.name
                            },
                            salesRepId: program.contract.salesRep.number
                        }
                    };
                    if (salesRepPhoneNumber) {
                        newUser.username = salesRepPhoneNumber;
                    }
                    if (salesRepEmail) {
                        newUser.email = salesRepEmail;
                    }
                    newUsers.push(newUser);
                } else if (salesRep) {
                    let modifiedFields: any = {};
                    if (setEmail) {
                        console.log("Setting sales rep email.");
                        modifiedFields.emails = [{address: program.contract.salesRep.email, verified: false}]
                    }
                    if (setUsername && salesRepPhoneNumber) {
                        console.log("Setting sales rep username as phone number");
                        modifiedFields.username = salesRepPhoneNumber;
                    }
                    if (!salesRep.profile.salesRepId) {
                        console.log("Setting sales rep ID");
                        modifiedFields["profile.salesRepId"] = program.contract.salesRep.number;
                    }
                    let hasModifiedKeys: boolean = Object.keys(modifiedFields).length > 0;
                    if (hasModifiedKeys) {
                        console.log("Updating user account.");
                        Meteor.users.update({_id: salesRep._id}, {
                            $set: modifiedFields
                        });
                    }
                }

                // if (!sponsor) {
                //     let nameSplit:Array<string> = program.contract.sponsor.name.split(" ");
                //     let newUser:any = {
                //         username: Utils.removeNonDigits(program.contract.sponsor.phone),
                //         email: program.contract.sponsor.email,
                //         password: program.contract.contractNumber,
                //         profile: {
                //             name: {
                //                 given: nameSplit[0],
                //                 family: nameSplit[1],
                //                 display: program.contract.sponsor.name
                //             },
                //             sponsorId: program.contract.sponsor.number
                //         }
                //     };
                //     newUsers.push(newUser);
                // } else {
                //     if (!sponsor.profile.sponsorId) {
                //         Meteor.users.update({_id: sponsor._id}, {
                //             $set: {
                //                 "profile.sponsorId": program.contract.sponsor.number
                //             }
                //         });
                //     }
                // }

                if (newUsers.length > 0) {
                    newUsers.forEach((user: any) => {
                        console.log("Creating new user: ", user);
                        let newUser: any = {
                            password: user.password.toString(),
                            profile: user.profile
                        };
                        if (user.username) {
                            newUser.username = user.username;
                        }
                        if (user.email) {
                            newUser.email = user.email;
                        }
                        if (newUser.username || newUser.email) {
                            let userId: string = Accounts.createUser(newUser);
                            user._id = userId;
                            console.log("Created new user account: ", userId);
                        }
                    });
                }

                salesRep = Meteor.users.findOne({"profile.salesRepId": program.contract.salesRep.number});
                //sponsor = Meteor.users.findOne({"profile.sponsorId": program.contract.sponsor.number});

                if (salesRep) {
                    program.userId = salesRep._id;
                }

                ProgramsCollection.upsert(program._id, program, {
                    multi: false
                }, (error, result) => {
                    if (error) {
                        console.error("Error saving program: ", error);
                        future.throw(error);
                    } else {
                        if (result.insertedId) {
                            program._id = result.insertedId;
                        }

                        // Add salesRep to admin role
                        if (salesRep) {
                            Roles.addUsersToRoles(salesRep._id, [Constants.ROLES.ADMIN], program._id);

                            // Send emails
                            let fromEmailAddress: string = Constants.EMAIL_STRINGS.from.ga;
                            let companyString: string = Constants.EMAIL_STRINGS.subject.ga;
                            if (program.contract.divisionCode === Constants.DIVISION_CODE.QSPCA) {
                                fromEmailAddress = Constants.EMAIL_STRINGS.from.qsp;
                                companyString = Constants.EMAIL_STRINGS.subject.qsp;
                            }
                            let emailOptions: any = {
                                from: Meteor.settings.public["appName"] + fromEmailAddress,
                                subject: "Your" + companyString + Meteor.settings.public["appName"] + " program is ready!"
                            };

                            let sendSalesRepEmail: boolean = false;
                            //let sendSponsorEmail:boolean = false;
                            let token: string = null;

                            if (isNewProgram) {
                                sendSalesRepEmail = true;
                                if (!salesRep.emails[0].verified) {
                                    token = Utils.generatePasswordResetTokenForUser(salesRep);
                                }
                            }

                            if (sendSalesRepEmail) {
                                console.log("Sending enrollment email to: ", salesRep.emails[0].address);
                                emailOptions.to = salesRep.emails[0].address;
                                emailOptions.html = Utils.getWelcomeEmailHtml({
                                    user: salesRep,
                                    token: token,
                                    program: program,
                                    salesRep: salesRep
                                });
                                emailOptions.recipientName = salesRep.profile.name.display;

                                // try {
                                //     Email.send(emailOptions);
                                // } catch (error) {
                                //     future.throw(error);
                                // }
                                self.sendProgramReadyEmail(emailOptions, (error, result) => {
                                    if (error) {
                                        console.error("sendProgramReadyEmail() Error: ", error);
                                    } else {
                                        console.log("sendProgramReadyEmail: ", result);
                                    }
                                });
                            }
                        }

                        // if (!sponsor) {
                        //     console.log("Sponsor account does not exist.");
                        // } else {
                        //     Roles.addUsersToRoles(sponsor._id, [Constants.ROLES.PROGRAM_MANAGEMENT], program._id);
                        //    
                        //     token = null;
                        //     if (!sponsor.emails[0].verified) {
                        //         console.log("Sending enrollment email to: ", sponsor.emails[0].address);
                        //         sendSponsorEmail = true;
                        //         token = Utils.generatePasswordResetTokenForUser(sponsor);
                        //     } else if (isNewProgram) {
                        //         sendSponsorEmail = true;
                        //     }
                        //
                        //     if (sendSponsorEmail) {
                        //         emailOptions.to = sponsor.emails[0].address;
                        //         emailOptions.html = Utils.getWelcomeEmailHtml({
                        //             user: sponsor,
                        //             token: token,
                        //             program: program,
                        //             salesRep: salesRep
                        //         });
                        //         emailOptions.recipientName = sponsor.profile.name.display;
                        //        
                        //         // try {
                        //         //     Email.send(emailOptions);
                        //         // } catch (error) {
                        //         //     future.throw(error);
                        //         // }
                        //         self.sendProgramReadyEmail(emailOptions, (error, result) => {
                        //             if (error) {
                        //                 console.error("sendProgramReadyEmail() Error: ", error);
                        //             } else {
                        //                 console.log("sendProgramReadyEmail: ", result);
                        //             }
                        //         });
                        //     }
                        // }

                        future.return(program);
                    }
                });

                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            'updateContract': (contract: IGAOContract) => {
                let self = this;
                let future = new Future();
                let program: IProgramInfo = ProgramsCollection.findOne({
                    "contract.contractNumber": contract.contractNumber
                });

                if (!program) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid Contract Number");
                }

                let updateFields: any = {};
                Object.keys(contract).forEach((key: string) => {
                    let selector: string = "contract." + key;
                    if (contract[key] !== null && typeof contract[key] === 'object') {
                        Object.keys(contract[key]).forEach((childKey) => {
                            let childSelector: string = selector + "." + childKey;
                            updateFields[childSelector] = contract[key][childKey];
                        });
                    } else {
                        updateFields[selector] = contract[key];
                    }
                });

                ProgramsCollection.update({_id: program._id}, {$set: updateFields}, {multi: false, upsert: false},
                    (error, result) => {
                        if (error) {
                            console.error("Error update program contract: ", error);
                            future.throw(error);
                        } else {
                            future.return(program._id);
                        }
                    });

                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            'receiveEventContactPayment': (data: {
                gtid: string,
                purchases: Array<IPurchase>
            }) => {
                let self = this;
                let future = new Future();

                let eventContact: IEventContact = EventContactsCollection.findOne({_id: data.gtid});
                if (!eventContact) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid contact ID");
                }

                let participant: Meteor.User = Meteor.users.findOne({_id: eventContact.userId});
                if (!participant) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid User ID");
                }

                let program: IProgramInfo = ProgramsCollection.findOne({_id: eventContact.programId});
                if (!program) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid Program ID");
                }

                eventContact.updated = moment().toISOString();
                eventContact.isPaid = true;
                eventContact.paidOnline = true;
                eventContact.status = Constants.EVENT_CONTACT_STATUS.PAID;

                let purchases: Array<IPurchase> = eventContact.purchases || [];
                let quantityPurchased: number = eventContact.purchased || 0;
                if (data.purchases && data.purchases.length > 0) {
                    data.purchases.forEach((purchase: IPurchase) => {
                        quantityPurchased += Number(purchase.quantity);
                        if (!purchase.productName) {
                            purchase.productName = program.contract.product.label;
                        }
                        if (!purchase.price) {
                            purchase.price = program.contract.product.price;
                        }

                        purchase.price = purchase.quantity * purchase.price;

                        purchases.push(purchase);
                    });
                }
                eventContact.purchased = quantityPurchased;
                eventContact.purchases = purchases;
                eventContact.eventPoints.paid = eventContact.purchased * program.eventPoints.paid;

                // TFS 45292 :Blitz: Always set commitments equal to quantity purchased
                // Edit: only if quantity is greater than commitment
                if (quantityPurchased > eventContact.commitment) {
                    eventContact.commitment = quantityPurchased;
                    eventContact.eventPoints.commitment = eventContact.purchased * program.eventPoints.commitment;
                }
                
                EventContactsCollection.update({_id: eventContact._id}, eventContact, {
                    multi: false,
                    upsert: false
                }, (error, result) => {
                    if (error) {
                        future.throw(error);
                    } else {
                        future.return(result);

                        self.checkSalesGoalAward(participant, program);
                    }
                });

                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            "deactivateSalesRep": (data: { salesRepId: string }) => {
                let user: Meteor.User = this.checkForUser(); // throws errors
                if (user.username !== Constants.ADMIN_USERNAME) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCESS_DENIED, "Access denied");
                }
                let salesRep: Meteor.User = Meteor.users.findOne({_id: data.salesRepId});
                if (!salesRep) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid Sales Rep ID");
                }

                if (salesRep["roles"]) {
                    let programIds: Array<string> = Object.keys(salesRep["roles"]) || [];
                    programIds.forEach((programId: string) => {
                        if (Roles.userIsInRole(salesRep._id, [
                            Constants.ROLES.ADMIN,
                            Constants.ROLES.PROGRAM_MANAGEMENT
                        ], programId)) {
                            let roles: Array<string> = salesRep["roles"][programId] || [];
                            let roleIndex: number = roles.indexOf(Constants.ROLES.ADMIN);
                            if (roleIndex !== -1) {
                                roles.splice(roleIndex, 1);
                            }
                            roleIndex = roles.indexOf(Constants.ROLES.PROGRAM_MANAGEMENT);
                            if (roleIndex !== -1) {
                                roles.splice(roleIndex, 1);
                            }
                            Roles.setUserRoles(salesRep._id, roles, programId);
                        }
                    });
                }

                Meteor.users.update({_id: data.salesRepId}, {$set: {"profile.salesRepId": Constants.EMPTY_STRING}});

                return {
                    success: true,
                    message: "Successfully deactivated sales rep."
                };
            },
            "saveMyScripts": (scripts: IProgramScripts) => {
                let user: Meteor.User = this.checkForUser();
                let future = new Future();
                MyScriptsCollection.upsert({_id: scripts._id}, scripts, {
                    multi: false
                }, (error, result) => {
                    if (error) {
                        console.error("Error saving MyScripts: ", error);
                        future.throw(error);
                    } else {
                        future.return(result);
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            "removeMyScripts": (data: { scriptId: string }) => {
                let user: Meteor.User = this.checkForUser();
                let future = new Future();
                MyScriptsCollection.remove({_id: data.scriptId}, (error, result) => {
                    if (error) {
                        console.error("Error removing MyScripts: ", error);
                        future.throw(error);
                    } else {
                        future.return(result);
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            "saveMyPoints": (points: IEventPoints) => {
                let user: Meteor.User = this.checkForUser();
                let future = new Future();
                EventPointsCollection.upsert({_id: points._id}, points, {
                    multi: false
                }, (error, result) => {
                    if (error) {
                        console.error("Error saving MyPoints: ", error);
                        future.throw(error);
                    } else {
                        future.return(result);
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            "removeMyPoints": (data: { id: string }) => {
                let user: Meteor.User = this.checkForUser();
                let future = new Future();
                EventPointsCollection.remove({_id: data.id}, (error, result) => {
                    if (error) {
                        console.error("Error removing MyPoints: ", error);
                        future.throw(error);
                    } else {
                        future.return(result);
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            "removeParticipant": (data: { programId: string, participantId: string }) => {
                let programManager: Meteor.User = this.checkForUser();
                if (programManager.username !== Constants.ADMIN_USERNAME) {
                    if (!Roles.userIsInRole(programManager._id,
                        [Constants.ROLES.ADMIN, Constants.ROLES.PROGRAM_MANAGEMENT],
                        data.programId)) {
                        throw new Meteor.Error(Constants.METEOR_ERRORS.ACCESS_DENIED, "Access Denied");
                    }
                }

                let participant: Meteor.User = Meteor.users.findOne({_id: data.participantId});
                if (!participant) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid Participant ID");
                }

                let program: IProgramInfo = ProgramsCollection.findOne({_id: data.programId});
                if (!program) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid Program ID");
                }

                let participantRoles: any = participant["roles"];
                if (participantRoles && participantRoles[program._id]) {
                    let programRoles: any = participantRoles[program._id];
                    if (programRoles) {
                        let memberIndex: number = programRoles.indexOf(Constants.ROLES.MEMBER);
                        if (memberIndex > -1) {
                            programRoles.splice(memberIndex, 1);
                            participantRoles[program._id] = programRoles;
                            let future = new Future();

                            Meteor.users.update({
                                _id: participant._id
                            }, {
                                $set: {
                                    roles: participantRoles
                                }
                            }, {
                                multi: false,
                                upsert: false
                            }, (error, result) => {
                                if (error) {
                                    console.error("removeParticipant() Error: ", error);
                                    future.throw(error);
                                } else {
                                    return future.return(result);
                                }
                            });

                            try {
                                return future.wait();
                            } catch (error) {
                                throw error;
                            }
                        }
                    }
                }
            },
            "createProgramGroup": (updatedProgramGroup: IProgramGroup) => {
                let user: Meteor.User = this.checkForUser();
                this.checkIsProgramManager(user, updatedProgramGroup.programId);

                let future = new Future();
                ProgramGroupsCollection.upsert(updatedProgramGroup._id, updatedProgramGroup, {
                    multi: false
                }, (error, result) => {
                    if (error) {
                        console.error("Error saving program group: ", error);
                        future.throw(error);
                    } else {
                        if (result.insertedId) {
                            console.log("Created new program group: ", result.insertedId);
                        }
                        future.return(result);
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            "removeProgramGroup": (programGroup: IProgramGroup) => {
                let user: Meteor.User = this.checkForUser();
                this.checkIsProgramManager(user, programGroup.programId);

                let future = new Future();
                let queryKey: string = "profile.groups." + programGroup.programId;
                let query: any = {};
                let modified: any = {};
                query[queryKey] = programGroup._id;
                modified[queryKey] = Constants.EMPTY_STRING;
                Meteor.users.update(query, {$unset: modified}, {multi: true});
                ProgramGroupsCollection.remove(programGroup._id, function (error, result) {
                    if (error) {
                        console.error("Error removing program group: ", error);
                        future.throw(error);
                    } else {
                        console.log("Successfully removed program group.");
                        future.return(result);
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            "assignProgramGroupMembers": (data: {
                programId: string,
                groupId: string,
                selectedParticipantIds: Array<string>
            }) => {
                let user: Meteor.User = this.checkForUser();
                this.checkIsProgramManager(user, data.programId);

                data.selectedParticipantIds.forEach((participantId: string) => {
                    let modifier: any = {};
                    let modifierKey: string = "profile.groups." + data.programId;
                    modifier[modifierKey] = data.groupId;
                    Meteor.users.update({_id: participantId}, {$set: modifier});
                });
                return {success: true};
            },
            "removeProgramGroupMember": (data: {
                programId: string,
                participantId: string
            }) => {
                let user: Meteor.User = this.checkForUser();
                this.checkIsProgramManager(user, data.programId);

                let modifier: any = {};
                let modifierKey: string = "profile.groups." + data.programId;
                modifier[modifierKey] = Constants.EMPTY_STRING;
                Meteor.users.update({_id: data.participantId}, {$unset: modifier});
                return {success: true};
            },
            "sendPushNotification": (data: {
                programId: string,
                notification: IPushNotification
            }) => {
                let user: Meteor.User = this.checkForUser();
                this.checkIsProgramManager(user, data.programId);

                data.notification.sentAt = moment().toISOString();
                data.notification.from = user._id;

                if (!data.notification.payload) {
                    data.notification.payload = {};
                }

                data.notification.payload.action = data.notification.action;
                data.notification.payload.programId = data.programId;
                data.notification.payload.title = data.notification.title;
                data.notification.payload.message = data.notification.message;

                let future = new Future();
                NotificationHistoryCollection.insert(data.notification, function (error, notificationId) {
                    if (error) {
                        console.error("Error inserting notification history: ", error);
                        future.throw(error);
                    } else {
                        data.notification._id = notificationId;
                        data.notification.payload._id = notificationId;
                        try {
                            console.log("Sending push notification:", data.notification);
                            Push.debug = true;
                            Push.send({
                                from: user.profile.name.display,
                                title: data.notification.title,
                                text: data.notification.message,
                                sound: 'default',
                                payload: data.notification.payload,
                                query: {
                                    userId: {
                                        $in: data.notification.userIds
                                    }
                                },
                                notId: NotificationHistoryCollection.find().count()
                            });
                            console.log("Push.send did not throw an exception!!!");
                            future.return({success: true});
                        } catch (error) {
                            future.throw(error);
                        }
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            "createProgramFromContractId": (data: { onlineStoreId: number }) => {
                let onlineStoreIdAsString:string = data.onlineStoreId.toString();
                let contractId:number = Number(onlineStoreIdAsString.substr(0, onlineStoreIdAsString.length - 1));

                let future = new Future();
                let endpoint:string = Meteor.settings.private["gaoRest"]["baseUrl"];
                if (endpoint) {
                    endpoint += Meteor.settings.private["gaoRest"]["endpoints"]["setupBlitzStoreCampaign"];
                }
                let clientId: string = Meteor.settings.private["gaoRest"]["credentials"]["clientId"];
                let clientSecret: string = Meteor.settings.private["gaoRest"]["credentials"]["clientSecret"];
                let headers: any = {
                    "Client-Id": clientId,
                    "Client-Secret": clientSecret
                };
                let postData = {
                    blitzStoreCampaignRequest: {
                        onlineStoreId: Number(data.onlineStoreId)
                    }
                };
                HTTP.call("POST", endpoint, {
                    headers: headers,
                    data: postData,
                    timeout: 20 * 1000
                }, function (error, response: any) {
                    if (error) {
                        future.throw(error)
                    } else {
                        console.log("response: ", response);

                        if (!response.data || !response.data.blitzStoreCampaignResponse) {
                            future.return({
                                success: false,
                                message: "Invalid response scheme."
                            });
                        } else if (!response.data.blitzStoreCampaignResponse.campaignId) {
                            future.return({
                                success: false,
                                message: "Response missing campaignId."
                            });
                        } else {
                            future.return({
                                success: true,
                                message: "Successfully retrieve campaignId.",
                                campaignId: response.data.blitzStoreCampaignResponse.campaignId
                            });
                        }
                    }
                });

                try {
                    return future.wait();
                } catch (error) {
                    console.error("createProgramFromContractId() Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }

            },
            "getPurchaseCommitmentDiscrepancies": (data: {
                participantIds: Array<string>,
                programId: string
            }) => {
                let user = this.checkForUser();
                this.checkIsProgramManager(user, data.programId);
                let result: Array<any> = [];
                data.participantIds.forEach((participantId: string) => {
                    let eventContacts: Array<IEventContact> = EventContactsCollection.find({
                        programId: data.programId,
                        userId: participantId,
                        commitment: {
                            $exists: true,
                            $nin: [0, null]
                        },
                        isPaid: {$ne: true}
                    }, {
                        fields: {
                            contactId: 1,
                            commitment: 1
                        }
                    }).fetch();
                    eventContacts.forEach((eventContact: IEventContact) => {
                        eventContact.contact = MyListContactsCollection.findOne({
                            _id: eventContact.contactId
                        }, {
                            fields: {
                                "name.display": 1
                            }
                        });
                    });
                    result.push(eventContacts);
                });
                return result;
            },
            "getManagedPrograms": () => {
                let user = this.checkForUser();
                let query:any = {
                    $or: [{
                        "contract.cancelDate": {
                            $exists: false
                        }
                    }, {
                        "contract.cancelDate": {
                            $eq: Constants.EMPTY_STRING
                        }
                    }]
                };

                let managedProgramIds:Array<string>;
                if (user && user.username !== Constants.ADMIN_USERNAME) {
                    managedProgramIds = [];
                    if (user && user["roles"]) {
                        let programIds:Array<string> = Object.keys(user["roles"]) || [];
                        programIds.forEach((programId:string) => {
                            if (Roles.userIsInRole(user._id,
                                [Constants.ROLES.ADMIN, Constants.ROLES.PROGRAM_MANAGEMENT], programId)) {
                                managedProgramIds.push(programId);
                            }
                        });
                    }
                    query["_id"] = {$in: managedProgramIds};
                }

                let programs:Array<IProgramInfo> = ProgramsCollection.find(query, {sort: {eventDate: 1}}).fetch();

                let pipeline:Array<any> = [{
                    $project: {
                        userId: 1,
                        programId: 1,
                        commitment: 1,
                        price: 1,
                        purchasedSales: {
                            $sum: {
                                $map: {
                                    input: "$purchases",
                                    as: "purchase",
                                    in: {
                                        $sum: [{
                                            $ifNull: ["$$purchase.price", 0]
                                        }]
                                    }
                                }
                            }
                        }
                    }
                }, {
                    $group: {
                        _id: {
                            userId: "$userId",
                            programId: "$programId"
                        },
                        commitmentSales: {
                            $sum: {
                                $multiply: ["$commitment", "$price"]
                            }
                        },
                        purchasedSales: {
                            $sum: "$purchasedSales"
                        }
                    }
                }, {
                    $group: {
                        _id: "$_id.programId",
                        commitmentSales: {
                            $sum: "$commitmentSales"
                        },
                        purchasedSales: {
                            $sum: "$purchasedSales"
                        }
                    }
                }, {
                    $project: {
                        commitmentSales: "$commitmentSales",
                        purchasedSales: "$purchasedSales"
                    }
                }];
                if (managedProgramIds && managedProgramIds.length > 0) {
                    let match = {
                        $match: {
                            programId: {
                                $in: managedProgramIds
                            }
                        }
                    };
                    pipeline.unshift(match);
                }
                let programsSales:Array<IProgramsSales> = (EventContactsCollection as any).aggregate(pipeline);

                if (programs && programsSales) {
                    programsSales.forEach((programSales: IProgramsSales) => {
                        let program: IProgramInfo = programs.find((program: IProgramInfo) => {
                            return program._id === programSales._id;
                        });
                        if (program) {
                            program["commitmentSales"] = programSales.commitmentSales;
                            program["purchasedSales"] = programSales.purchasedSales;
                            program["commitmentProfit"] = programSales.commitmentSales * program.contract.profitPercent;
                            program["purchasedProfit"] = programSales.purchasedSales * program.contract.profitPercent;

                        }
                    });
                }
                return programs;
            }
        });
    }

    private checkForUser(): Meteor.User {
        let currentUserId = Meteor.userId();
        let user: Meteor.User;
        if (!currentUserId) {
            throw new Meteor.Error(Constants.METEOR_ERRORS.SIGN_IN, "Please sign in.");
        } else {
            user = Meteor.users.findOne(currentUserId);
            if (!user) {
                throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid User ID");
            }
        }
        return user;
    }

    private checkIsProgramManager(user: Meteor.User, programId: string): boolean {
        if (user.username !== Constants.ADMIN_USERNAME) {
            if (!Roles.userIsInRole(user._id,
                [Constants.ROLES.ADMIN, Constants.ROLES.PROGRAM_MANAGEMENT],
                programId)) {
                throw new Meteor.Error(Constants.METEOR_ERRORS.ACCESS_DENIED, "Access Denied");
            }
        }
        return true;
    }

    private sendProgramReadyEmail(emailOptions: {
        from: string,
        recipientName: string,
        subject: string,
        to: string,
        html: string
    }, callback: Function) {
        let emailEndpoint: string = Meteor.settings.private["gaoRest"]["baseUrl"];
        if (emailEndpoint) {
            emailEndpoint += Meteor.settings.private["gaoRest"]["endpoints"]["email"];
        }
        let clientId: string = Meteor.settings.private["gaoRest"]["credentials"]["clientId"];
        let clientSecret: string = Meteor.settings.private["gaoRest"]["credentials"]["clientSecret"];
        let headers: any = {
            "Client-Id": clientId,
            "Client-Secret": clientSecret
        };

        let postData: EmailRequestObject = new EmailRequestObject({
            senderName: emailOptions.from,
            recipientName: emailOptions.recipientName,
            recipientEmailAddress: emailOptions.to,
            subject: emailOptions.subject,
            body: emailOptions.html,
        });

        console.log("postData: ", postData);
        HTTP.call("POST", emailEndpoint, {
            headers: headers,
            data: postData,
            timeout: 20 * 1000
        }, function (error, response: any) {
            if (error) {
                console.error("Error sending email: ", error);
                if (callback) {
                    callback(error);
                }
            } else {
                console.log("send email response: ", response);

                let returnResponse: any = {
                    sentEmail: false
                };

                if (!response.data || !response.data.sendEmailResponse) {
                    returnResponse.message = "Failed to send email: Invalid response scheme";
                } else if (!response.data.sendEmailResponse.emailIdentifier) {
                    returnResponse.message = "Failed to send Email: Missing confirmation id";
                } else {
                    returnResponse.sentEmail = true;
                    returnResponse.message = "Successfully added program manager and sent invitation email.";
                }
                console.log(returnResponse);

                if (callback) {
                    callback(null, returnResponse);
                }
            }
        });
    }

    private checkSalesGoalAward(participant: Meteor.User, program: IProgramInfo): void {
        let eventContacts: Array<IEventContact> = EventContactsCollection.find({
            userId: participant._id,
            programId: program._id
        }).fetch();

        let paidOrderCount: number = 0;
        if (eventContacts && eventContacts.length > 0) {
            eventContacts.forEach((contact: IEventContact) => {
                if (contact.isPaid && contact.purchased) {
                    paidOrderCount += Number(contact.purchased);
                }
            });
        }
        if (paidOrderCount >= program.salesGoal) {
            if (!participant.profile.programs || !participant.profile.programs[program._id] || !participant.profile.programs[program._id].eventPoints || (!participant.profile.programs[program._id].eventPoints.salesGoalAward && participant.profile.programs[program._id].eventPoints.salesGoalAward !== 0)) {
                let modifierKeyString: string = "profile.programs." + program._id + ".eventPoints.salesGoalAward";
                let modifier: any = {$set: {}};
                modifier.$set[modifierKeyString] = program.eventPoints.salesGoalAward;
                Meteor.users.update({_id: participant._id}, modifier);
                this.pushSalesGoalAwardNotification(participant, program);
            }
        }
    }

    private pushSalesGoalAwardNotification(user: Meteor.User, program: IProgramInfo): void {
        let notification: IPushNotification = {};
        notification.sentAt = moment().toISOString();
        notification.from = user._id;
        notification.action = Constants.PUSH_NOTIFICATION_ACTIONS.AWARD;
        notification.title = "Award Earned";
        notification.message = "Congratulations!  You earned the sales goal award and received "
            + program.eventPoints.salesGoalAward + " points!";

        let payload: any = {
            action: notification.action,
            userId: user._id,
            programId: program._id,
            title: notification.title,
            message: notification.message,
            awardKey: Constants.AWARD_KEYS.SALES
        };
        notification.payload = payload;

        NotificationHistoryCollection.insert(notification, function (error, notificationId) {
            if (error) {
                console.error("Error inserting notification history: ", error);
            } else {
                notification._id = notificationId;
                payload._id = notificationId;
                console.log("Sending award notification: ", notification);
                Push.send({
                    from: user.profile.name.display,
                    title: notification.title,
                    text: notification.message,
                    sound: 'default',
                    payload: payload,
                    query: {
                        userId: {
                            $in: [user._id]
                        }
                    },
                    notId: NotificationHistoryCollection.find().count()
                });
            }
        });
    }
}

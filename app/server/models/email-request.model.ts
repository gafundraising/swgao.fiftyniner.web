import {Constants} from "../../both/Constants";
export interface IEmailRequest {
    senderName?: string,
    recipientName?: string,
    recipientEmailAddress?: string,
    ccEmailAddress?: string,
    bccEmailAddress?: string,
    subject?: string,
    body?: string,
    isBodyHtml?: boolean,
    priority?:string,
    country?:string
}

export class EmailRequestObject  {
    public sendEmailRequest:IEmailRequest;

    constructor(emailRequest:IEmailRequest) {
        this.sendEmailRequest = {
            senderName: Constants.EMPTY_STRING,
            recipientName: Constants.EMPTY_STRING,
            recipientEmailAddress: Constants.EMPTY_STRING,
            ccEmailAddress: Constants.EMPTY_STRING,
            bccEmailAddress: Constants.EMPTY_STRING,
            subject: Constants.EMPTY_STRING,
            body: Constants.EMPTY_STRING,
            isBodyHtml: true,
            priority: "Normal",
            country: "US"
        };
        if (emailRequest.hasOwnProperty("senderName")) {
            this.sendEmailRequest.senderName = emailRequest.senderName;
        }
        if (emailRequest.hasOwnProperty("recipientName")) {
            this.sendEmailRequest.recipientName = emailRequest.recipientName;
        }
        if (emailRequest.hasOwnProperty("recipientEmailAddress")) {
            this.sendEmailRequest.recipientEmailAddress = emailRequest.recipientEmailAddress;
        }
        if (emailRequest.hasOwnProperty("ccEmailAddress")) {
            this.sendEmailRequest.ccEmailAddress = emailRequest.ccEmailAddress;
        }
        if (emailRequest.hasOwnProperty("bccEmailAddress")) {
            this.sendEmailRequest.bccEmailAddress = emailRequest.bccEmailAddress;
        }
        if (emailRequest.hasOwnProperty("subject")) {
            this.sendEmailRequest.subject = emailRequest.subject;
        }
        if (emailRequest.hasOwnProperty("body")) {
            this.sendEmailRequest.body = emailRequest.body;
        }
        if (emailRequest.hasOwnProperty("isBodyHtml")) {
            this.sendEmailRequest.isBodyHtml = emailRequest.isBodyHtml;
        }
        if (emailRequest.hasOwnProperty("priority")) {
            this.sendEmailRequest.priority = emailRequest.priority;
        }
        if (emailRequest.hasOwnProperty("country")) {
            this.sendEmailRequest.country = emailRequest.country;
        }
    }
}

export interface ISendEmailResponseObject {
    sendEmailResponse: {
        emailIdentifier:number
    }
}
import {Constants} from "../../both/Constants";
import {EmailRequestObject} from "../models/email-request.model";

Accounts.validateNewUser(function (user:Meteor.User) {
    var email;
    if (user && user.services) {
        if (user.services.google && user.services.google.email) {
            email = user.services.google.emailControl;
        } else if (user.services.facebook && user.services.facebook.email) {
            email = user.services.facebook.emailControl;
        }
    }
    if (email) {
        var meteorUser:Meteor.User = Accounts.findUserByEmail(email);
        if (meteorUser) {
            var provider;
            if (meteorUser.services.password) {
                provider = Meteor.settings.public["appName"];
            } else if (meteorUser.services.google) {
                provider = "Google";
            } else if (meteorUser.services.facebook) {
                provider = "Facebook";
            }
            throw new Meteor.Error("email-registered", "This account signs in with " + provider + ".");
        }
    }
    return true;
});

Accounts.onCreateUser(function (options, user) {
    if (options.profile) {
        user.profile = options.profile;
    }
    // Copy oauth information to meteor profile
    if (user && user.services) {
        if (user.services.google) {
            var google = user.services.google;
            user.emails = [{
                address: google.email,
                verified: google.verified_email
            }];
            user.profile.name = {
                display: google.name,
                given: google.given_name,
                family: google.family_name
            };
            user.profile.picture = google.picture;
        } else if (user.services.facebook) {
            var facebook = user.services.facebook;
            user.emails = [{
                address: facebook.email,
                verified: false
            }];
            user.profile.name = {
                display: facebook.name,
                given: facebook.first_name,
                family: facebook.last_name
            };
        }
    }
    return user;
});

Accounts.emailTemplates.siteName = Meteor.settings.public["appName"];
Accounts.emailTemplates.from = Meteor.settings.public["appName"] + " Accounts <accounts@gafundraising.com.com>";

// Reset Password
Accounts.emailTemplates.resetPassword.subject = function (user) {
    return "Password Reset Requested";
};
Accounts.emailTemplates.resetPassword.html = function (user, url) {
    var token = url.split("reset-password/")[1];
    console.log("token: " + token);
    var href:string = Meteor.absoluteUrl()
        + Constants.ROUTES.RESET_PASSWORD + "?token=" + token;
    var userLang = "en";
    var emailHtml = Constants.EMPTY_STRING;
    var buttonStyle:string = "display: inline-block; padding: 0px 30px; margin: 0 auto; font-size: 30px; font-weight:bold; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; line-height: 1.5; text-align: center; white-space: pre-wrap; vertical-align: middle; -ms-touch-action: manipulation; touch-action: manipulation; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; background-image: none; border: 1px solid transparent; border-radius: 4px; color: #fff; background-color: #337ab7; border-color: #2e6da4; text-decoration: none;";

    if (userLang === "en") {
        emailHtml = "Hello " + user.profile.name.given +
            ", <br><br>" +
            "<div>Click the button below to navigate to the " + Meteor.settings.public["appName"] + " website and reset your password.</div>" +
            "<br><br><a href='" + href + "' style='" + buttonStyle + "'>Reset Password</a>" +
            "<br><br>Thanks for using " + Meteor.settings.public["appName"] + "!";
    }

    let emailOptions:any = {
        from: Meteor.settings.public["appName"] + " Accounts",
        subject: "Password Reset Request",
        to: user.emails[0].address,
        html: emailHtml
    };

    let emailEndpoint:string = Meteor.settings.private["gaoRest"]["baseUrl"];
    if (emailEndpoint) {
        emailEndpoint += Meteor.settings.private["gaoRest"]["endpoints"]["email"];
    }
    let clientId:string = Meteor.settings.private["gaoRest"]["credentials"]["clientId"];
    let clientSecret:string = Meteor.settings.private["gaoRest"]["credentials"]["clientSecret"];
    let headers:any = {
        "Client-Id": clientId,
        "Client-Secret": clientSecret
    };

    let postData:EmailRequestObject = new EmailRequestObject({
        senderName: emailOptions.from,
        recipientName: user.profile.name.display,
        recipientEmailAddress: emailOptions.to,
        subject: emailOptions.subject,
        body: emailOptions.html,
        priority: "Realtime"
    });

    console.log("postData: ", postData);
    HTTP.call("POST", emailEndpoint, {
        headers: headers,
        data: postData,
        timeout: 20 * 1000
    }, function (error, response:any) {
        if (error) {
            console.error(error);
        } else {
            console.log("response: ", response);
            let message = Constants.EMPTY_STRING;
            if (!response.data || !response.data.sendEmailResponse) {
                message = "Failed to send email: Invalid response scheme";
            } else if (!response.data.sendEmailResponse.emailIdentifier) {
                message = "Failed to send Email: Missing confirmation id";
            } else {
                message = "Successfully added program manager and sent invitation email.";
            }
            console.log("message");
        }
    });

    return emailHtml;
};

// // Enrollment Email
// Accounts.emailTemplates.enrollAccount.subject = function (user) {
//     return "Your Great American " + Meteor.settings.public["appName"] + " program is ready!";
// };
//
// Accounts.emailTemplates.enrollAccount.html = function (user:Meteor.User, url:string) {
//     console.log("Enrollment url: ", url);
//     var token = url.split("enroll-account/")[1];
//     console.log("token: " + token);
//
//     return Utils.getWelcomeEmailHtml(user, token);
// };
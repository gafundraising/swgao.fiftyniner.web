import {Injectable} from '@angular/core';
import {Alert, NavController} from 'ionic-angular/es2015';
import {IProgramInfo, ProgramInfo} from "../../../../both/models/program-info.model";
import {Constants} from "../../../../both/Constants";

@Injectable()
export class ProgramInfoService {
    private _programInfo:ProgramInfo = new ProgramInfo();

    public getProgramInfo():ProgramInfo {
        return this._programInfo;
    }

    public setProgramInfo(programInfo:IProgramInfo):void {
        var program:ProgramInfo = new ProgramInfo(programInfo);
        Session.set(Constants.SESSION.PROGRAM_ID, program._id);
        this._programInfo = program;
    }

    public upsert(programInfo:IProgramInfo, callback:Function):void {
        Meteor.call("saveProgramInfo", programInfo, (error, result) => {
            callback(error, result);
        });
    }
}
import {Component, OnInit, NgZone, ViewChild} from "@angular/core";
import {MeteorComponent} from "angular2-meteor";
import {Platform, LoadingController, Loading, Content, ViewController, AlertController} from "ionic-angular/es2015";
import {Constants} from "../../../both/Constants";
import {HomePage} from "./pages/home/home";
import {TranslateService} from "@ngx-translate/core";
import {LoginPage} from "./pages/account/login/login";
import {AboutPage} from "./pages/about/about";
import {AccountMenuPage} from "./pages/account/account-menu/account-menu";
import {LeaderboardPage} from "./pages/leaderboard/leaderboard";
import {SalesRepsPage} from "./pages/sales-reps/sales-reps";
import {RequestHelper} from "./utils/RequestHelper";
import {LandingPage} from "./pages/landing/landing";
import {ImageService} from "./utils/ImageService";

@Component({
    selector: "ion-app",
    templateUrl: "app.component.html"
})
export class AppComponent extends MeteorComponent implements OnInit {
    @ViewChild('leftMenu') leftMenu:any;
    @ViewChild('content') nav:any;
    @ViewChild(Content) content:Content;

    // Set the root (or first) page
    public rootPage:any = LandingPage;
    public appName:string;
    public appIcon:string = Constants.APP_ICON;
    public user:Meteor.User;
    public pages:Array<INavigationMenuPage>;
    public userPages:Array<INavigationMenuPage>;
    public noUserPages:Array<INavigationMenuPage>;
    private isLoading:boolean = false;
    private loading:Loading;

    constructor(public platform:Platform,
                public loadingCtrl:LoadingController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService,
                private imageService:ImageService) {
        super();
    }

    ngOnInit():void {
        var self = this;
        this.parseUrl();
        this.initializeApp();
        // set the nav menu title to the application name from settings.json
        this.appName = Meteor.settings.public["appName"];

        // set our app's pages
        // title references a key in the language JSON to be translated by the translate pipe in the HTML
        this.noUserPages = [{
            icon: "log-in",
            title: 'page-login.signIn',
            component: LoginPage,
            rootPage: true
        }];
        this.pages = [{
            icon: "information-circle",
            title: "page-about.title",
            component: AboutPage,
            rootPage: false
        }];

        this.autorun(() => this.zone.run(() => {
            if (Session.get(Constants.SESSION.PLATFORM_READY)) {
                this.platformReady();

                // Reset PLATFORM_READY flag so styles will be applied correctly with hotcode push
                Session.set(Constants.SESSION.PLATFORM_READY, false);
            }

            if (Session.get(Constants.SESSION.SHOW_LEADERBOARD_MENU_ITEM)) {
                this.setUserPages();
            }
        }));

        this.translate.onLangChange.subscribe(() => {
            Session.set(Constants.SESSION.TRANSLATIONS_READY, true);
        });

        // Global loading dialog
        this.autorun(() => {
            // Use Session.set(Constants.SESSION.LOADING, true) to trigger loading dialog
            if (Session.get(Constants.SESSION.LOADING)) {
                if (this.nav) {
                    // Delay to prevent showing if loaded quickly
                    Meteor.setTimeout(() => {
                        if (!this.loading && Session.get(Constants.SESSION.LOADING)) {
                            this.loading = this.loadingCtrl.create({
                                spinner: 'crescent'
                                //content: 'Loggin in...'
                            });
                            this.loading.present();
                            this.isLoading = true;
                        }
                    }, 500);
                }
            } else {
                if (this.isLoading && this.loading) {
                    this.loading.dismiss();
                    this.loading = null;
                }
            }
        });

        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
            console.log("user: ", this.user);
            this.content.resize();
            this.setUserPages();
        }));
    }

    private setUserPages():void {
        this.userPages = [{
            icon: "home",
            title: 'page-home.title',
            component: HomePage,
            rootPage: true
        }, {
            icon: "trophy",
            title: 'page-leaderboard.title',
            component: LeaderboardPage,
            hide: !Session.get(Constants.SESSION.SHOW_LEADERBOARD_MENU_ITEM)
        }, {
            icon: "contacts",
            title: "page-sales-reps.title",
            component: SalesRepsPage,
            hide: (!this.user || this.user.username !== Constants.ADMIN_USERNAME)
        }];
    }

    public isActivePage(page:INavigationMenuPage):boolean {
        var isActive:boolean = false;
        if (this.nav && this.nav.getActive()) {
            isActive = this.nav.getActive().component === page.component;
        }
        return isActive;
    }

    private parseUrl():void {
        if (!Meteor.isCordova) {
            var path = RequestHelper.getPath(this.platform.url());
            var urlParams:any = RequestHelper.getUrlParams(this.platform.url());
            if (urlParams) {
                var scope:any = urlParams.scope;
                if (scope) {
                    urlParams.scope = scope.split(',');
                }
                urlParams.string = RequestHelper.getUrlParamString(this.platform.url());
            }
            Session.set(Constants.SESSION.PATH, path);
            Session.set(Constants.SESSION.URL_PARAMS, urlParams);
            console.log("path: " + path);
            console.log("urlParams: " + JSON.stringify(urlParams));
        }
    }

    private initializeApp() {
        this.platform.ready().then(() => {
            Session.set(Constants.SESSION.PLATFORM_READY, true);
        });
    }

    private platformReady():void {
        this.initializeTranslateServiceConfig();
        this.setStyle();
        this.checkBrowser();
    }

    private initializeTranslateServiceConfig() {
        var prefix = '/i18n/';
        var suffix = '.json';

        var userLang = navigator.language.split('-')[0];
        userLang = /(en|es)/gi.test(userLang) ? userLang : 'en';

        this.translate.setDefaultLang('en');
        let langPref = Session.get(Constants.SESSION.LANGUAGE);
        if (langPref) {
            userLang = langPref;
        }
        Session.set(Constants.SESSION.LANGUAGE, userLang);
        this.translate.use(userLang);
    }

    private setStyle():void {
        // Change value of the meta tag
        var links:any = document.getElementsByTagName("link");
        for (var i = 0; i < links.length; i++) {
            var link = links[i];
            if (link.getAttribute("rel").indexOf("style") != -1 && link.getAttribute("title")) {
                link.disabled = true;
                if (link.getAttribute("title") === this.getBodyStyle())
                    link.disabled = false;
            }
        }
    }

    private getBodyStyle():string {
        var bodyTag:any = document.getElementsByTagName("ion-app")[0];
        var bodyClass = bodyTag.className;
        var classArray = bodyClass.split(" ");
        var bodyStyle = classArray[2];
        return bodyStyle;
    }

    private checkBrowser():void {
        var ie:any = /MSIE|Trident/.exec(navigator.userAgent);
        if (ie) {
            console.log("Browser is Internet Explorer");
            Meteor.defer(() => {
                this.autorun(() => this.zone.run(() => {
                    // Wait for translations to be ready
                    // in case component loads before the language is set
                    // or the language is changed after the component has been rendered.
                    // Since this is the home page, this component and any child components
                    // will need to wait for translations to be ready.
                    if (Session.get(Constants.SESSION.TRANSLATIONS_READY)) {
                        this.translate.get('general.errors').subscribe((translation:any) => {
                            let alert = this.alertCtrl.create({
                                title: translation.browserCompatibility,
                                message: translation.browserCompatibilityMessage,
                                enableBackdropDismiss: false
                            });
                            alert.present();
                        });
                    }
                }));
            });
        }
    }

    private openPage(page:INavigationMenuPage) {
        this.navigate({page: page.component, setRoot: page.rootPage});
    }

    private showAccountMenu():void {
        this.navigate({page: AccountMenuPage, setRoot: false});
    }

    private logout():void {
        this.user = null;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.logout(() => {
            Session.set(Constants.SESSION.LOADING, false);
            console.log("Logged out!!!");
            this.navigate({page: LoginPage, setRoot: true});
        });
    }

    private navigate(location:{page:any, setRoot:boolean}):void {
        // close the menu when clicking a link from the menu
        // getComponent selector is the component id attribute
        this.leftMenu.close().then(() => {
            if (location.page) {
                // navigate to the new page if it is not the current page
                let viewCtrl:ViewController = this.nav.getActive();
                if (viewCtrl.component !== location.page) {
                    if (location.setRoot) {
                        this.nav.setRoot(location.page);
                    } else {
                        this.nav.push(location.page);
                    }
                }
            }
        });
    }

    private showSplitPane():any {
        let viewCtrl:ViewController = this.nav.getActive();
        if (viewCtrl) {
            if (viewCtrl.component === LeaderboardPage || viewCtrl.component === LandingPage) {
                return false;
            }
        }
        return "(min-width: 768px)";
    }
}

export interface INavigationMenuPage {
    icon?:string,
    title:string,
    component:any,
    rootPage?:boolean,
    navParams?:any,
    hide?:boolean
}

import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {ToastMessenger} from "../../utils/ToastMessenger";

declare var Roles;

@Component({
    selector: "program-manager-item",
    templateUrl: "program-manager-item.html"
})
export class ProgramManagerItemComponent extends MeteorComponent implements OnInit {
    @Input() user:Meteor.User;
    @Output() onRemoveUser:EventEmitter<{user:Meteor.User}> = new EventEmitter();
    public isSelf:boolean;
    public isAdmin:boolean;

    constructor(public alertCtrl:AlertController,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.isSelf = (this.user._id === Meteor.userId());
        this.isAdmin = Roles.userIsInRole(
            this.user._id,
            [Constants.ROLES.ADMIN],
            Session.get(Constants.SESSION.PROGRAM_ID)
        );
    }

    private removeUser(user:Meteor.User
                       //slidingItem:ItemSliding
    ):void {
        //slidingItem.close();
        var self = this;
        if (Meteor.user().username !== Constants.ADMIN_USERNAME) {
            if (!Roles.userIsInRole(Meteor.userId(), [Constants.ROLES.ADMIN, Constants.ROLES.PROGRAM_MANAGEMENT],
                    Session.get(Constants.SESSION.PROGRAM_ID))) {
                new ToastMessenger().toast({
                    type: "error",
                    message: self.translate.instant("general.errors.accessDenied")
                });
            }
        } else if (user._id === Meteor.userId()) {
            new ToastMessenger().toast({
                type: "error",
                message: self.translate.instant("program-manager-item.errors.notRemoveSelf")
            });
        } else if (Roles.userIsInRole(
                user._id,
                [Constants.ROLES.ADMIN],
                Session.get(Constants.SESSION.PROGRAM_ID))) {
            new ToastMessenger().toast({
                type: "error",
                message: self.translate.instant("program-manager-item.errors.notRemoveAdmin")
            });
        } else {
            let confirmDeleteAlert = self.alertCtrl.create({
                title: self.translate.instant("program-manager-item.alerts.removeManager.title"),
                message: self.translate.instant("program-manager-item.alerts.removeManager.message", {
                    value: user.profile.name.display
                }),
                buttons: [{
                    text: self.translate.instant("general.cancel"),
                    role: 'cancel',
                    handler: () => {
                    }
                }, {
                    text: self.translate.instant("general.remove"),
                    handler: () => {
                        self.onRemoveUser.emit({user: user});
                    }
                }],
                enableBackdropDismiss: false
            });
            confirmDeleteAlert.present();
        }
    }
}

export interface ISlidingUserItemComponent {
    onRemoveUser($event:{user:Meteor.User}):void;
}
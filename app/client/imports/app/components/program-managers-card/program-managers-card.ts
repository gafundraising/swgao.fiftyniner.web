import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, AlertController, ModalController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {ProgramInfoService} from "../../services/program-info.service";
import {Constants} from "../../../../../both/Constants";
import {ToastMessenger} from "../../utils/ToastMessenger";
import {ISlidingUserItemComponent} from "../program-manager-item/program-manager-item";
import {AddProgramManagerModal} from "../../modals/add-program-manager-modal/add-program-manager-modal";
import {FormValidator} from "../../utils/FormValidator";

declare var Roles;

@Component({
    selector: "program-managers-card",
    templateUrl: "program-managers-card.html"
})
export class ProgramManagersCardComponent extends MeteorComponent implements OnInit, ISlidingUserItemComponent {
    public user:Meteor.User;
    public managers:Array<Meteor.User>;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService,
                private programInfoService:ProgramInfoService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => {
            this.user = Meteor.user();
        });
        this.autorun(() => {
            var programId:string = Session.get(Constants.SESSION.PROGRAM_ID);
            if (programId) {
                this.subscribe(Constants.PUBLICATIONS.MANAGERS, programId, () => {
                    // console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MANAGERS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        let queryKey:string = "roles." + programId;
                        let query:any = {};
                        query[queryKey] = {
                            $in: [Constants.ROLES.ADMIN, Constants.ROLES.PROGRAM_MANAGEMENT]
                        };
                        this.managers = Meteor.users.find(query).fetch();
                        // console.log("managers: ", this.managers);
                    }));
                });
            }
        });
    }

    private addManager():void {
        var self = this;
        let alert = this.alertCtrl.create({
            title: self.translate.instant("program-managers-card.alerts.addManager.title"),
            message: self.translate.instant("program-managers-card.alerts.addManager.message"),
            inputs: [
                {
                    name: 'email',
                    placeholder: self.translate.instant("login-card.labels.email")
                },
            ],
            buttons: [{
                text: self.translate.instant("general.cancel"),
                handler: data => {
                }
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    if (!FormValidator.isValidEmail(data.email)) {
                        let alert = self.alertCtrl.create({
                            title: self.translate.instant("program-managers-card.errors.addProgramManager"),
                            message: self.translate.instant("login-card.errors.invalidEmail"),
                            buttons: [{
                                text: self.translate.instant("general.cancel"),
                                role: "cancel"
                            }, {
                                text: self.translate.instant("general.retry"),
                                handler: () => {
                                    self.addManager();
                                }
                            }]
                        });
                        alert.present();
                    } else {
                        Meteor.call("addProgramManager", {
                            programId: Session.get(Constants.SESSION.PROGRAM_ID),
                            email: data.email
                        }, (error, result) => {
                            if (error) {
                                // console.log("addProgramManager() Error: ", error);
                                if (error.error === Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND) {
                                    let modal = self.modalCtrl.create(AddProgramManagerModal, {email: data.email});
                                    modal.present();
                                } else {
                                    new ToastMessenger().toast({
                                        type: "error",
                                        title: self.translate.instant("program-managers-card.errors.addProgramManager"),
                                        message: error.reason || error.message || error
                                    });
                                }
                            } else {
                                // console.log("addProgramManager() result: ", result);
                                if (result.addedProgramManager) {
                                    new ToastMessenger().toast({
                                        type: "success",
                                        title: self.translate.instant("program-managers-card.toasts.addedProgramManager"),
                                        message: data.email
                                    });
                                }
                            }
                        });
                    }
                }
            }]
        });
        alert.present();
    }

    onRemoveUser($event:{user:Meteor.User}):void {
        var self = this;
        // console.log("onRemoveUser() Removing program manager: " + $event.user);
        Meteor.call("removeProgramManager", {
            userId: $event.user._id,
            programId: Session.get(Constants.SESSION.PROGRAM_ID)
        }, (error, result) => {
            if (error) {
                // console.log("removeProgramManager() Error: ", error);
                let errorMsg = error.reason || error.message;
                if (error.error === Constants.METEOR_ERRORS.ACCESS_DENIED) {
                    errorMsg = self.translate.instant("general.errors.accessDenied");
                }
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("program-managers-card.errors.removeProgramManager"),
                    message: errorMsg,
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }]
                });
                alert.present();
            } else {
                // console.log("removeProgramManager() Result: ", result);
                if (result.success) {
                    new ToastMessenger().toast({
                        type: "success",
                        title: self.translate.instant("program-managers-card.toasts.removedProgramManager"),
                        message: $event.user.emails[0].address
                    });
                }
            }
        });
    }
}
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class Pagination {
    page: number;
    pageSize: number;
    rowCount: number;
    pageCount: number;
}

@Component({
    selector: "pagination-component",
    templateUrl: "pagination-component.html"
})
export class PaginationComponent {
    @Input() pagination: Pagination;
    @Input() color: string;
    @Output() clickBeginning = new EventEmitter();
    @Output() clickPrevious = new EventEmitter();
    @Output() clickNext = new EventEmitter();
    @Output() clickEnd = new EventEmitter();

    get currentPageItemsMin() {
        return ((this.pagination.page - 1) * this.pagination.pageSize) + 1;
    }

    get currentPageItemsMax() {
        return Math.min((this.pagination.page) * this.pagination.pageSize, this.maxItems);
    }

    get maxItems() {
        return this.pagination.rowCount;
    }

    constructor() {}

}
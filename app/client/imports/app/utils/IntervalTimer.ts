import Moment = moment.Moment;
import moment = require("moment/moment");

export class IntervalTimer {
    private remaining:number;
    private state:number;
    private name:string;
    private interval:number;
    private callback:Function;
    private maxFires:number;
    private pausedTime:number;
    private fires:number;
    private lastTimeFired:Moment;
    private timerId:any;
    private lastPauseTime:Moment;
    private resumeId:any;

    constructor(data:{
        name:string, 
        interval:number, 
        maxFires?:number
    }, callback:Function) {
        this.remaining = 0;
        this.state = 0; //  0 = idle, 1 = running, 2 = paused, 3= resumed

        this.name = data.name;
        this.interval = data.interval; //in ms
        this.callback = callback;
        if (data.hasOwnProperty("maxFires")) {
            this.maxFires = data.maxFires;
        } else {
            this.maxFires = null;
        }
        this.pausedTime = 0; //how long we've been paused for

        this.fires = 0;
    }

    private proxyCallback() {
        if (this.maxFires != null && this.fires >= this.maxFires) {
            this.stop();
            return;
        }
        this.lastTimeFired = moment();
        this.fires++;
        this.callback();
    }

    public start() {
        console.log('Starting Timer ' + this.name);
        this.timerId = setInterval(() => this.proxyCallback(), this.interval);
        this.lastTimeFired = moment();
        this.state = 1;
        this.fires = 0;
    }

    public pause() {
        if (this.state != 1 && this.state != 3) return;

        console.log('Pausing Timer ' + this.name);

        this.remaining = this.interval - (moment().unix() - this.lastTimeFired.unix()) + this.pausedTime;
        this.lastPauseTime = moment();
        clearInterval(this.timerId);
        clearTimeout(this.resumeId);
        this.state = 2;
    }

    public resume() {
        if (this.state != 2) return;

        this.pausedTime += moment().unix() - this.lastPauseTime.unix();
        console.log(`Resuming Timer ${this.name} with ${this.remaining} remaining`);
        this.state = 3;
        this.resumeId = setTimeout(() => this.timeoutCallback(), this.remaining);
    }

    private timeoutCallback() {
        if (this.state != 3) return;

        this.pausedTime = 0;
        this.proxyCallback();
        this.start();
    }

    public stop() {
        if (this.state === 0) return;

        console.log('Stopping Timer %s. Fired %s/%s times', this.name, this.fires, this.maxFires);
        clearInterval(this.timerId);
        clearTimeout(this.resumeId);
        this.state = 0;
    }

    //set a new interval to use on the next interval loop
    public setInterval(newInterval) {
        console.log('Changing interval from %s to %s for %s', this.interval, newInterval, this.name);

        //if we're running do a little switch-er-oo
        if (this.state == 1) {
            this.pause();
            this.interval = newInterval;
            this.resume();
        }
        //if we're already stopped, idle, or paused just switch it
        else {
            this.interval = newInterval;
        }
    }

    public setMaxFires(newMax) {
        if (newMax != null && this.fires >= newMax) {
            this.stop();
        }
        this.maxFires = newMax;
    }
}
import {Component, NgZone, OnInit, ViewChild} from "@angular/core";
import {Alert, AlertController, App, Content, NavController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {IProgramInfo} from "../../../../../both/models/program-info.model";
import {ProgramInfoService} from "../../services/program-info.service";
import {ProgramTabsPage} from "../program/program-tabs";
import {LeaderboardPage} from "../leaderboard/leaderboard";
import {Pagination} from "../../components/pagination-component/pagination-component";
import * as moment from "moment";
import {ToastMessenger} from "../../utils/ToastMessenger";

declare var Roles;

@Component({
    selector: "page-home",
    templateUrl: "home.html"
})
export class HomePage extends MeteorComponent implements OnInit {
    @ViewChild(Content) content: Content;
    public user: Meteor.User;
    public programs: Array<IProgramInfo>;
    public placeholderImageUri: string = Constants.APP_LOGO;
    public searchQuery: string = Constants.EMPTY_STRING;
    public filteredPrograms: Array<IProgramInfo>;
    public CATEGORIES: any = {
        GROUP_NAME: "groupName",
        EVENT_DATE: "eventDate"
    };
    public selectedCategory: string = this.CATEGORIES.EVENT_DATE;
    public sortDescending: boolean = Session.get(Constants.SESSION.SORT_PROGRAMS_DESCENDING);
    public selectedFilter: string;

    // Pagination
    public paginationInfo: Pagination = {
        page: 1,
        pageSize: 50,
        rowCount: 0,
        pageCount: 0
    };

    constructor(public app: App,
                public nav: NavController,
                public alertCtrl: AlertController,
                public zone: NgZone,
                public translate: TranslateService,
                private programInfoService: ProgramInfoService) {
        super();
    }

    ngOnInit(): void {
        Session.set(Constants.SESSION.LOADING, true);
        if (Session.get(Constants.SESSION.SORT_CATEGORY)) {
            this.selectedCategory = Session.get(Constants.SESSION.SORT_CATEGORY);
        }
        Meteor.defer(() => {

            // Use MeteorComponent autorun to respond to reactive session variables.
            this.autorun(() => this.zone.run(() => {
                // Wait for translations to be ready
                // in case component loads before the language is set
                // or the language is changed after the component has been rendered.
                // Since this is the home page, this component and any child components
                // will need to wait for translations to be ready.
                if (Session.get(Constants.SESSION.TRANSLATIONS_READY)) {
                    this.translate.get('page-home.title').subscribe((translation: string) => {

                        // Set title of web page in browser
                        this.app.setTitle(translation);
                    });
                    this.translate.get('general.all').subscribe((translation: string) => {
                        if (Session.get(Constants.SESSION.SELECTED_FILTER)) {
                            translation = Session.get(Constants.SESSION.SELECTED_FILTER);
                        }
                        this.selectedFilter = translation;
                    });
                }
            }));

            let path: string = Session.get(Constants.SESSION.PATH);
            let urlParams: any = Session.get(Constants.SESSION.URL_PARAMS);

            if (path === "/" + Constants.ROUTES.LEADERBOARD && urlParams) {
                // console.log("Navigate to leaderboard for contract number: ", urlParams.id);
                let program: IProgramInfo = this.programs.find((program: IProgramInfo) => {
                    return program.contract.contractNumber == urlParams.id
                });
                if (program) {
                    this.programInfoService.setProgramInfo(program);
                    this.nav.insertPages(this.nav.length(), [{page: ProgramTabsPage}, {page: LeaderboardPage}]);
                }
                window.history.pushState('', document.title, Meteor.absoluteUrl());
                Session.set(Constants.SESSION.PATH, "/");
                Session.set(Constants.SESSION.URL_PARAMS, null);
            }
        });
    }

    private getItems(event: any): void {
        this.searchQuery = event.target.value;
        this.filterPrograms();
    }

    private onClearSearch(event: any): void {
        this.searchQuery = Constants.EMPTY_STRING;
        this.filterPrograms();
    }

    private filterPrograms(): void {
        let filteredPrograms: Array<IProgramInfo> = [];
        this.programs.forEach((program: IProgramInfo) => {
            filteredPrograms.push(program);
        });

        this.zone.run(() => {
            this.filteredPrograms = filteredPrograms;
            // if the value is an empty string don't filter the items
            if (this.searchQuery && this.searchQuery.trim() != Constants.EMPTY_STRING) {
                this.filteredPrograms = this.filteredPrograms.filter((program: IProgramInfo) => {
                    let searchQuery: string = this.searchQuery.toLocaleLowerCase();
                    let searchableProgramString: string = Constants.EMPTY_STRING;
                    if (program.contract) {
                        let salesRepName: string = program.contract.salesRep.name.toLocaleLowerCase();
                        let contractNumber: string = program.contract.contractNumber.toString();
                        let programName: string = program.name.toLocaleLowerCase();
                        let dateString: string = moment(program.eventDate).format("MMM D, YYYY").toLocaleLowerCase();
                        searchableProgramString = programName + salesRepName + dateString + contractNumber;
                    }
                    return (searchableProgramString && searchableProgramString.indexOf(searchQuery) > -1);
                });
            }

            if (this.selectedFilter !== this.translate.instant("general.all")) {
                this.filteredPrograms = this.filteredPrograms.filter((program: IProgramInfo) => {
                    let programYear = moment(program.eventDate).year().toString();
                    return this.selectedFilter === programYear;
                });
            }

            let pageCount: number = Math.ceil(this.filteredPrograms.length / this.paginationInfo.pageSize);
            if (this.paginationInfo.page > pageCount && pageCount > 0) {
                this.paginationInfo.page = pageCount;
            }
            this.paginationInfo.rowCount = this.filteredPrograms.length;
            this.paginationInfo.pageCount = pageCount;

            this.content.resize();
            Session.set(Constants.SESSION.LOADING, false);
            this.sortPrograms();
        });
    }

    private getManagedPrograms(): void {
        let self = this;
        Meteor.call("getManagedPrograms", (error, result) => {
            if (error) {
                Session.set(Constants.SESSION.LOADING, false);
                let alert: Alert = this.alertCtrl.create({
                    title: this.translate.instant("general.error"),
                    message: error.reason || error.message || error,
                    buttons: [this.translate.instant("general.ok")]
                });
                alert.present();
            } else {
                self.programs = result;
                self.filterPrograms();
            }
        });
    }

    private editProgram(program: IProgramInfo): void {
        this.programInfoService.setProgramInfo(program);
        this.nav.push(ProgramTabsPage);
    }

    private showLeaderboard(program: IProgramInfo): void {
        this.programInfoService.setProgramInfo(program);
        this.nav.push(LeaderboardPage)
    }

    get currentPageItemsMin() {
        let currentPageItemsMin: number = ((this.paginationInfo.page - 1) * this.paginationInfo.pageSize) + 1;
        return currentPageItemsMin;
    }

    get currentPageItemsMax() {
        let currentPageItemsMax: number = Math.min((this.paginationInfo.page) * this.paginationInfo.pageSize, this.maxItems);
        return currentPageItemsMax;
    }

    get maxItems() {
        return this.paginationInfo.rowCount;
    }

    public changePage(pageNum: number): void {
        this.zone.run(() => {
            this.paginationInfo.page = pageNum;
        });
    }

    public promptOnlineStoreId(): void {
        let self = this;
        let alert: Alert = this.alertCtrl.create({
            title: self.translate.instant("page-home.alerts.promptOnlineStoreId.title"),
            message: self.translate.instant("page-home.alerts.promptOnlineStoreId.message"),
            inputs: [{
                name: 'onlineStoreId',
                type: 'number',
                placeholder: self.translate.instant(self.translate.instant("page-home.alerts.promptOnlineStoreId.placeholder"))
            }],
            buttons: [{
                text: self.translate.instant("general.cancel")
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    if (!data.onlineStoreId) {
                        new ToastMessenger().toast({
                            type: "error",
                            message: self.translate.instant("page-home.alerts.promptOnlineStoreId.error")
                        });
                        return false;
                    } else {
                        Session.set(Constants.SESSION.LOADING, true);
                        Meteor.call("createProgramFromContractId", {onlineStoreId: data.onlineStoreId}, (error, result) => {
                            Session.set(Constants.SESSION.LOADING, false);
                            if (error) {
                                // console.error("createProgramFromContractId Error: ", error);
                                let alert: Alert = self.alertCtrl.create({
                                    title: self.translate.instant("general.error"),
                                    message: error.reason || error.message || error,
                                    buttons: [{text: self.translate.instant("general.ok")}]
                                });
                                alert.present();
                            } else {
                                // console.log("createProgramFromContractId() success: ", result);
                                this.getManagedPrograms();
                                let alert: Alert = self.alertCtrl.create({
                                    title: self.translate.instant("general.success"),
                                    message: self.translate.instant("page-home.alerts.setupBlitzStoreCampaignSuccess.message", {
                                        onlineStoreId: data.onlineStoreId,
                                        campaignId: result.campaignId
                                    }),
                                    buttons: [{
                                        text: self.translate.instant("general.done"),
                                        role: "cancel"
                                    }, {
                                        text: self.translate.instant("page-home.alerts.setupBlitzStoreCampaignSuccess.okButton"),
                                        handler: () => {
                                            self.showStoreCampaignProgramDetails(Number(result.campaignId));
                                        }
                                    }]
                                });
                                alert.present();
                            }
                        });
                        return true;
                    }
                }
            }]
        });
        alert.present();
    }

    private showStoreCampaignProgramDetails(campaignId: number):void {
        let self = this;
        setTimeout(() => {
            let program: IProgramInfo = this.programs.find((program: IProgramInfo) => {
                return program.contract.contractNumber === campaignId;
            });
            if (!program) {
                let alert:Alert = self.alertCtrl.create({
                    title: self.translate.instant("general.error"),
                    message: self.translate.instant("page-home.alerts.setupBlitzStoreCampaignSuccess.programNotFound"),
                    buttons: [{
                        text: self.translate.instant("general.cancel"),
                        role: "cancel"
                    }, {
                        text: self.translate.instant("general.tryAgain"),
                        handler: () => {
                            Session.set(Constants.SESSION.LOADING, true);
                            self.getManagedPrograms();
                            self.showStoreCampaignProgramDetails(campaignId);
                        }
                    }]
                });
                alert.present();
            } else {
                self.editProgram(program);
            }
        }, 500);
    }

    public selectSortCategory(): void {
        let self = this;
        let categories: Array<string> = Object.keys(self.CATEGORIES);
        let inputs: Array<any> = [];
        categories.forEach((category: string) => {
            let input: any = {
                type: 'radio',
                label: self.translate.instant("page-home.sortCategories." + self.CATEGORIES[category]),
                value: self.CATEGORIES[category],
                checked: (self.selectedCategory === self.CATEGORIES[category])
            };
            inputs.push(input);
        });

        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-home.alerts.selectCategory.title"),
            inputs: inputs,
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: 'cancel'
            }, {
                text: self.translate.instant("general.ok"),
                handler: (selectedValue: string) => {
                    self.selectedCategory = selectedValue;
                    Session.set(Constants.SESSION.SORT_CATEGORY, self.selectedCategory);
                    self.sortPrograms();
                }
            }]
        });
        alert.present();
    }

    public selectFilter(): void {
        let self = this;
        let options: Array<string> = [];
        options.push(self.translate.instant("general.all"));
        self.programs.forEach((program: IProgramInfo) => {
            let eventDate = moment(program.eventDate);
            let year = eventDate.year();
            if (!(options as any).includes(year.toString())) {
                options.push(year.toString());
            }
        });

        let inputs: Array<any> = [];
        options.forEach((option: string) => {
            let input: any = {
                type: 'radio',
                label: option,
                value: option,
                checked: (self.selectedFilter === option)
            };
            inputs.push(input);
        });

        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-home.alerts.selectFilter.title"),
            inputs: inputs,
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: 'cancel'
            }, {
                text: self.translate.instant("general.ok"),
                handler: (selectedValue: string) => {
                    self.selectedFilter = selectedValue;
                    Session.set(Constants.SESSION.SELECTED_FILTER, self.selectedFilter);
                    self.filterPrograms();
                }
            }]
        });
        alert.present();
    }

    public toggleSortDirection(): void {
        this.zone.run(() => {
            this.sortDescending = !this.sortDescending;
            Session.set(Constants.SESSION.SORT_PROGRAMS_DESCENDING, this.sortDescending);
            this.sortPrograms();
        });
    }

    private sortPrograms(): void {
        this.zone.run(() => {
            switch (this.selectedCategory) {
                case this.CATEGORIES.EVENT_DATE:
                    this.filteredPrograms.sort(this.sortByEventDate);
                    break;
                case this.CATEGORIES.GROUP_NAME:
                    this.filteredPrograms.sort(this.sortByName);
                    break;
            }

            if (this.sortDescending) {
                this.filteredPrograms.reverse();
            }
        });
    }

    private sortByName(a: IProgramInfo, b: IProgramInfo): number {
        if (!a.name && !b.name) {
            return 0;
        } else if (!a.name && b.name) {
            return 1;
        } else if (a.name && !b.name) {
            return -1;
        } else if (a.name.toLowerCase().trim() > b.name.toLowerCase().trim()) {
            return 1;
        } else if (a.name.toLowerCase() < b.name.toLowerCase()) {
            return -1;
        } else {
            return 0;
        }
    }

    private sortByEventDate(a: IProgramInfo, b: IProgramInfo): number {
        if (!a.eventDate && !b.eventDate) {
            return 0;
        } else if (!a.eventDate && b.eventDate) {
            return -1;
        } else if (a.eventDate && !b.eventDate) {
            return 1;
        } else {
            let aMoment = moment(a.eventDate);
            let bMoment = moment(b.eventDate);
            let diff = aMoment.diff(bMoment);
            if (diff > 0) {
                return 1;
            } else if (diff < 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    ionViewWillEnter() {
        Session.set(Constants.SESSION.SHOW_LEADERBOARD_MENU_ITEM, false);
        this.content.resize();
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.getManagedPrograms();
            }
        });
    }

    ionViewWillLeave() {
        Session.set(Constants.SESSION.SHOW_LEADERBOARD_MENU_ITEM, true);
    }
}

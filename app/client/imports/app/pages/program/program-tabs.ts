import {Component, OnInit} from "@angular/core";
import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {ProgramDetailsPage} from "./tabs/program-details/program-details";
import {ProgramScriptsPage} from "./tabs/program-scripts/program-scripts";
import {ProgramSalesPage} from "./tabs/program-sales/program-sales";
import {Constants} from "../../../../../both/Constants";
import {ProgramInfoService} from "../../services/program-info.service";
import {ProgramInfo} from "../../../../../both/models/program-info.model";

@Component({
    selector: "page-program-tabs",
    templateUrl: "program-tabs.html"
})
export class ProgramTabsPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public tab1Root = ProgramDetailsPage;
    public tab3Root = ProgramScriptsPage;
    public tab5Root = ProgramSalesPage;
    public program:ProgramInfo;

    constructor(public nav:NavController,
                public translate:TranslateService,
                private programInfoService:ProgramInfoService) {
        super();
    }

    ngOnInit():void {
        this.program = this.programInfoService.getProgramInfo();

        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                console.log("programId: ", this.program._id);
                this.subscribe(Constants.PUBLICATIONS.EVENT_TOTALS, this.program._id, () => {
                    // console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.EVENT_TOTALS + " <><><>");
                });
                this.subscribe(Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS, this.program._id, () => {
                    // console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS + " <><><>");
                });
                this.subscribe(Constants.PUBLICATIONS.PARTICIPANT_TOTALS, this.program._id, () => {
                    // console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PARTICIPANT_TOTALS + " <><><>");
                });
            }
        });
    }
}
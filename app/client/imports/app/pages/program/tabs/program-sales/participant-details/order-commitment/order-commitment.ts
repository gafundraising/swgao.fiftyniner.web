import {Component, OnInit, NgZone} from '@angular/core';
import {NavController, NavParams, AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../../../../../both/Constants";
import {IProgramInfo} from "../../../../../../../../../both/models/program-info.model";
import {IEventContact, EventContact} from "../../../../../../../../../both/models/event-contact.model";
import {ProgramsCollection} from "../../../../../../../../../both/collections/programs.collection";
import {ToastMessenger} from "../../../../../../utils/ToastMessenger";

@Component({
    selector: "page-order-commitment",
    templateUrl: "order-commitment.html"
})
export class OrderCommitmentPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public program:IProgramInfo;
    public eventContact:IEventContact;
    public quantity:number;
    public contactNameTranslateParam:any;
    private maxQuantity:number = 50;

    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.eventContact = this.params.data;
        this.contactNameTranslateParam = {contactName: this.eventContact.contact.name.display};
        this.quantity = Number(this.eventContact.commitment || 0);
        this.autorun(() => this.zone.run(() => {
            this.program = ProgramsCollection.findOne({_id: Session.get(Constants.SESSION.PROGRAM_ID)});
            this.user = Meteor.user();
        }));
    }

    private onChangeQuantity(direction:string):void {
        this.zone.run(() => {
            switch (direction) {
                case '-':
                    if (this.quantity > 0) {
                        this.quantity--;
                    }
                    break;
                case '+':
                    if (this.quantity < this.maxQuantity) {
                        this.quantity++;
                    }
                    break;
            }
        });
    }

    private onSubmit():void {
        var self = this;
        // Clean up event contact
        var tempContact:EventContact = new EventContact(self.eventContact);
        var contactUpdate:IEventContact = tempContact;
        delete contactUpdate.contact;

        // update commitments
        contactUpdate.commitment = self.quantity;
        contactUpdate.price = Number(self.program.contract.product.price);
        contactUpdate.receivedCommitment = true;
        if (contactUpdate.receivedCommitment) {
            if (contactUpdate.declinedCommitment) {
                contactUpdate.declinedCommitment = false;
            }
        }

        contactUpdate.eventPoints.commitment = contactUpdate.commitment * self.program.eventPoints.commitment;

        Meteor.call("sponsorUpdateEventContact", contactUpdate, (error, result) => {
            if (error) {
                console.log("sponsorUpdateEventContact() Error: " + JSON.stringify(error));
                var errorMsg = error.reason;
                if (!errorMsg && error.message) {
                    errorMsg = error.message;
                } else {
                    errorMsg = error;
                }
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-order-commitment.errors.updateCommitment"),
                    message: errorMsg,
                    buttons: [{
                        text: self.translate.instant("general.ok"),
                        role: 'cancel'
                    }]
                });
                alert.present();
            } else {
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-order-commitment.toast.success")
                });
                self.nav.pop();
            }
        });
    }
}
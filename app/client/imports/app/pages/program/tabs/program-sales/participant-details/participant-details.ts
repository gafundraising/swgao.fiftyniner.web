import {Component, OnInit, NgZone} from '@angular/core';
import {NavController, NavParams, AlertController, Alert} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../../../../both/Constants";
import {ProgramInfoService} from "../../../../../services/program-info.service";
import {ProgramInfo, IEventPoints} from "../../../../../../../../both/models/program-info.model";
import {EventContactsCollection} from "../../../../../../../../both/collections/event-contacts.collection";
import {IEventContact, EventContact, IPurchase} from "../../../../../../../../both/models/event-contact.model";
import {MyListContact, IMyListContact} from "../../../../../../../../both/models/my-list-contact.model";
import {MyListContactsCollection} from "../../../../../../../../both/collections/my-list-contacts";
import {OrderCommitmentPage} from "./order-commitment/order-commitment";
import {ToastMessenger} from "../../../../../utils/ToastMessenger";

@Component({
    selector: "page-participant-details",
    templateUrl: "participant-details.html"
})
export class ParticipantDetailsPage extends MeteorComponent implements OnInit {
    public participant:Meteor.User;
    public program:ProgramInfo;
    public contactData:Array<any>;
    private eventContacts:Array<IEventContact>;
    public participantListContacts:Array<IMyListContact>;
    public achievements:IEventPoints;
    private filterOptions:any;
    public selectedFilter:string;
    public filteredContacts:Array<any>;

    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService,
                private programInfoService:ProgramInfoService) {
        super();
    }

    ngOnInit() {
        this.participant = this.params.data;
        this.program = this.programInfoService.getProgramInfo();
        
        if (this.participant.profile.programs && this.participant.profile.programs[this.program._id]) {
            this.achievements = this.participant.profile.programs[this.program._id].eventPoints;
        }
        
        if (!this.achievements) {
            this.achievements = {};
        }
        
        this.achievements.registration = this.program.eventPoints.registration;

        this.filterOptions = this.translate.instant("page-participant-details.filterOptions");
        this.selectedFilter = this.filterOptions.all;

        this.subscribe(Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS, {
            programId: this.program._id,
            participantId: this.participant._id
        }, () => {
            // console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                this.eventContacts = EventContactsCollection.find({
                    userId: this.participant._id,
                    programId: this.program._id
                }).fetch();
                // console.log("EventContacts: ", this.eventContacts);
                this.joinContactData();
            }));
        });

        this.subscribe(Constants.PUBLICATIONS.MY_LIST_CONTACTS, this.program._id, () => {
            // console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MY_LIST_CONTACTS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                let participantListContacts:Array<IMyListContact> = MyListContactsCollection.find({
                    userId: this.participant._id,
                    hidden: {$in: [null, false]}
                }).fetch();
                // console.log("participantListContacts: ", participantListContacts);
                this.participantListContacts = participantListContacts;
                this.joinContactData();
            }));
        });
    }

    private joinContactData():void {
        var contactData:Array<any> = [];
        if (this.eventContacts && this.participantListContacts) {
            this.participantListContacts.forEach((listContact:Meteor.User, index:number) => {
                var eventContact:IEventContact = this.eventContacts.find(
                    (eventContact:IEventContact) => eventContact.contactId === listContact._id);
                if (!eventContact) {
                    eventContact = {};
                }
                // calculate total points
                let totalPoints:number = 0;
                if (eventContact.eventPoints) {
                    var pointKeys:Array<string> = Object.keys(eventContact.eventPoints) || [];
                    pointKeys.forEach((pointKey:string) => {
                        totalPoints += Number(eventContact.eventPoints[pointKey]);
                    });
                }
                eventContact["totalPoints"] = totalPoints;

                listContact["contact"] = eventContact;
                contactData.push(listContact);
            });
        }
        this.contactData = contactData;
        this.filterContacts();
    }

    private editOrderCommitments(contact:any):void {
        var eventContact:EventContact = new EventContact(contact.contact);
        eventContact.contact = contact;
        this.nav.push(OrderCommitmentPage, eventContact);
    }

    private confirmMarkAsPaid(eventContact:IEventContact):void {
        var self = this;
        var alert = self.alertCtrl.create({
            title: self.translate.instant("page-participant-details.buttons.markAsPaid"),
            message: self.translate.instant("page-participant-details.alerts.markAsPaid.message"),
            buttons: [{
                text: self.translate.instant("general.no"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.yes"),
                handler: () => {
                    self.markAsPaid(eventContact);
                }
            }]
        });
        alert.present();
    }

    private markAsPaid(eventContact:IEventContact):void {
        var self = this;
        var contactUpdate:EventContact = new EventContact(eventContact);
        contactUpdate.isPaid = true;
        contactUpdate.status = Constants.EVENT_CONTACT_STATUS.PAID;
        contactUpdate.purchased = contactUpdate.commitment;
        contactUpdate.eventPoints.paid = contactUpdate.purchased * self.program.eventPoints.paid;
        
        let purchase:IPurchase = {
            productName: self.program.contract.product.label,
            quantity: Number(contactUpdate.purchased),
            price : Number(self.program.contract.product.price) * Number(contactUpdate.purchased)
        };
        let purchases:Array<IPurchase> = contactUpdate.purchases || [];
        purchases.push(purchase);
        contactUpdate.purchases = purchases;

        Meteor.call("sponsorUpdateEventContact", contactUpdate, (error, result) => {
            if (error) {
                // console.log("sponsorUpdateEventContact() Error: " + JSON.stringify(error));
                var errorMsg = error.reason;
                if (!errorMsg && error.message) {
                    errorMsg = error.message;
                } else {
                    errorMsg = error;
                }
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-participant-details.errors.markAsPaid"),
                    message: errorMsg,
                    buttons: [{
                        text: self.translate.instant("general.ok"),
                        role: 'cancel'
                    }]
                });
                alert.present();
            } else {
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-participant-details.toast.markAsPaidSuccess")
                });
            }
        });
    }

    public selectFilterOption():void {
        let self = this;
        let inputs = [];
        let filterOptionValues:Array<string> = (Object as any).values(self.filterOptions);
        filterOptionValues.forEach((option:string) => {
            inputs.push({
                type: 'radio',
                label: option,
                value: option,
                checked: self.selectedFilter === option
            });
        });
        let alert:Alert = self.alertCtrl.create({
            title: self.translate.instant("page-participant-details.headers.filterContacts"),
            inputs: inputs,
            buttons: [{
                text: self.translate.instant("general.cancel")
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    self.selectedFilter = data;
                    self.filterContacts();
                }
            }]
        });
        alert.present();
    }

    private filterContacts():void {
        let filteredContacts:Array<any> = [];
        this.contactData.forEach((contact:any) => {
            let addContact:boolean = false;
            switch (this.selectedFilter) {
                case this.filterOptions.all:
                    addContact = true;
                    break;
                case this.filterOptions.uncommitted:
                    if (!contact.contact.receivedCommitment && !contact.contact.isPaid) {
                        addContact = true;
                    }
                    break;
                case this.filterOptions.unpaid:
                    if (contact.contact.receivedCommitment && !contact.contact.isPaid && contact.contact.commitment) {
                        addContact = true;
                    }
                    break;
                case this.filterOptions.paid:
                    if (contact.contact.isPaid) {
                        addContact = true;
                    }
                    break;
                case this.filterOptions.declined:
                    if (contact.contact.declinedCommitment) {
                        addContact = true;
                    }
            }
            if (addContact) {
                filteredContacts.push(contact);
            }
        });
        this.zone.run(() => {
            this.filteredContacts = filteredContacts;
        });
    }
}
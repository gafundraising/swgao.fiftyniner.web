import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
declare var Papa;
declare var window;
@Component({
    selector: "page-participant-roster",
    templateUrl: "participant-roster.html"
})
export class ParticipantRosterPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public participantsData:Array<any>;

    constructor(public nav:NavController,
                public params:NavParams,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));

        this.participantsData = [];
        this.params.data.forEach((participant:any)=> {
            this.participantsData.push(participant);
        });
        this.participantsData.sort(ParticipantRosterPage.sortParticipantsByLastName.bind(this));
    }

    public exportToCsv():void {
        this.unparseToCSV();
    }

    public static sortParticipantsByLastName(a, b):number {
        if (a.profile.name.family.toLocaleLowerCase() < b.profile.name.family.toLocaleLowerCase()) return -1;
        if (a.profile.name.family.toLocaleLowerCase() > b.profile.name.family.toLocaleLowerCase()) return 1;
        if (a.profile.name.given.toLocaleLowerCase() < b.profile.name.given.toLocaleLowerCase()) return -1;
        if (a.profile.name.given.toLocaleLowerCase() > b.profile.name.given.toLocaleLowerCase()) return 1;
        return 0;
    }

    /* Export to CSV */
    /*1*/
    private unparseToCSV():void {
        let preparedItems = this.prepareItemsToBeUnparsed();
        let csv = Papa.unparse(preparedItems);
        let blob = new Blob([csv], {type: "text/csv;charset=utf-8"});
        window.saveAs(blob, "ParticipantRoster.csv");
    }

    /*2*/
    private prepareItemsToBeUnparsed():Array<{}> {
        let exportItems:Array<{}> = [];
        for (let i = 0; i < this.participantsData.length; i++) {
            let exportItem = this.prepareItem(this.participantsData[i]);
            exportItems.push(exportItem);
        }
        return exportItems;
    }

    /*3*/
    private prepareItem(item:any):any {
        let exportItem:any = {};
        exportItem.LastName = item.profile.name.family;
        exportItem.FirstName = item.profile.name.given;
        exportItem.Contacted = item.totals.contacts;
        exportItem.Contacts = item.listContacts;
        exportItem.Orders = item.totals.purchases;
        exportItem.Commitments = item.totals.commitments;
        exportItem.Rank = item.rank;
        return exportItem;
    }

    /* End export */
}
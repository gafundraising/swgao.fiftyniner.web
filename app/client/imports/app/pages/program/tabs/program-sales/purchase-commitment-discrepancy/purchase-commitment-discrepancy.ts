import {Component, NgZone, OnInit} from '@angular/core';
import {Alert, AlertController, NavController, NavParams} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {ParticipantRosterPage} from "../participant-roster/participant-roster";
import {ProgramInfoService} from "../../../../../services/program-info.service";
import {Constants} from "../../../../../../../../both/Constants";

declare var Papa;
declare var window;

@Component({
    selector: "page-purchase-commitment-discrepancy",
    templateUrl: "purchase-commitment-discrepancy.html"
})
export class PurchaseCommitmentDiscrepancyPage extends MeteorComponent implements OnInit {
    public user: Meteor.User;
    public participantsData: Array<any>;

    constructor(public nav: NavController,
                public params: NavParams,
                public alertCtrl: AlertController,
                public zone: NgZone,
                public translate: TranslateService,
                public programInfoService: ProgramInfoService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));

        this.participantsData = [];
        this.params.data.forEach((participant: any) => {
            if (participant.totals && participant.totals.commitments > participant.totals.purchases) {
                this.participantsData.push(participant);
            }
        });
        this.participantsData.sort(ParticipantRosterPage.sortParticipantsByLastName.bind(this));
    }

    public exportToCsv(): void {
        let self = this;
        let participantIds: Array<string> = this.participantsData.map((participant: any) => {
            return participant._id;
        });
        Meteor.call("getPurchaseCommitmentDiscrepancies", {
            programId: this.programInfoService.getProgramInfo()._id,
            participantIds: participantIds
        }, (error, result) => {
            if (error) {
                console.error("Error retrieving discrepancies: ", error);
                let alert: Alert = self.alertCtrl.create({
                    title: self.translate.instant("general.error"),
                    message: error.reason || error.message || error,
                    buttons: [{text: self.translate.instant("general.ok")}]
                });
                alert.present();
            } else {
                console.log("result: ", result);
                this.participantsData.forEach((participant: any, index: number) => {
                    participant.unpaidContacts = result[index];
                });
                this.unparseToCSV();
            }
        });
    }

    /* Export to CSV */

    /*1*/
    private unparseToCSV(): void {
        let preparedItems = this.prepareItemsToBeUnparsed();
        let csv = Papa.unparse(preparedItems);
        let blob = new Blob([csv], {type: "text/csv;charset=utf-8"});
        window.saveAs(blob, "Discrepancies.csv");
    }

    /*2*/
    private prepareItemsToBeUnparsed(): Array<{}> {
        let exportItems: Array<any> = [];
        this.participantsData.forEach((participant: any) => {
            let exportItem = this.prepareItem(participant);
            exportItems.push(exportItem);
        });
        return exportItems;
    }

    /*3*/
    private prepareItem(item: any): any {
        let exportItem: any = {};
        exportItem.LastName = item.profile.name.family;
        exportItem.FirstName = item.profile.name.given;
        exportItem.Contacted = item.totals.contacts;
        exportItem.Contacts = item.listContacts;
        exportItem.Orders = item.totals.purchases;
        exportItem.Commitments = item.totals.commitments;
        exportItem.Rank = item.rank;
        let unpaidContactsString: string = Constants.EMPTY_STRING;
        let unpaidCommitments: number = 0;
        item.unpaidContacts.forEach((commitment: any) => {
            unpaidCommitments += commitment.commitment;
            if (unpaidContactsString.length > 0) {
                unpaidContactsString += ",";
            }
            unpaidContactsString += commitment.contact.name.display + ": " + commitment.commitment;
        });
        exportItem.UnpaidCommitments = unpaidCommitments;
        exportItem.UnpaidDetails = unpaidContactsString;
        return exportItem;
    }

    /* End export */
}
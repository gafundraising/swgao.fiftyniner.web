import {Component, OnInit, NgZone, ViewChild} from "@angular/core";
import {NavController, Navbar, AlertController, ModalController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {ProgramInfoService} from "../../../../services/program-info.service";
import {ProgramInfo, IEventPoints} from "../../../../../../../both/models/program-info.model";
import {Constants} from "../../../../../../../both/Constants";
import {EventTotalsCollection} from "../../../../../../../both/collections/event-totals.collection";
import {IEventTotals, EventTotals} from "../../../../../../../both/models/event-totals.model";
import {ParticipantTotalsCollection} from "../../../../../../../both/collections/participant-totals.collection";
import {IParticipantTotals, ParticipantTotals} from "../../../../../../../both/models/participant-totals.model";
import {ParticipantDetailsPage} from "./participant-details/participant-details";
import {MyListContactsCollection} from "../../../../../../../both/collections/my-list-contacts";
import {IMyListContact} from "../../../../../../../both/models/my-list-contact.model";
import * as moment from "moment";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {ProgramGroupsCollection} from "../../../../../../../both/collections/program-groups.collection";
import {IProgramGroup} from "../../../../../../../both/models/program-group.model";
import {PushNotificationModalPage} from "../../../../modals/push-notification-modal/push-notification-modal";
import {ParticipantRosterPage} from "./participant-roster/participant-roster";
import {PurchaseCommitmentDiscrepancyPage} from "./purchase-commitment-discrepancy/purchase-commitment-discrepancy";
@Component({
    selector: "page-program-sales",
    templateUrl: "program-sales.html"
})
export class ProgramSalesPage extends MeteorComponent implements OnInit {
    @ViewChild(Navbar) navBar:Navbar;
    public user:Meteor.User;
    public program:ProgramInfo;
    public eventTotals:IEventTotals = {};
    private participants:Array<Meteor.User>;
    private participantsTotals:Array<IParticipantTotals>;
    public participantsData:Array<any>;
    private participantsListContacts:Array<IMyListContact>;
    private subMyListContacts:Meteor.SubscriptionHandle;
    public selectedSegment:string;
    public COMPETITION_TYPE:any = Constants.COMPETITION_TYPE;
    public programGroups:Array<IProgramGroup>;
    public NOTIFICATION_TYPE:any = Constants.PUSH_NOTIFICATION_TYPE;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService,
                private programInfoService:ProgramInfoService) {
        super();
    }

    ngOnInit() {
        console.log("ProgramSalesPage ngOnInit()");
        this.program = this.programInfoService.getProgramInfo();
        this.selectedSegment = this.program.competitionType || Constants.COMPETITION_TYPE.INDIVIDUAL;

        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));

        // this.subscribe(Constants.PUBLICATIONS.EVENT_TOTALS, this.program._id, () => {
        //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.EVENT_TOTALS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                let eventTotals:IEventTotals = EventTotalsCollection.findOne({_id: this.program._id});
                console.log("Event Totals: ", eventTotals);
                if (eventTotals && this.participants) {
                    eventTotals.participants = this.participants.length;
                }
                if (eventTotals) {
                    eventTotals.commitmentProfit = Number(this.program.contract.profitPercent) * eventTotals.commitmentSales;
                    eventTotals.purchasedProfit = Number(this.program.contract.profitPercent) * eventTotals.purchasedSales;
                }
                this.eventTotals = new EventTotals(eventTotals);
            }));
        // });

        // this.subscribe(Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS, this.program._id, () => {
        //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                this.getParticipantsContacts();
                let queryKey:string = "roles." + this.program._id;
                let query:any = {};
                query[queryKey] = {
                    $in: [Constants.ROLES.MEMBER]
                };
                let participants:Array<Meteor.User> = Meteor.users.find(query).fetch();
                console.log("participants: " + participants.length, participants);
                this.participants = participants;
                if (this.eventTotals && participants.length > 0) {
                    this.eventTotals.participants = participants.length;
                } else if (participants.length === 0) {
                    this.eventTotals.participants = 0;
                }
                this.joinParticipantsData();
            }));
        // });

        // this.subscribe(Constants.PUBLICATIONS.PARTICIPANT_TOTALS, this.program._id, () => {
        //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PARTICIPANT_TOTALS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                // query aggregated collection
                console.log("programId: ", this.program._id);
                let participantTotals:Array<IParticipantTotals> = ParticipantTotalsCollection.find({programId: this.program._id}, {
                    sort: {
                        points: -1,
                        lastUpdated: 1
                    }
                }).fetch();
                this.participantsTotals = participantTotals;
                this.joinParticipantsData();
            }));
        // });
    }

    private getParticipantsContacts():void {
        if (this.subMyListContacts) {
            this.subMyListContacts.stop();
            this.subMyListContacts = null;
        }
        this.subMyListContacts = this.subscribe(Constants.PUBLICATIONS.MY_LIST_CONTACTS, this.program._id, () => {
            console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MY_LIST_CONTACTS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                let queryKey:string = "roles." + this.program._id;
                let query:any = {};
                query[queryKey] = {
                    $in: [Constants.ROLES.MEMBER]
                };
                let programParticipants:Array<Meteor.User> = Meteor.users.find(query).fetch();
                let participantIds:Array<string> = programParticipants.map((participant:Meteor.User) => {
                    return participant._id;
                });
                let participantsListContacts:Array<IMyListContact> = MyListContactsCollection.find({
                    userId: {$in: participantIds},
                    hidden: {$in: [null, false]}
                }).fetch();
                this.participantsListContacts = participantsListContacts;
                this.joinParticipantsData();
            }));
        });
    }

    ionViewWillEnter() {
        this.program = this.programInfoService.getProgramInfo();
        this.selectedSegment = this.program.competitionType || Constants.COMPETITION_TYPE.INDIVIDUAL;
        if (this.program.competitionType === Constants.COMPETITION_TYPE.GROUP) {
            this.subscribe(Constants.PUBLICATIONS.PROGRAM_GROUPS, this.program._id, () => {
                console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_GROUPS + " <><><>");
                this.autorun(() => this.zone.run(() => {
                    let programGroups:Array<IProgramGroup> = ProgramGroupsCollection.find({
                        programId: this.program._id
                    }).fetch();
                    console.log("program groups: ", programGroups);
                    this.programGroups = programGroups;
                    this.joinParticipantsData();
                }));
            });
        }
    }

    ionViewDidLoad() {
        this.navBar.backButtonClick = (e:UIEvent) => {
            this.nav.parent.viewCtrl.dismiss();
        };
    }

    private joinParticipantsData():void {
        let participantsData:Array<any> = [];
        if (this.participantsTotals && this.participants && this.participantsListContacts) {
            // this.participants.sort(this.sortParticipantsByTotals.bind(this));
            this.participants.forEach((participant:Meteor.User, index:number) => {
                let participantTotals:IParticipantTotals = this.participantsTotals.find(
                    (totals:IParticipantTotals) => totals._id === participant._id);

                let participantListContacts:Array<IMyListContact> = this.participantsListContacts.filter(
                    (contact:IMyListContact) => {
                        return contact.userId === participant._id;
                    }
                );

                if (participantTotals) {
                    participantTotals.commitmentProfit = participantTotals.commitmentSales * this.program.contract.profitPercent;
                    participantTotals.purchasedProfit = participantTotals.purchasedSales * this.program.contract.profitPercent;
                }
                let totals:ParticipantTotals = new ParticipantTotals(participantTotals);

                let eventPoints:IEventPoints = participant.profile.programs[this.program._id].eventPoints;
                let points:number = Number(eventPoints.contactListGoal || 0) +
                    Number(eventPoints.contactGoal || 0) + Number(eventPoints.salesGoalAward || 0);
                totals.points += points;
                totals.points += Number(this.program.eventPoints.registration || 0);
                participant["listContacts"] = participantListContacts.length;
                participant["totals"] = totals;
                if (!participant["totals"].pointsLastUpdated) {
                    participant["totals"].pointsLastUpdated = participant.profile.programs[this.program._id].eventPoints.updated;
                }

                participantsData.push(participant);
            });
        }
        this.zone.run(() => {
            participantsData.sort(this.sortDataByTotalPoints.bind(this));
            participantsData.forEach((participant:any, index:number) => {
                participant["rank"] = index + 1;
            });
            this.participantsData = participantsData;
        });

        if (this.program.competitionType === Constants.COMPETITION_TYPE.GROUP) {
            this.groupParticipants();
        }
    }

    private groupParticipants():void {
        if (this.programGroups && this.participantsData) {
            this.programGroups.forEach((group:IProgramGroup) => {
                group.members = [];
                group["totals"] = new ParticipantTotals();
            });
            this.participantsData.forEach((participant:any) => {
                if (participant.profile.groups && participant.profile.groups[this.program._id]) {
                    let participantGroupId:string = participant.profile.groups[this.program._id];
                    let programGroup:any = this.programGroups.find((group:IProgramGroup) => {
                        return group._id === participantGroupId;
                    });
                    if (programGroup) {
                        if (participant.totals) {
                            if (programGroup.members.length === 0) {
                                programGroup.totals.pointsLastUpdated = participant.totals.pointsLastUpdated;
                            } else {
                                let groupPointsLastUpdated = moment(programGroup.totals.pointsLastUpdated);
                                let participantPointsLastUpdated = moment(participant.totals.pointsLastUpdated);
                                if (groupPointsLastUpdated.diff(participantPointsLastUpdated) > 0) {
                                    programGroup.totals.pointsLastUpdated = participant.totals.pointsLastUpdated;
                                }
                            }

                            programGroup.totals.points += participant.totals.points;
                            programGroup.totals.contacts += participant.totals.contacts;
                            programGroup.totals.commitments += participant.totals.commitments;
                            programGroup.totals.commitmentSales += participant.totals.commitmentSales;
                            programGroup.totals.purchases += participant.totals.purchases;
                            programGroup.totals.purchasedSales += participant.totals.purchasedSales;
                            participant.groupName = programGroup.name;
                        }

                        programGroup.members.push(participant);
                    }
                }
            });
            this.programGroups.forEach((group:IProgramGroup) => {
                if (group["totals"]) {
                    group["totals"].commitmentProfit = Number(this.program.contract.profitPercent) * group["totals"].commitmentSales;
                    group["totals"].purchasedProfit = Number(this.program.contract.profitPercent) * group["totals"].purchasedSales;
                }
            });

            this.programGroups = this.programGroups.sort(this.sortDataByTotalPoints.bind(this));
            this.zone.run(() => {
                this.programGroups.forEach((group:any, index:number) => {
                    group["rank"] = index + 1;
                });
            });
        }
    }

    private sortParticipantsByTotals(a, b):number {
        let idMap:Array<string> = this.participantsTotals.map((participantTotal:IParticipantTotals) => {
            return participantTotal._id;
        });
        let indexA = idMap.indexOf(a._id);
        let indexB = idMap.indexOf(b._id);
        if (indexA < indexB) {
            return -1;
        } else if (indexA > indexB) {
            return 1;
        } else {
            return 0;
        }
    }

    private sortDataByTotalPoints(a, b):number {
        let sortDirection:number = b.totals.points - a.totals.points;
        if (sortDirection === 0) {
            let aMoment = moment(a.totals.pointsLastUpdated);
            let bMoment = moment(b.totals.pointsLastUpdated);
            if (aMoment.diff(bMoment) > 0) {
                sortDirection = 1;
            } else if (aMoment.diff(bMoment) < 0) {
                sortDirection = -1;
            }
        }
        return sortDirection;
    }

    private openParticipantDetails(participant:Meteor.User):void {
        this.nav.push(ParticipantDetailsPage, participant);
    }

    private confirmRemoveParticipant(participant:Meteor.User):void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-sales.alerts.confirmRemoveParticipant.title"),
            subTitle: participant.profile.name.display,
            message: self.translate.instant("page-program-sales.alerts.confirmRemoveParticipant.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.remove"),
                handler: () => {
                    self.removeParticipant(participant);
                }
            }]
        });
        alert.present();
    }

    private removeParticipant(participant:Meteor.User):void {
        let self = this;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("removeParticipant", {
            programId: self.program._id,
            participantId: participant._id
        }, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("removeParticipant() Error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-program-sales.errors.removeParticipant"),
                    message: error.reason || error.message || error,
                    buttons: [{text: self.translate.instant("general.ok")}]
                });
                alert.present();
            } else {
                new ToastMessenger().toast({
                    type: "success",
                    title: self.translate.instant("page-program-sales.toasts.removedParticipant"),
                    message: participant.profile.name.display
                });
            }
        });
    }

    private openNotificationModal(data:any):void {
        let userIds:Array<string> = [];
        let name:string = Constants.EMPTY_STRING;
        switch (data.type) {
            case Constants.PUSH_NOTIFICATION_TYPE.PROGRAM:
                name = this.program.name;
                userIds = this.participants.map((participant:Meteor.User) => {
                    return participant._id;
                });
                break;
            case Constants.PUSH_NOTIFICATION_TYPE.TEAM:
                name = data.group.name;
                userIds = data.group.members.map((participant:Meteor.User) => {
                    return participant._id;
                });
                break;
            case Constants.PUSH_NOTIFICATION_TYPE.PARTICIPANT:
                name = data.participant.profile.name.display;
                userIds.push(data.participant._id);
                break;
        }
        let modal = this.modalCtrl.create(PushNotificationModalPage, {
            type: data.type, 
            userIds: userIds, 
            name:name,
            programId: this.program._id
        });
        modal.present();
    }

    private openParticipantRoster():void {
        this.nav.push(ParticipantRosterPage, this.participantsData);
    }

    private openDiscrepancies():void {
        this.nav.push(PurchaseCommitmentDiscrepancyPage, this.participantsData);
    }
}
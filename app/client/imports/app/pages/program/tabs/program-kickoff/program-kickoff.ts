import {Component, OnInit, NgZone} from '@angular/core';
import {NavController, Navbar, AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {ProgramInfoService} from "../../../../services/program-info.service";
import {IProgramInfo, ProgramInfo} from "../../../../../../../both/models/program-info.model";
import {FormBuilder, Validators, AbstractControl, FormGroup} from '@angular/forms';
import {Constants} from "../../../../../../../both/Constants";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {FormValidator} from "../../../../utils/FormValidator";

@Component({
    selector: "component-program-kickoff",
    templateUrl: "program-kickoff.html"
})
export class ProgramKickoffComponent extends MeteorComponent implements OnInit {
    public program:ProgramInfo;
    public formGroup:FormGroup;
    public formControl:{
        kickoffCode:AbstractControl
    };
    public EVENT_POINTS:any = Constants.EVENT_POINTS;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService, 
                private programInfoService:ProgramInfoService, public fb:FormBuilder) {
        super();
    }

    ngOnInit() {
        this.program = this.programInfoService.getProgramInfo();

        this.formGroup = this.fb.group({
            'kickoffCode': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.kickoffCodeValid
            ])]
        });

        this.formControl = {
            kickoffCode: this.formGroup.controls['kickoffCode']
        };
    }

    public onSubmit():void {
        var self = this;
        if (self.formGroup.valid) {
            self.programInfoService.upsert(self.program, (error, result) => {
                //TODO use translated messages
                if (error) {
                    console.log("Error saving program info: " + error.reason);
                    if (error.error === Constants.METEOR_ERRORS.INVALID_KICKOFF_CODE) {
                        console.log("KICKOFF_CODE_INVALID");
                        Session.set(Constants.SESSION.INVALID_KICKOFF_CODE, true);
                        self.zone.run(() => {
                            self.formControl.kickoffCode.updateValueAndValidity({onlySelf: true});
                        });
                    }
                    new ToastMessenger().toast({
                        type: "error",
                        message: self.translate.instant("page-program-kickoff.errors.kickoffCode")
                    });
                } else {
                    console.log("editProgramInfo() result: ", result);
                    new ToastMessenger().toast({
                        type: "success",
                        message: "Successfully saved program information."
                    });
                }
            });
        }
    }

    private setContactMethodPoints(eventPointsKey:string):void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-kickoff.alerts.eventPoints.title"),
            subTitle: self.translate.instant("page-program-kickoff.eventPoints." + eventPointsKey),
            message: self.translate.instant("page-program-kickoff.alerts.eventPoints.message"),
            inputs: [{
                name: 'points',
                type: 'number',
                value: self.program.eventPoints[eventPointsKey].toString(),
                placeholder: self.translate.instant("page-program-kickoff.alerts.eventPoints.inputPlaceholder")
            }],
            buttons: [{
                text: self.translate.instant("general.cancel")
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    self.program.eventPoints[eventPointsKey] = data.points || 0;
                }
            }]
        });
        alert.present();
    }
    
    private setEventDuration():void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-kickoff.alerts.duration.title"),
            message: self.translate.instant("page-program-kickoff.alerts.duration.message"),
            inputs: [{
                name: 'duration',
                type: 'number',
                value: self.program.duration.toString(),
                placeholder: self.translate.instant("page-program-kickoff.alerts.duration.inputPlaceholder")
            }],
            buttons: [{
                text: self.translate.instant("general.cancel")
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    self.program.duration = data.duration || 59;
                }
            }]
        });
        alert.present();
    }
}
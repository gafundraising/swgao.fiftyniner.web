import {Component, OnInit, NgZone} from '@angular/core';
import {NavParams, NavController, ViewController, AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {IProgramGroup, ProgramGroup} from "../../../../../../../../../both/models/program-group.model";
import {Constants} from "../../../../../../../../../both/Constants";
import {ToastMessenger} from "../../../../../../utils/ToastMessenger";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {ImageService} from "../../../../../../utils/ImageService";

@Component({
    selector: "page-edit-group",
    templateUrl: "edit-group.html"
})
export class EditGroupPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public group:IProgramGroup;
    private tracker:any;
    private initPicture:boolean = true;
    public imageUri:string;
    public placeholderImageUri:string = Constants.ADD_IMAGE_PLACEHOLDER_URI;
    public formGroup:FormGroup;
    public formControl:{
        name:AbstractControl
    };

    constructor(public params:NavParams,
                public nav:NavController,
                public viewCtrl:ViewController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService, 
                public fb:FormBuilder, 
                private imageService:ImageService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));

        this.group = new ProgramGroup(this.params.data);
        if (!this.group.name) {
            this.group.name = Constants.EMPTY_STRING;
        }

        this.formGroup = this.fb.group({
            'name': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
            ])]
        });

        this.formControl = {
            name: this.formGroup.controls['name']
        };

        this.tracker = Tracker.autorun(() => this.zone.run(() => {
            if (this.initPicture) {
                this.initPicture = false;
                Session.set(Constants.SESSION.imageUri, this.group.imageUri);
            }
            this.imageUri = Session.get(Constants.SESSION.imageUri);
        }));
    }

    private dismiss():void {
        this.viewCtrl.dismiss();
    }

    private saveGroup():void {
        var self = this;
        this.group.programId = Session.get(Constants.SESSION.PROGRAM_ID);
        this.group.imageUri = this.imageUri;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("createProgramGroup", this.group, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-group-manager.errors.programGroup"),
                    subTitle: self.translate.instant("page-group-manager.errors.createGroup"),
                    message: error.reason || error.message || error,
                    buttons: [{text: self.translate.instant("general.ok")}]
                });
                alert.present();
            } else {
                console.log("createProgramGroup() result: ", result);
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-group-manager.toasts.createdGroup")
                });
                self.viewCtrl.dismiss();
            }
        });
    }

    ionViewWillLeave() {
        // stop tracker so item does not continue to update
        this.tracker.stop();
    }
}
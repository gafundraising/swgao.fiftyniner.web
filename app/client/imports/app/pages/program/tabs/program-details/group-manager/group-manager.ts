import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, AlertController, ModalController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../../../../both/Constants";
import {IProgramGroup} from "../../../../../../../../both/models/program-group.model";
import {ToastMessenger} from "../../../../../utils/ToastMessenger";
import {ProgramGroupsCollection} from "../../../../../../../../both/collections/program-groups.collection";
import {ProgramInfo} from "../../../../../../../../both/models/program-info.model";
import {ProgramInfoService} from "../../../../../services/program-info.service";
import {SelectGroupMembersModalPage} from "../../../../../modals/select-group-members-modal/select-group-members-modal";
import {EditGroupPage} from "./edit-group/edit-group";
@Component({
    selector: "page-group-manager",
    templateUrl: "group-manager.html"
})
export class GroupManagerPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public programGroups:Array<IProgramGroup>;
    private program:ProgramInfo;
    private participants:Array<Meteor.User>;
    public unassignedParticipants:Array<Meteor.User>;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService,
                private programInfoService:ProgramInfoService) {
        super();
    }

    ngOnInit() {
        this.program = this.programInfoService.getProgramInfo();

        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
            if (this.user) {
                let programId:string = Session.get(Constants.SESSION.PROGRAM_ID);
                this.subscribe(Constants.PUBLICATIONS.PROGRAM_GROUPS, programId, () => {
                    console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_GROUPS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        var programGroups:Array<IProgramGroup> = ProgramGroupsCollection.find({
                            programId: programId
                        }).fetch();
                        console.log("program groups: ", programGroups);
                        this.programGroups = programGroups;
                        this.groupParticipants();
                    }));
                });
            }
        }));

        this.subscribe(Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS, this.program._id, () => {
            console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                let queryKey:string = "roles." + this.program._id;
                let query:any = {};
                query[queryKey] = {
                    $in: [Constants.ROLES.MEMBER]
                };
                var participants:Array<Meteor.User> = Meteor.users.find(query).fetch();
                console.log("participants: " + participants.length, participants);
                this.participants = participants;
                this.groupParticipants();
            }));
        });
    }

    private groupParticipants():void {
        if (this.participants) {
            this.zone.run(() => {
                this.participants.sort(this.sortParticipantsByLastName.bind(this));
                this.unassignedParticipants = [];
                this.programGroups.forEach((group:IProgramGroup) => {
                    group.members = [];
                });
                this.participants.forEach((participant:Meteor.User) => {
                    if (participant.profile.groups && participant.profile.groups[this.program._id]) {
                        let participantGroupId:string = participant.profile.groups[this.program._id];
                        let programGroup:IProgramGroup = this.programGroups.find((group:IProgramGroup) => {
                            return group._id === participantGroupId;
                        });
                        if (programGroup) {
                            programGroup.members.push(participant);
                        }
                    } else {
                        this.unassignedParticipants.push(participant);
                    }
                });
            });
        }
    }

    private editGroup(group:IProgramGroup):void {
        var self = this;
        let modal = self.modalCtrl.create(EditGroupPage, group);
        modal.present();
    }

    private promptRemoveGroup(programGroup:IProgramGroup):void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-group-manager.alerts.removeGroup.title"),
            subTitle: programGroup.name,
            message: self.translate.instant("page-group-manager.alerts.removeGroup.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.remove"),
                handler: () => {
                    self.removeGroup(programGroup);
                }
            }]
        });
        alert.present();
    }

    private removeGroup(programGroup:IProgramGroup):void {
        var self = this;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("removeProgramGroup", programGroup, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("removeProgramGroup() error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-group-manager.errors.programGroup"),
                    subTitle: self.translate.instant("page-group-manager.errors.removeGroup"),
                    message: error.reason || error.message || error,
                    buttons: [{text: self.translate.instant("general.ok")}]
                });
                alert.present();
            } else {
                console.log("removeProgramGroup() result: ", result);
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-group-manager.toasts.removedGroup")
                });
            }
        });
    }

    private selectParticipants(programGroup:IProgramGroup):void {
        var self = this;
        let modal = self.modalCtrl.create(SelectGroupMembersModalPage, {
            unassignedParticipants: self.unassignedParticipants
        });
        modal.onDidDismiss((data:{selectedParticipantIds:Array<string>}) => {
            if (data) {
                console.log("selectedParticipantIds: ", data.selectedParticipantIds);
                self.assignGroupMembers({
                    groupId: programGroup._id,
                    selectedParticipantIds: data.selectedParticipantIds
                });
            }
        });
        modal.present();
    }

    private assignGroupMembers(data:{
        groupId:string,
        selectedParticipantIds:Array<string>
    }):void {
        var self = this;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("assignProgramGroupMembers", {
            programId: this.program._id,
            selectedParticipantIds: data.selectedParticipantIds,
            groupId: data.groupId
        }, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("assignProgramGroupMembers() error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-group-manager.errors.programGroup"),
                    subTitle: self.translate.instant("page-group-manager.errors.assignMembers"),
                    message: error.reason || error.message || error,
                    buttons: [{text: self.translate.instant("general.ok")}]
                });
                alert.present();
            } else {
                console.log("assignProgramGroupMembers() result: ", result);
            }
        });
    }

    private removeGroupMember(participantId:string) {
        var self = this;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("removeProgramGroupMember", {
            programId: this.program._id,
            participantId: participantId
        }, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("removeProgramGroupMember() error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-group-manager.errors.programGroup"),
                    subTitle: self.translate.instant("page-group-manager.errors.removeMember"),
                    message: error.reason || error.message || error,
                    buttons: [{text: self.translate.instant("general.ok")}]
                });
                alert.present();
            } else {
                console.log("removeProgramGroupMember() result: ", result);
            }
        });
    }

    private sortParticipantsByLastName(a, b):number {
        if (a.profile.name.family < b.profile.name.family) return -1;
        if (a.profile.name.family > b.profile.name.family) return 1;
        if (a.profile.name.given < b.profile.name.given) return -1;
        if (a.profile.name.given > b.profile.name.given) return 1;
        return 0;
    }
}
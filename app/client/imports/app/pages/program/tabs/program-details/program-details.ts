import {Component, OnInit, NgZone, ViewChild} from "@angular/core";
import {NavController, Navbar, AlertController, ModalController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {Constants} from "../../../../../../../both/Constants";
import {
    IProgramInfo,
    ProgramInfo,
    IEventPoints,
    EventPoints
} from "../../../../../../../both/models/program-info.model";
import {ProgramInfoService} from "../../../../services/program-info.service";
import * as moment from "moment";
import {SaveAsModalPage, ISaveAsModalPageReturnData} from "../../../../modals/save-as-modal/save-as-modal";
import {EventPointsCollection} from "../../../../../../../both/collections/event-points.collection";
import {Utils} from "../../../../../../../both/Utils";
import {GroupManagerPage} from "./group-manager/group-manager";
import {ImageService} from "../../../../utils/ImageService";
@Component({
    selector: "page-program-details",
    templateUrl: "program-details.html"
})
export class ProgramDetailsPage extends MeteorComponent implements OnInit {
    @ViewChild(Navbar) navBar:Navbar;
    public user:Meteor.User;
    public formGroup:FormGroup;
    public formControl:{
        programName:AbstractControl,
        kickoffDate:AbstractControl
    };

    public program:ProgramInfo;
    public imageUri:string;
    public placeholderImageUri:string = Constants.ADD_IMAGE_PLACEHOLDER_URI;
    private tracker:any;
    private initPicture:boolean = true;
    public minDate:string = moment().toISOString();
    public maxDate:string = moment().add(1, 'year').toISOString();

    // Program managers card
    public managers:Array<Meteor.User>;

    // Kickoff card
    public EVENT_POINTS:any = Constants.EVENT_POINTS;
    public COMPETITION_TYPE:any = Constants.COMPETITION_TYPE;

    public eventDetailItems:Array<IEventDetailItem>;

    public eventPointItems:Array<IEventDetailItem> = [{
        name: Constants.EVENT_POINTS.REGISTRATION,
        icon: "clipboard"
    }, {
        name: Constants.EVENT_POINTS.CALL,
        icon: "call"
    }, {
        name: Constants.EVENT_POINTS.TEXT,
        icon: "text"
    }, {
        name: Constants.EVENT_POINTS.COMMITMENT,
        icon: "fa-handshake-o",
        isFA: true
    }, {
        name: Constants.EVENT_POINTS.PAID,
        icon: "fa-usd",
        isFA: true
    }, {
        name: Constants.EVENT_POINTS.CONTACT_LIST_GOAL,
        icon: "fa-address-card",
        isFA: true
    }, {
        name: Constants.EVENT_POINTS.CONTACT_GOAL,
        icon: "fa-paper-plane-o",
        isFA: true
    }, {
        name: Constants.EVENT_POINTS.SALES_GOAL,
        icon: "trophy",
    }];

    public savedPoints:Array<IEventPoints>;
    public savedEventPointsId:string = "0";
    public savedEventPointsDialogOptions:any;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService,
                public fb:FormBuilder,
                private programInfoService:ProgramInfoService,
                private imageService:ImageService) {
        super();
    }

    ngOnInit() {
        this.program = this.programInfoService.getProgramInfo();
        // console.log("program: ", this.program);

        this.eventDetailItems = [{
            name: "duration",
            icon: "timer",
            onClick: this.setEventDuration.bind(this)
        }, {
            name: Constants.EVENT_POINTS.CONTACT_LIST_GOAL,
            icon: "fa-address-card",
            isFA: true,
            onClick: this.setContactListGoal.bind(this)
        }, {
            name: "salesGoal",
            icon: "fa-usd",
            isFA: true,
            onClick: this.setSalesGoal.bind(this)
        }, {
            name: "salesGoalTotal",
            icon: "fa-usd",
            isFA: true,
            onClick: this.setSalesGoalTotal.bind(this)
        }];


        if (!this.program.eventDate) {
            this.program.eventDate = moment().toISOString();
        }

        this.formGroup = this.fb.group({
            'programName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
            ])],
            'kickoffDate': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])]
        });

        this.formControl = {
            programName: this.formGroup.controls['programName'],
            kickoffDate: this.formGroup.controls['kickoffDate']
        };

        this.savedEventPointsDialogOptions = {
            title: this.translate.instant("page-program-details.savedEventPointsDialogOptions.title"),
            subTitle: this.translate.instant("page-program-details.savedEventPointsDialogOptions.subTitle")
        };

        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.subscribe(Constants.PUBLICATIONS.EVENT_POINTS, () => {
                    // console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.EVENT_POINTS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        this.savedPoints = EventPointsCollection.find({userId: this.user._id}).fetch();
                    }));
                });
            }
        });
    }

    private onSelectSavedEventPoints():void {
        let savedPoints:IEventPoints = this.savedPoints.find((points:IEventPoints) => points._id === this.savedEventPointsId);
        if (savedPoints) {
            let points:EventPoints = new EventPoints(savedPoints);
            delete points._id;
            delete points.name;
            delete points.userId;
            this.program.eventPoints = points;
        }
    }

    private goBack():void {
        this.nav.parent.viewCtrl.dismiss();
    }

    public onSubmit():void {
        let self = this;
        if (self.formGroup.valid) {
            self.program.imageUri = this.imageUri;
            self.programInfoService.upsert(self.program, (error, programInfo:IProgramInfo) => {
                if (error) {
                    // console.log("Error saving program info: " + error.reason);
                    new ToastMessenger().toast({
                        type: "error",
                        message: error.reason
                    });
                } else {
                    // console.log("editProgramInfo() result: ", programInfo);
                    new ToastMessenger().toast({
                        type: "success",
                        message: "Successfully saved program information."
                    });
                    self.programInfoService.setProgramInfo(programInfo);
                    self.program = self.programInfoService.getProgramInfo();
                }
            });
        }
    }

    private setEventPoints(eventPointsKey:string):void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-kickoff.alerts.eventPoints.title"),
            subTitle: self.translate.instant("page-program-kickoff.eventPoints." + eventPointsKey),
            message: self.translate.instant("page-program-kickoff.alerts.eventPoints.message"),
            inputs: [{
                name: 'points',
                type: 'number',
                value: self.program.eventPoints[eventPointsKey],
                placeholder: self.translate.instant("page-program-kickoff.alerts.eventPoints.inputPlaceholder")
            }],
            buttons: [{
                text: self.translate.instant("general.cancel")
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    if (!data.points || data.points < 0) {
                        data.points = 0;
                    }
                    self.program.eventPoints[eventPointsKey] = Number(data.points);
                }
            }]
        });
        alert.present();
    }

    private setContactListGoal():void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-kickoff.alerts.contactListGoal.title"),
            message: self.translate.instant("page-program-kickoff.alerts.contactListGoal.message"),
            inputs: [{
                name: 'points',
                type: 'number',
                value: self.program.contactListGoal.toString(),
                placeholder: self.translate.instant("page-program-kickoff.alerts.contactListGoal.inputPlaceholder")
            }],
            buttons: [{
                text: self.translate.instant("general.cancel")
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    if (!data.points || data.points < 0) {
                        data.points = 0;
                    }
                    self.program.contactListGoal = Number(data.points);
                }
            }]
        });
        alert.present();
    }

    private setSalesGoal():void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-kickoff.alerts.salesGoal.title"),
            message: self.translate.instant("page-program-kickoff.alerts.salesGoal.message"),
            inputs: [{
                name: 'goal',
                type: 'number',
                value: self.program.salesGoal.toString(),
                placeholder: self.translate.instant("page-program-kickoff.alerts.salesGoal.inputPlaceholder")
            }],
            buttons: [{
                text: self.translate.instant("general.cancel")
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    if (!data.goal || data.goal < 0) {
                        data.goal = 0;
                    }
                    self.program.salesGoal = Number(data.goal);
                }
            }]
        });
        alert.present();
    }

    private setSalesGoalTotal():void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-kickoff.alerts.salesGoalTotal.title"),
            message: self.translate.instant("page-program-kickoff.alerts.salesGoalTotal.message"),
            inputs: [{
                name: 'goal',
                type: 'number',
                value: self.program.salesGoalTotal.toString(),
                placeholder: self.translate.instant("page-program-kickoff.alerts.salesGoalTotal.inputPlaceholder")
            }],
            buttons: [{
                text: self.translate.instant("general.cancel")
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    if (!data.goal || data.goal < 0) {
                        data.goal = 0;
                    }
                    self.program.salesGoalTotal = Number(data.goal);
                }
            }]
        });
        alert.present();
    }

    private setEventDuration():void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-kickoff.alerts.duration.title"),
            message: self.translate.instant("page-program-kickoff.alerts.duration.message"),
            inputs: [{
                name: 'duration',
                type: 'number',
                value: self.program.duration.toString(),
                placeholder: self.translate.instant("page-program-kickoff.alerts.duration.inputPlaceholder")
            }],
            buttons: [{
                text: self.translate.instant("general.cancel")
            }, {
                text: self.translate.instant("general.ok"),
                handler: data => {
                    if (!data.duration || data.duration < 1) {
                        data.duration = 59;
                    }
                    self.program.duration = Number(data.duration);
                }
            }]
        });
        alert.present();
    }

    private promptSaveMyPoints():void {
        let self = this;
        let modal = self.modalCtrl.create(SaveAsModalPage, {
            existingList: self.savedPoints
        });
        modal.onDidDismiss((data:ISaveAsModalPageReturnData) => {
            if (data) {
                // console.log("save as data: ", data);
                let points:EventPoints = new EventPoints(self.program.eventPoints);
                if (data.existingId && !data.itemTitle) {
                    let savedPoints:IEventPoints = self.savedPoints.find((points:IEventPoints) => {
                        return points._id === data.existingId;
                    });
                    points._id = savedPoints._id;
                    points.name = savedPoints.name;
                } else if (data.itemTitle) {
                    points.name = data.itemTitle;
                }
                points.userId = self.user._id;
                self.saveMyPoints(points);
            }
        });
        modal.present();
    }

    private saveMyPoints(points:EventPoints):void {
        let self = this;
        // console.log("Saving points...");
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("saveMyPoints", points, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                // console.error("saveMyPoints() error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-program-details.errors.myPoints"),
                    message: error.reason || error.message || error,
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }]
                });
                alert.present();
            } else {
                // console.log("saveMyPoints() result: ", result);
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-program-details.toasts.savedMyPoints")
                });
            }
        });
    }

    private promptRemoveMyPoints():void {
        let self = this;
        let inputs:Array<any> = [];
        self.savedPoints.forEach((points:IEventPoints) => {
            let input:any = {
                type: 'radio',
                label: points.name,
                value: points._id
            };
            inputs.push(input);
        });

        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-details.alerts.promptRemoveMyPoints.title"),
            message: self.translate.instant("page-program-details.alerts.promptRemoveMyPoints.message"),
            inputs: inputs,
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.remove"),
                handler: (id:string) => {
                    self.removeMyPoints(id);
                }
            }],
        });
        alert.present();
    }

    private removeMyPoints(id:string):void {
        let self = this;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("removeMyPoints", {id: id}, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                // console.error("removeMyPoints() error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-program-details.errors.myPoints"),
                    message: error.reason || error.message || error,
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }]
                });
                alert.present();
            } else {
                // console.log("removeMyPoints() result: ", result);
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-program-details.toasts.removedMyPoints")
                });
            }
        });
    }

    private selectCompetitionType():void {
        let self = this;
        let inputs:Array<any> = [];
        Object.keys(Constants.COMPETITION_TYPE).forEach((key) => {
            let input = {
                type: "radio",
                value: Constants.COMPETITION_TYPE[key],
                label: Utils.capitalizeFirstLetter(
                    self.translate.instant("page-program-details.labels." + Constants.COMPETITION_TYPE[key])),
                checked: self.program.competitionType === Constants.COMPETITION_TYPE[key]
            };
            inputs.push(input);
        });
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-details.labels.competitionType"),
            inputs: inputs,
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.ok"),
                handler: (type:string) => {
                    self.program.competitionType = type;
                }
            }]
        });
        alert.present();
    }

    private manageGroups():void {
        this.nav.push(GroupManagerPage)
    }

    public confirmNewOnlineCampaign():void {
        let self = this;
        let alert = this.alertCtrl.create({
            title: self.translate.instant("page-program-details.alerts.confirmNewOnlineCampaign.title"),
            message: self.translate.instant("page-program-details.alerts.confirmNewOnlineCampaign.message"),
            buttons: [{
                text: self.translate.instant("general.no")
            }, {
                text: self.translate.instant("general.yes"),
                handler: data => {

                }
            }]
        });
        alert.present();
    }

    /*Life Cycle*/
    ionViewWillEnter() {
        this.program = this.programInfoService.getProgramInfo();
        this.tracker = Tracker.autorun(() => this.zone.run(() => {
            if (this.initPicture) {
                this.initPicture = false;
                Session.set(Constants.SESSION.imageUri, this.program.imageUri);
            }
            this.imageUri = Session.get(Constants.SESSION.imageUri);
        }));
    }

    ionViewWillLeave() {
        // stop tracker so item does not continue to update
        this.tracker.stop();
    }

    ionViewDidLoad() {
        this.navBar.backButtonClick = (e:UIEvent) => {
            this.nav.parent.viewCtrl.dismiss();
        };
    }
}

interface IEventDetailItem {
    name:string,
    icon:string,
    isFA?:boolean,
    onClick?:Function,
    hide?:boolean
}
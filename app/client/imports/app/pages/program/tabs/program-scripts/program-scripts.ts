import {Component, OnInit, ViewChild, NgZone} from "@angular/core";
import {NavController, Navbar, AlertController, ModalController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {ProgramInfoService} from "../../../../services/program-info.service";
import {
    IProgramInfo,
    ProgramInfo,
    IProgramScripts,
    ProgramScripts
} from "../../../../../../../both/models/program-info.model";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {Constants} from "../../../../../../../both/Constants";
import {ProgramsCollection} from "../../../../../../../both/collections/programs.collection";
import {MyScriptsCollection} from "../../../../../../../both/collections/my-scripts.collection";
import {SaveAsModalPage, ISaveAsModalPageReturnData} from "../../../../modals/save-as-modal/save-as-modal";

declare var Roles;

@Component({
    selector: "page-program-scripts",
    templateUrl: "program-scripts.html"
})
export class ProgramScriptsPage extends MeteorComponent implements OnInit {
    @ViewChild(Navbar) navBar:Navbar;
    public user:Meteor.User;
    public program:ProgramInfo;
    public otherPrograms:Array<IProgramInfo>;
    public savedScripts:Array<IProgramScripts>;
    public otherProgramScriptsId:string = "0";
    public savedScriptId:string = "0";
    public otherScriptDialogOptions:any;
    public savedScriptDialogOptions:any;
    public scriptKeys:any = Constants.SCRIPT_KEYS;
    public scriptVariables:Array<string>;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService,
                private programInfoService:ProgramInfoService) {
        super();
    }

    ngOnInit() {
        this.program = this.programInfoService.getProgramInfo();
        this.scriptVariables = [
            "programName",
            "duration",
            "salesGoal",
            "productName",
            "quantity",
            "totalCost",
            "paymentLink",
            "customerName",
            "myPhoneNumber",
            "me"
        ];
        this.autorun(() => {
            console.log("ProgramScriptsPage autorun()");
            this.user = Meteor.user();
            if (this.user) {
                this.subscribe(Constants.PUBLICATIONS.MANAGED_PROGRAMS, () => {
                    console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MANAGED_PROGRAMS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        console.log("HomePage subscription autorun");
                        var membershipProgramIds:Array<string> = [];
                        if (this.user && this.user["roles"]) {
                            var programIds:Array<string> = Object.keys(this.user["roles"]) || [];
                            programIds.forEach((programId:string) => {
                                if (Roles.userIsInRole(this.user._id, [
                                        Constants.ROLES.ADMIN,
                                        Constants.ROLES.PROGRAM_MANAGEMENT
                                    ], programId)) {
                                    membershipProgramIds.push(programId);
                                }
                            });
                        }
                        var programs:Array<IProgramInfo> = ProgramsCollection.find({
                            _id: {
                                $in: membershipProgramIds
                            }
                        }).fetch();
                        console.log("programs: ", programs);
                        var otherPrograms:Array<IProgramInfo> = new Array();
                        programs.forEach((program:IProgramInfo) => {
                            if (program._id !== this.program._id) {
                                otherPrograms.push(program);
                            }
                        });

                        this.otherPrograms = otherPrograms;
                    }));
                });

                this.subscribe(Constants.PUBLICATIONS.MY_SCRIPTS, () => {
                    console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MY_SCRIPTS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        this.savedScripts = MyScriptsCollection.find({userId: this.user._id}).fetch();
                    }));
                });
            }
        });

        this.otherScriptDialogOptions = {
            title: this.translate.instant("page-program-scripts.otherScriptDialogOptions.title"),
            subTitle: this.translate.instant("page-program-scripts.otherScriptDialogOptions.subTitle")
        };
        this.savedScriptDialogOptions = {
            title: this.translate.instant("page-program-scripts.savedScriptDialogOptions.title"),
            subTitle: this.translate.instant("page-program-scripts.savedScriptDialogOptions.subTitle")
        };
    }

    ionViewWillEnter() {
        this.program = this.programInfoService.getProgramInfo();
    }

    ionViewDidLoad() {
        this.navBar.backButtonClick = (e:UIEvent) => {
            this.nav.parent.viewCtrl.dismiss();
        };
    }

    private onSubmit():void {
        var self = this;
        self.programInfoService.upsert(self.program, (error, result) => {
            //TODO use translated messages
            if (error) {
                console.log("Error saving program info: " + error.reason);

                new ToastMessenger().toast({
                    type: "error",
                    message: error.reason
                });
            } else {
                console.log("editProgramInfo() result: ", result);
                new ToastMessenger().toast({
                    type: "success",
                    message: "Successfully saved program information."
                });
            }
        });
    }

    private onSelectOtherScripts():void {
        var otherProgram:IProgramInfo = this.otherPrograms.find((program:IProgramInfo) => program._id === this.otherProgramScriptsId);
        if (otherProgram) {
            this.program.scripts = otherProgram.scripts;
        }
    }

    private onSelectSavedScripts():void {
        var savedScript:IProgramScripts = this.savedScripts.find((script:IProgramScripts) => script._id === this.savedScriptId);
        if (savedScript) {
            let script:ProgramScripts = new ProgramScripts(savedScript);
            delete script._id;
            delete script.name;
            delete script.userId;
            this.program.scripts = script;
        }
    }

    private clearAllScripts():void {
        this.confirmClearAllScriptsAlert();
    }

    private loadDefaultScripts():void {
        this.confirmLoadAllDefaultScriptsAlert();
    }

    private clearScript(script:string):void {
        this.confirmClearScriptAlert(script);
    }

    private loadDefaultScript(script:string):void {
        this.confirmLoadDefaultScriptAlert(script);
    }

    private confirmLoadDefaultScriptAlert(script:string):void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-scripts.alerts.confirmLoadDefaultScript.title"),
            message: self.translate.instant("page-program-scripts.defaultScripts." + script),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.ok"),
                handler: () => {
                    self.otherProgramScriptsId = Constants.EMPTY_STRING;
                    self.savedScriptId = Constants.EMPTY_STRING;
                    self.program.scripts[script] = self.translate.instant("page-program-scripts.defaultScripts." + script);
                }
            }],
        });
        alert.present();
    }

    private confirmLoadAllDefaultScriptsAlert():void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-scripts.alerts.confirmLoadAllDefaultScripts.title"),
            message: self.translate.instant("page-program-scripts.alerts.confirmLoadAllDefaultScripts.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.ok"),
                handler: () => {
                    self.otherProgramScriptsId = Constants.EMPTY_STRING;
                    self.savedScriptId = Constants.EMPTY_STRING;
                    self.program.scripts = self.translate.instant("page-program-scripts.defaultScripts");
                }
            }],
        });
        alert.present();
    }

    private confirmClearScriptAlert(script:string):void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-scripts.alerts.confirmClearScript.title"),
            message: self.translate.instant("page-program-scripts.alerts.confirmClearScript.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.ok"),
                handler: () => {
                    self.otherProgramScriptsId = Constants.EMPTY_STRING;
                    self.savedScriptId = Constants.EMPTY_STRING;
                    self.program.scripts[script] = Constants.EMPTY_STRING;
                }
            }],
        });
        alert.present();
    }

    private confirmClearAllScriptsAlert():void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-scripts.alerts.confirmClearAllScripts.title"),
            message: self.translate.instant("page-program-scripts.alerts.confirmClearAllScripts.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.ok"),
                handler: () => {
                    self.zone.run(() => {
                        self.otherProgramScriptsId = Constants.EMPTY_STRING;
                        self.savedScriptId = Constants.EMPTY_STRING;
                        self.program.scripts = {
                            callToAction: Constants.EMPTY_STRING,
                            call: Constants.EMPTY_STRING,
                            text: Constants.EMPTY_STRING,
                            email: Constants.EMPTY_STRING,
                            orderConfirmation: Constants.EMPTY_STRING
                        };
                    });
                }
            }],
        });
        alert.present();
    }

    private promptSaveScripts():void {
        var self = this;
        let modal = self.modalCtrl.create(SaveAsModalPage, {
            existingList: self.savedScripts
        });
        modal.onDidDismiss((data:ISaveAsModalPageReturnData) => {
            if (data) {
                console.log("save as data: ", data);
                let scripts:ProgramScripts = new ProgramScripts(self.program.scripts);
                if (data.existingId && !data.itemTitle) {
                    let savedScript:IProgramScripts = self.savedScripts.find((script:IProgramScripts) => {
                        return script._id === data.existingId;
                    });
                    scripts._id = savedScript._id;
                    scripts.name = savedScript.name;
                } else if (data.itemTitle) {
                    scripts.name = data.itemTitle;
                }
                scripts.userId = self.user._id;
                self.saveScripts(scripts);
            }
        });
        modal.present();
    }

    private saveScripts(scripts:IProgramScripts):void {
        var self = this;
        console.log("Saving scripts...");
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("saveMyScripts", scripts, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("saveMyScripts() error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-program-scripts.errors.myScripts"),
                    message: error.reason || error.message || error,
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }]
                });
                alert.present();
            } else {
                console.log("saveMyScripts() result: ", result);
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-program-scripts.toasts.savedScripts")
                });
            }
        });
    }

    private promptRemoveScripts():void {
        var self = this;
        let inputs:Array<any> = [];
        self.savedScripts.forEach((script:IProgramScripts) => {
            let input:any = {
                type: 'radio',
                label: script.name,
                value: script._id
            };
            inputs.push(input);
        });

        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-program-scripts.alerts.promptRemoveScripts.title"),
            message: self.translate.instant("page-program-scripts.alerts.promptRemoveScripts.message"),
            inputs: inputs,
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.remove"),
                handler: (scriptId:string) => {
                    self.removeScripts(scriptId);
                }
            }],
        });
        alert.present();
    }

    private removeScripts(scriptId:string):void {
        var self = this;
        console.log("remove scriptId: ", scriptId);
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("removeMyScripts", {scriptId: scriptId}, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("removeMyScripts() error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-program-scripts.errors.myScripts"),
                    message: error.reason || error.message || error,
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }]
                });
                alert.present();
            } else {
                console.log("removeMyScripts() result: ", result);
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-program-scripts.toasts.removedScripts")
                });
            }
        });
    }
}
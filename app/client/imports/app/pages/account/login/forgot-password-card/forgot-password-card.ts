import {Component, NgZone} from '@angular/core';
import {NavController, Alert} from 'ionic-angular/es2015';
import {FormBuilder, Validators, AbstractControl, FormGroup} from '@angular/forms';
import {MeteorComponent} from 'angular2-meteor';
import {Constants} from "../../../../../../../both/Constants";
import {FormValidator} from "../../../../utils/FormValidator";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: "forgot-password-card",
    templateUrl: "forgot-password-card.html"
})
export class ForgotPasswordCardComponent extends MeteorComponent {
    public forgotPwForm:FormGroup;
    public formControl:{
        // mobilePhone:AbstractControl,
        email:AbstractControl
    };
    // public mobilePhone:string;
    public email:string;
    public sentEmail:boolean = false;

    constructor(public nav:NavController,
                public zone:NgZone,
                public fb:FormBuilder,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.forgotPwForm = this.fb.group({
            // 'mobilePhone': [Constants.EMPTY_STRING, Validators.compose([
            //     Validators.required,
            //     FormValidator.registered,
            //     Validators.minLength(7),
            //     Validators.maxLength(11),
            //     FormValidator.validPhoneLength
            // ])],
            'email': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.registered,
                FormValidator.validEmail
            ])],

        });

        this.formControl = {
            // mobilePhone: this.forgotPwForm.controls['mobilePhone'],
            email: this.forgotPwForm.controls['email']
        };

        Meteor.defer(() => {
            this.autorun(() => this.zone.run(() => {
                Session.get(Constants.SESSION.REGISTERED_ERROR);
                // this.mobilePhone = Session.get(Constants.SESSION.MOBILE_PHONE) || null;
                this.email = Session.get(Constants.SESSION.EMAIL) || null;
            }));
        });
    }

    // private removeNonDigits():void {
    //     var regex = /^\d*$/;
    //     var value:string = this.mobilePhone;
    //     if (value && !regex.test(value)) {
    //         var result = value.replace(/[^0-9]/g, Constants.EMPTY_STRING);
    //         this.mobilePhone = result;
    //     }
    // }

    public sendPasswordResetCode():void {
        var self = this;
        if (this.forgotPwForm.valid) {
            Session.set(Constants.SESSION.EMAIL, this.email);
            Session.set(Constants.SESSION.LOADING, true);
            Accounts.forgotPassword({email: this.email.toLowerCase()}, function (error) {
                Session.set(Constants.SESSION.LOADING, false);
                if (error) {
                    console.log("Error sending forgot password email: " + JSON.stringify(error));
                    if (error.reason && error.reason === Constants.METEOR_ERRORS.USER_NOT_FOUND) {
                        console.log("User not found");
                        Session.set(Constants.SESSION.REGISTERED_ERROR, true);
                        self.formControl.email.updateValueAndValidity({onlySelf: true});
                    } else {
                        new ToastMessenger().toast({
                            type: "error",
                            message: error.reason || error.message || error,
                            title: self.translate.instant("forgot-password-card.errors.passwordReset")
                        });
                    }
                } else {
                    console.log("Sending password reset email...");
                    // Session.set(Constants.SESSION.FORGOT_PASSWORD, !Session.get(Constants.SESSION.FORGOT_PASSWORD));
                    // Session.set(Constants.SESSION.RESET_PASSWORD, !Session.get(Constants.SESSION.RESET_PASSWORD));
                    self.zone.run(() => {
                        self.sentEmail = true;
                    });
                }
            });
        }
    }

    public showSignInCard() {
        Session.set(Constants.SESSION.FORGOT_PASSWORD, !Session.get(Constants.SESSION.FORGOT_PASSWORD));
        // Session.set(Constants.SESSION.MOBILE_PHONE, this.mobilePhone);
        Session.set(Constants.SESSION.EMAIL, this.email);
    }
}
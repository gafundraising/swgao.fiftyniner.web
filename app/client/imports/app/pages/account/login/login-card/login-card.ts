import {Component, OnInit, NgZone} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular/es2015';
import {FormBuilder, Validators, AbstractControl, FormGroup} from '@angular/forms';
import {MeteorComponent} from 'angular2-meteor';
import {HomePage} from '../../../home/home';
import {Constants} from "../../../../../../../both/Constants";
import {FormValidator} from "../../../../utils/FormValidator";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {TranslateService} from "@ngx-translate/core";

declare var device;
declare var Meteor; // Property 'loginWithToken' does not exist on type 'typeof Meteor'.

import {LandingPage} from "../../../landing/landing";
@Component({
    selector: "login-card",
    templateUrl: "login-card.html"
})
export class LoginCardComponent extends MeteorComponent implements OnInit {
    public loginForm:FormGroup;
    public formControl:{
        // mobilePhone:AbstractControl,
        email:AbstractControl,
        password:AbstractControl
    };
    public loginInputs:{
        // mobilePhone:string,
        email:string,
        password:string
    };
    public isCordova:boolean;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public fb:FormBuilder,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.isCordova = Meteor.isCordova;
        this.loginInputs = {
            // mobilePhone: Constants.EMPTY_STRING,
            email: Constants.EMPTY_STRING,
            password: Constants.EMPTY_STRING
        };
        this.loginForm = this.fb.group({
            // 'mobilePhone': [Constants.EMPTY_STRING, Validators.compose([
            //     Validators.required,
            //     FormValidator.registered,
            //     Validators.minLength(7),
            //     Validators.maxLength(11),
            //     FormValidator.validPhoneLength
            // ])],
            'email': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.registered,
                FormValidator.validEmail
            ])],
            "password": [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.validPassword
            ])]
        });

        this.formControl = {
            // mobilePhone: this.loginForm.controls['mobilePhone'],
            email: this.loginForm.controls['email'],
            password: this.loginForm.controls['password']
        };

        // this.autorun(() => {
        //     if (Meteor.user()) {
        //         this.zone.run(() => {
        //             this.nav.setRoot(HomePage);
        //         });
        //     }
        // });

        Meteor.defer(() => {
            this.autorun(() => this.zone.run(() => {
                Session.get(Constants.SESSION.REGISTERED_ERROR);
                Session.get(Constants.SESSION.INCORRECT_PASSWORD);
                // this.loginInputs.mobilePhone = Session.get(Constants.SESSION.MOBILE_PHONE) || null;
                this.loginInputs.email = Session.get(Constants.SESSION.EMAIL) || null;
            }));
        });
    }

    // private removeNonDigits():void {
    //     var regex = /^\d*$/;
    //     var value:string = this.loginInputs.mobilePhone;
    //     if (value && !regex.test(value)) {
    //         var result = value.replace(/[^0-9]/g, Constants.EMPTY_STRING);
    //         this.loginInputs.mobilePhone = result;
    //     }
    // }

    public onSubmit():void {
        var self = this;
        if (self.loginForm.valid) {
            // Session.set(Constants.SESSION.MOBILE_PHONE, self.loginInputs.mobilePhone);
            Session.set(Constants.SESSION.EMAIL, self.loginInputs.email);

            Session.set(Constants.SESSION.LOADING, true);
            Meteor.loginWithPassword({
                    // username: self.loginInputs.mobilePhone
                    email: self.loginInputs.email.toLowerCase()
                },
                self.loginInputs.password,
                (error) => {
                    Session.set(Constants.SESSION.LOADING, false);
                    if (error) {
                        console.log("loginWithPassword Error: " + JSON.stringify(error));
                        var toastMessage = null;
                        if (error.reason) {
                            if (error.reason === Constants.METEOR_ERRORS.INCORRECT_PASSWORD) {
                                console.log("Incorrect password");
                                Session.set(Constants.SESSION.INCORRECT_PASSWORD, true);
                                self.formControl.password.updateValueAndValidity({onlySelf: true});
                            } else if (error.reason === Constants.METEOR_ERRORS.USER_NOT_FOUND) {
                                console.log("User not found");
                                Session.set(Constants.SESSION.REGISTERED_ERROR, true);
                                // self.formControl.mobilePhone.updateValueAndValidity({onlySelf: true});
                                self.formControl.email.updateValueAndValidity({onlySelf: true});
                            } else if (error.reason === Constants.METEOR_ERRORS.NO_PASSWORD) {
                                toastMessage = self.translate.instant("login-card.errors.noPassword");
                            } else {
                                toastMessage = error.reason;
                            }
                        } else {
                            toastMessage = error.message;
                        }
                        if (toastMessage) {
                            new ToastMessenger().toast({
                                type: "error",
                                message: toastMessage,
                                title: self.translate.instant("login-card.errors.signIn")
                            });
                        }
                    } else {
                        console.log("Successfully logged in with password.");
                        this.zone.run(() => {
                            this.nav.setRoot(LandingPage);
                        });
                    }
                }
            );
        }
    }

    public showForgotPasswordCard():void {
        Session.set(Constants.SESSION.FORGOT_PASSWORD, true);
        // Session.set(Constants.SESSION.MOBILE_PHONE, this.loginInputs.mobilePhone);
        Session.set(Constants.SESSION.EMAIL, this.loginInputs.email);
    }

    public showCreateAccountCard():void {
        Session.set(Constants.SESSION.CREATE_ACCOUNT, true);
        // Session.set(Constants.SESSION.MOBILE_PHONE, this.loginInputs.mobilePhone);
        Session.set(Constants.SESSION.EMAIL, this.loginInputs.email);
    }
}
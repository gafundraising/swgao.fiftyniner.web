import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, MenuController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {LoginPage} from "../account/login/login";
import {HomePage} from "../home/home";

@Component({
    selector: "page-landing",
    templateUrl: "landing.html"
})
export class LandingPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;

    constructor(public nav:NavController,
                public menu:MenuController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
        this.menu.swipeEnable(false);
    }

    ngOnInit() {
        var path:string = Session.get(Constants.SESSION.PATH);
        var urlParams:any = Session.get(Constants.SESSION.URL_PARAMS);

        if (path === "/" + Constants.ROUTES.RESET_PASSWORD && urlParams) {
            console.log("received password reset token: ", urlParams.token);
            Session.set(Constants.SESSION.RESET_PASSWORD_TOKEN, urlParams.token);
            Session.set(Constants.SESSION.RESET_PASSWORD, true);
            window.history.pushState('', document.title, Meteor.absoluteUrl());
            Session.set(Constants.SESSION.PATH, "/");
            Session.set(Constants.SESSION.URL_PARAMS, null);
        }

        this.autorun(() => {
            this.user = Meteor.user();
        });

        Session.set(Constants.SESSION.LOADING, true);
        setTimeout(() => {
            Session.set(Constants.SESSION.LOADING, false);
            this.setRootPage();
        }, 2500);
    }

    private setRootPage():void {
        if (Session.get(Constants.SESSION.RESET_PASSWORD) || !this.user) {
            this.nav.setRoot(LoginPage);
        } else {
            this.nav.setRoot(HomePage);
        }
    }

    ionViewWillLeave() {
        this.menu.swipeEnable(true);
    }
}
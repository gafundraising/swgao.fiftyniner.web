import {Component, ElementRef, NgZone, OnInit, ViewChild} from "@angular/core";
import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {ProgramInfoService} from "../../services/program-info.service";
import {IEventPoints, IProgramInfo, ProgramInfo} from "../../../../../both/models/program-info.model";
import {ToastMessenger} from "../../utils/ToastMessenger";
import {Constants} from "../../../../../both/Constants";
import {EventTotalsCollection} from "../../../../../both/collections/event-totals.collection";
import {EventTotals, IEventTotals} from "../../../../../both/models/event-totals.model";
import {IParticipantTotals, ParticipantTotals} from "../../../../../both/models/participant-totals.model";
import {IMyListContact} from "../../../../../both/models/my-list-contact.model";
import {ParticipantTotalsCollection} from "../../../../../both/collections/participant-totals.collection";
import {MyListContactsCollection} from "../../../../../both/collections/my-list-contacts";
import * as moment from "moment";
import {IntervalTimer} from "../../utils/IntervalTimer";
import {IProgramGroup} from "../../../../../both/models/program-group.model";
import {ProgramGroupsCollection} from "../../../../../both/collections/program-groups.collection";

declare var ProgressBar;

@Component({
    selector: "page-leaderboard",
    templateUrl: "leaderboard.html"
})
export class LeaderboardPage extends MeteorComponent implements OnInit {
    public user: Meteor.User;
    public program: ProgramInfo;
    public gaoLogo: string = Constants.GAO_LOGO;
    public appLogo: string = Constants.APP_LOGO;
    private duration: {
        days?: number,
        hours?: number,
        minutes: number,
        seconds: number
    };
    public eventTotals: IEventTotals = {};
    private participants: Array<Meteor.User>;
    private participantsTotals: Array<IParticipantTotals>;
    public participantsData: Array<any>;
    private participantsListContacts: Array<IMyListContact>;
    private intervalTimer: IntervalTimer;
    public isTimerPaused: boolean = false;
    private subMyListContacts: Meteor.SubscriptionHandle;
    public COMPETITION_TYPE: any = Constants.COMPETITION_TYPE;
    public programGroups: Array<IProgramGroup>;
    public progressBackgroundImageOpacity: number = 0.2;
    public commitmentProgressInfo: any = {
        imageUri: "/images/icon_communication_award.png",
        color: "#222C6A"
    };
    public purchaseProgressInfo: any = {
        imageUri: "/images/icon_sales_goal_award.png",
        color: "#F36C24"
    };
    @ViewChild('commitmentProgressElement') commitmentProgressElement: ElementRef;
    @ViewChild('purchaseProgressElement') purchaseProgressElement: ElementRef;
    public commitmentProgressBar: any;
    public purchaseProgressBar: any;

    constructor(public nav: NavController,
                public zone: NgZone,
                public translate: TranslateService,
                private programInfoService: ProgramInfoService) {
        super();
    }

    ngOnInit() {
        this.program = this.programInfoService.getProgramInfo();
        this.setCountDownTimer();

        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));

        this.subscribe(Constants.PUBLICATIONS.EVENT_TOTALS, this.program._id, () => {
            console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.EVENT_TOTALS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                let eventTotals: IEventTotals = EventTotalsCollection.findOne({_id: this.program._id});
                console.log("Event Totals: ", eventTotals);
                if (eventTotals && this.participants) {
                    eventTotals.participants = this.participants.length;
                }
                if (eventTotals) {
                    eventTotals.commitmentProfit = Number(this.program.contract.profitPercent) * eventTotals.commitmentSales;
                    eventTotals.purchasedProfit = Number(this.program.contract.profitPercent) * eventTotals.purchasedSales;
                }
                this.eventTotals = new EventTotals(eventTotals);
                this.configureSalesTotalProgressBars();
            }));
        });

        this.subscribe(Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS, this.program._id, () => {
            console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                this.getParticipantsContacts();
                let queryKey: string = "roles." + this.program._id;
                let query: any = {};
                query[queryKey] = {
                    $in: [Constants.ROLES.MEMBER]
                };
                let participants: Array<Meteor.User> = Meteor.users.find(query).fetch();
                console.log("participants: " + participants.length, participants);
                this.participants = participants;
                if (this.eventTotals && participants.length > 0) {
                    this.eventTotals.participants = participants.length;
                }
                this.joinParticipantsData();
            }));
        });

        this.subscribe(Constants.PUBLICATIONS.PARTICIPANT_TOTALS, this.program._id, () => {
            console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PARTICIPANT_TOTALS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                // query aggregated collection
                let participantTotals: Array<IParticipantTotals> = ParticipantTotalsCollection.find({}, {
                    sort: {
                        points: -1,
                        lastUpdated: 1
                    }
                }).fetch();
                console.log("participant totals: " + participantTotals.length, participantTotals);
                this.participantsTotals = participantTotals;
                this.joinParticipantsData();
            }));
        });

        if (this.program.competitionType === Constants.COMPETITION_TYPE.GROUP) {
            this.subscribe(Constants.PUBLICATIONS.PROGRAM_GROUPS, this.program._id, () => {
                console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_GROUPS + " <><><>");
                this.autorun(() => this.zone.run(() => {
                    let programGroups: Array<IProgramGroup> = ProgramGroupsCollection.find({
                        programId: this.program._id
                    }).fetch();
                    console.log("program groups: ", programGroups);
                    this.programGroups = programGroups;
                    this.joinParticipantsData();
                }));
            });
        }

        this.setVisibilityListener();
    }

    private configureCommitmentProgressBar(): void {
        this.commitmentProgressInfo.current = this.eventTotals.commitments;
        this.configureProgressBar({
            progressInfo: this.commitmentProgressInfo,
            progressElement: this.commitmentProgressElement
        });
    }

    private configurePurchasedProgressBar(): void {
        this.purchaseProgressInfo.current = this.eventTotals.purchases;
        this.configureProgressBar({
            progressInfo: this.purchaseProgressInfo,
            progressElement: this.purchaseProgressElement
        });
    }

    private configureProgressBar(data: { progressInfo: any, progressElement: any }): void {
        data.progressInfo.goal = this.program.salesGoalTotal;
        if (data.progressInfo.goal) {
            let progressText: string = "<div style='color:" + data.progressInfo.color + "; text-align:center; font-size:36px;'>" +
                data.progressInfo.current +
                "</div><div style='color:#333; text-align:center; font-size:22px;'>----<br>" +
                data.progressInfo.goal + "</div>";

            let progressPercent: number = (data.progressInfo.current / data.progressInfo.goal);
            data.progressInfo.percent = Math.floor(progressPercent * 100);
            data.progressInfo.progress = progressPercent;
            if (data.progressInfo.progress >= 1) {
                data.progressInfo.progress = 1;
            }

            let progressBar: any;
            switch (data.progressElement) {
                case this.commitmentProgressElement:
                    if (!this.commitmentProgressBar) {
                        this.commitmentProgressBar = this.createProgressBar({
                            nativeElement: data.progressElement.nativeElement,
                            color: data.progressInfo.color,
                            text: progressText
                        })
                    }
                    progressBar = this.commitmentProgressBar;
                    break;
                case this.purchaseProgressElement:
                    if (!this.purchaseProgressBar) {
                        this.purchaseProgressBar = this.createProgressBar({
                            nativeElement: data.progressElement.nativeElement,
                            color: data.progressInfo.color,
                            text: progressText
                        })
                    }
                    progressBar = this.purchaseProgressBar;
                    break;
            }

            this.zone.run(() => {
                progressBar.setText(progressText);
                progressBar.animate(data.progressInfo.progress);
            });
        }
    }

    private createProgressBar(data: { nativeElement: any, color: string, text: string }): any {
        return new ProgressBar.Circle(data.nativeElement, {
                color: data.color,
                trailColor: '#FFEA82',
                trailWidth: 2,
                duration: 1400,
                easing: 'bounce',
                strokeWidth: 6,
                text: {
                    value: data.text,
                    style: {
                        color: data.color,
                        position: 'absolute',
                        left: '50%',
                        top: '50%',
                        padding: 0,
                        margin: 0,
                        // You can specify styles which will be browser prefixed
                        transform: {
                            prefix: true,
                            value: 'translate(-50%, -50%)'
                        }
                    }
                }
            }
        );
    }

    public configureSalesTotalProgressBars(): void {
        this.configureCommitmentProgressBar();
        this.configurePurchasedProgressBar();
    }

    private getParticipantsContacts(): void {
        if (this.subMyListContacts) {
            this.subMyListContacts.stop();
            this.subMyListContacts = null;
        }
        this.subMyListContacts = this.subscribe(Constants.PUBLICATIONS.MY_LIST_CONTACTS, this.program._id, () => {
            console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MY_LIST_CONTACTS + " <><><>");
            this.autorun(() => this.zone.run(() => {
                let queryKey: string = "roles." + this.program._id;
                let query: any = {};
                query[queryKey] = {
                    $in: [Constants.ROLES.MEMBER]
                };
                let programParticipants: Array<Meteor.User> = Meteor.users.find(query).fetch();
                let participantIds: Array<string> = programParticipants.map((participant: Meteor.User) => {
                    return participant._id;
                });
                let participantsListContacts: Array<IMyListContact> = MyListContactsCollection.find({
                    userId: {$in: participantIds}
                }).fetch();
                //console.log("participantsListContacts: ", participantsListContacts);
                this.participantsListContacts = participantsListContacts;
                this.joinParticipantsData();
            }));
        });
    }

    private setVisibilityListener(): void {
        let self = this;
        let hidden, visibilityState, visibilityChange;

        if (typeof document.hidden !== "undefined") {
            hidden = "hidden";
            visibilityChange = "visibilitychange";
            visibilityState = "visibilityState";
        }

        let document_hidden = document[hidden];

        document.addEventListener(visibilityChange, function () {
            if (document_hidden != document[hidden]) {
                if (document[hidden]) {
                    // Document hidden
                    // console.log("document hidden");
                } else {
                    // Document shown
                    // console.log("document shown;");
                    self.setCountDownTimer();
                }

                document_hidden = document[hidden];
            }
        });
    }

    private startTimer(): void {
        let self = this;
        self.program.startTime = moment().toISOString();
        self.programInfoService.upsert(self.program, (error, programInfo: IProgramInfo) => {
            if (error) {
                console.error("Error saving program info: " + error.reason);
                new ToastMessenger().toast({
                    type: "error",
                    message: error.reason
                });
            } else {
                self.programInfoService.setProgramInfo(programInfo);
                self.program = self.programInfoService.getProgramInfo();
                self.duration = {
                    minutes: self.program.duration,
                    seconds: 0
                };
                self.setCountDownTimer();
            }
        });
    }

    private clearStartTime(): void {
        let self = this;
        self.program.startTime = null;
        self.clearTimer();
        self.programInfoService.upsert(self.program, (error, programInfo: IProgramInfo) => {
            if (error) {
                console.error("Error saving program info: " + error.reason);
                new ToastMessenger().toast({
                    type: "error",
                    message: error.reason
                });
            } else {
                self.zone.run(() => {
                    self.programInfoService.setProgramInfo(programInfo);
                    self.program = self.programInfoService.getProgramInfo();
                    self.duration = null;
                });
            }
        });
    }

    private toggleTimerPaused(): void {
        this.zone.run(() => {
            if (this.intervalTimer) {
                if (this.isTimerPaused) {
                    this.intervalTimer.resume();
                } else {
                    this.intervalTimer.pause();
                }
            }
            this.isTimerPaused = !this.isTimerPaused;
        });
    }

    private setCountDownTimer(): void {
        let self = this;
        if (self.program.startTime) {
            let startMoment = moment(self.program.startTime);
            let endMoment = startMoment.add(self.program.duration, "minutes");
            console.log("endMoment: ", endMoment.toISOString());

            self.clearTimer();

            let eventTime = endMoment.unix();
            let currentTime = moment().unix();
            let diffTime = eventTime - currentTime;
            let duration = moment.duration(diffTime * 1000, 'milliseconds');
            let interval = 1000;

            // if time to countdown
            if (diffTime > 0) {
                self.intervalTimer = new IntervalTimer({
                    name: "Event Interval Timer",
                    interval: 1000
                }, () => {
                    self.zone.run(() => {
                        let diff = duration.asMilliseconds() - interval;
                        if (diff < 0) {
                            self.clearTimer();
                            self.eventComplete();
                        } else {
                            duration = moment.duration(duration.asMilliseconds() - interval, 'milliseconds');

                            self.duration = {
                                days: moment.duration(duration).days(),
                                hours: moment.duration(duration).hours(),
                                minutes: moment.duration(duration).minutes(),
                                seconds: moment.duration(duration).seconds()
                            };
                            // console.log("duration: ", JSON.stringify(self.duration));
                        }
                    });
                });
                self.intervalTimer.start();
            } else {
                self.eventComplete();
            }
        }
    }

    private eventComplete(): void {
        let self = this;
        self.duration = {
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        };
    }

    private clearTimer(): void {
        if (this.intervalTimer) {
            this.intervalTimer.stop();
            this.intervalTimer = null;
        }
    }

    ionViewWillLeave() {
        this.clearTimer();
    }

    private joinParticipantsData(): void {
        let participantsData: Array<any> = [];
        if (this.participantsTotals && this.participants && this.participantsListContacts) {
            // this.participants.sort(this.sortParticipantsByTotals.bind(this));
            this.participants.forEach((participant: Meteor.User, index: number) => {
                let participantTotals: IParticipantTotals = this.participantsTotals.find(
                    (totals: IParticipantTotals) => totals._id === participant._id);

                let participantListContacts: Array<IMyListContact> = this.participantsListContacts.filter(
                    (contact: IMyListContact) => {
                        return contact.userId === participant._id;
                    }
                );

                let totals: ParticipantTotals = new ParticipantTotals(participantTotals);
                let eventPoints: IEventPoints = participant.profile.programs[this.program._id].eventPoints;
                let points: number = Number(eventPoints.contactListGoal || 0) +
                    Number(eventPoints.contactGoal || 0) + Number(eventPoints.salesGoalAward || 0);
                totals.points += points;
                totals.points += Number(this.program.eventPoints.registration || 0);

                participant["listContacts"] = participantListContacts.length;
                participant["totals"] = totals;
                if (!participant["totals"].pointsLastUpdated) {
                    participant["totals"].pointsLastUpdated = participant.profile.programs[this.program._id].eventPoints.updated;
                }

                participantsData.push(participant);
            });
        }
        this.zone.run(() => {
            participantsData.sort(this.sortDataByTotalPoints.bind(this));
            participantsData.forEach((participant: any, index: number) => {
                participant["rank"] = index + 1;
            });
            this.participantsData = participantsData;
        });

        if (this.program.competitionType === Constants.COMPETITION_TYPE.GROUP) {
            this.groupParticipants();
        }
    }

    private groupParticipants(): void {
        if (this.programGroups && this.participantsData) {
            this.programGroups.forEach((group: IProgramGroup) => {
                group.members = [];
                group["totals"] = new ParticipantTotals();
            });
            this.participantsData.forEach((participant: any) => {
                if (participant.profile.groups && participant.profile.groups[this.program._id]) {
                    let participantGroupId: string = participant.profile.groups[this.program._id];
                    let programGroup: any = this.programGroups.find((group: IProgramGroup) => {
                        return group._id === participantGroupId;
                    });
                    if (programGroup) {
                        if (participant.totals) {
                            if (programGroup.members.length === 0) {
                                programGroup.totals.pointsLastUpdated = participant.totals.pointsLastUpdated;
                            } else {
                                let groupPointsLastUpdated = moment(programGroup.totals.pointsLastUpdated);
                                let participantPointsLastUpdated = moment(participant.totals.pointsLastUpdated);
                                if (groupPointsLastUpdated.diff(participantPointsLastUpdated) > 0) {
                                    programGroup.totals.pointsLastUpdated = participant.totals.pointsLastUpdated;
                                }
                            }

                            programGroup.totals.points += participant.totals.points;
                            programGroup.totals.contacts += participant.totals.contacts;
                            programGroup.totals.commitments += participant.totals.commitments;
                            programGroup.totals.commitmentSales += participant.totals.commitmentSales;
                            programGroup.totals.purchases += participant.totals.purchases;
                            programGroup.totals.purchasedSales += participant.totals.purchasedSales;
                        }

                        programGroup.members.push(participant);
                    }
                }
            });
            this.programGroups = this.programGroups.sort(this.sortDataByTotalPoints.bind(this));
            this.zone.run(() => {
                this.programGroups.forEach((group: any, index: number) => {
                    group["rank"] = index + 1;
                });
            });
        }
    }

    private sortParticipantsByTotals(a, b): number {
        let idMap: Array<string> = this.participantsTotals.map((participantTotal: IParticipantTotals) => {
            return participantTotal._id;
        });
        let indexA = idMap.indexOf(a._id);
        let indexB = idMap.indexOf(b._id);
        if (indexA < indexB) {
            return -1;
        } else if (indexA > indexB) {
            return 1;
        } else {
            return 0;
        }
    }

    private sortDataByTotalPoints(a, b): number {
        let sortDirection: number = b.totals.points - a.totals.points;
        if (sortDirection === 0) {
            let aMoment = moment(a.totals.pointsLastUpdated);
            let bMoment = moment(b.totals.pointsLastUpdated);
            if (aMoment.diff(bMoment) > 0) {
                sortDirection = 1;
            } else if (aMoment.diff(bMoment) < 0) {
                sortDirection = -1;
            }
        }
        return sortDirection;
    }

    private getBackgroundColor(index: number): string {
        let backgroundColor: string;
        switch (index) {
            case 0:
                backgroundColor = "gold";
                break;
            case 1:
                backgroundColor = "silver";
                break;
            case 2:
                backgroundColor = "bronze";
                break;
            default:
                backgroundColor = "white";
        }
        return backgroundColor;
    }
}
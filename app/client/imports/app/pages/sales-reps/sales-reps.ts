import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {ToastMessenger} from "../../utils/ToastMessenger";
@Component({
    selector: "page-sales-reps",
    templateUrl: "sales-reps.html"
})
export class SalesRepsPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public salesReps:Array<Meteor.User>;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();

            if (this.user) {
                this.subscribe(Constants.PUBLICATIONS.SALES_REPS, () => {
                    console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.SALES_REPS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        var salesReps:Array<Meteor.User> = Meteor.users.find({
                            "profile.salesRepId": {$exists: true, $ne: Constants.EMPTY_STRING}
                        }).fetch();
                        console.log("salesReps: ", salesReps);
                        this.salesReps = salesReps;
                    }));
                });
            }
        }));
    }

    private confirmDeactivateSalesRep(salesRep:Meteor.User):void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-sales-reps.alerts.deactivateSalesRep.title"),
            subTitle: salesRep.profile.name.display,
            message: self.translate.instant("page-sales-reps.alerts.deactivateSalesRep.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: 'cancel'
            }, {
                text: self.translate.instant("page-sales-reps.alerts.deactivateSalesRep.confirmButton"),
                handler: () => {
                    self.deactivateSalesRep(salesRep);
                }
            }]
        });
        alert.present();
    }

    private deactivateSalesRep(salesRep:Meteor.User):void {
        var self = this;
        Meteor.call("deactivateSalesRep", {salesRepId: salesRep._id}, (error, result) => {
            if (error) {
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-sales-reps.errors.deactivateSalesRep"),
                    message: error.reason,
                    buttons: [self.translate.instant("general.ok")]
                });
                alert.present();
            } else {
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-sales-reps.toast.deactivateSalesRepSuccess")
                });
            }
        });
    }

    ionViewWillEnter() {
        Session.set(Constants.SESSION.SHOW_LEADERBOARD_MENU_ITEM, false);
    }

    ionViewWillLeave() {
        Session.set(Constants.SESSION.SHOW_LEADERBOARD_MENU_ITEM, true);
    }
}
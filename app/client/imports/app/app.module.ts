import {NgModule, ErrorHandler} from "@angular/core";
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular/es2015';
import {Storage} from '@ionic/storage';
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {CommonModule} from '@angular/common';
import {ResponsiveModule} from 'ng2-responsive';
import {TranslateModule, TranslateLoader} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {HttpClientModule, HttpClient} from "@angular/common/http";
import {Constants} from "../../../both/Constants";
import {AppComponent} from "./app.component";
import {HomePage} from "./pages/home/home";
import {LanguageSelectComponent} from "./components/language-select/language-select";
import {AboutPage} from "./pages/about/about";

// Login components
import {LoginPage} from "./pages/account/login/login";
import {LoginCardComponent} from "./pages/account/login/login-card/login-card";
import {CreateAccountCardComponent} from "./pages/account/login/create-account-card/create-account-card";
import {ForgotPasswordCardComponent} from "./pages/account/login/forgot-password-card/forgot-password-card";
import {PasswordResetCardComponent} from "./pages/account/login/password-reset-card/password-reset-card";
import {OauthProviderComponent} from "./pages/account/login/oauth/oauth-provider";

// Account management components
import {AccountMenuPage} from "./pages/account/account-menu/account-menu";
import {ChangePasswordPage} from "./pages/account/account-menu/change-password/change-password";
import {EditProfilePage} from "./pages/account/account-menu/edit-profile/edit-profile";
import {AddImageComponent} from "./components/add-image/add-image";

// Other pages
import {ProgramInfoService} from "./services/program-info.service";
import {ProgramTabsPage} from "./pages/program/program-tabs";
import {ProgramDetailsPage} from "./pages/program/tabs/program-details/program-details";
import {ProgramSalesPage} from "./pages/program/tabs/program-sales/program-sales";
import {ProgramScriptsPage} from "./pages/program/tabs/program-scripts/program-scripts";
import {ProgramManagersCardComponent} from "./components/program-managers-card/program-managers-card";
import {ProgramManagerItemComponent} from "./components/program-manager-item/program-manager-item";
import {ParticipantDetailsPage} from "./pages/program/tabs/program-sales/participant-details/participant-details";
import {ProgramKickoffComponent} from "./pages/program/tabs/program-kickoff/program-kickoff";
import {PhonePipe} from "./pipes/phone.pipe";
import {OrderCommitmentPage} from "./pages/program/tabs/program-sales/participant-details/order-commitment/order-commitment";
import {LeaderboardPage} from "./pages/leaderboard/leaderboard";
import {SalesRepsPage} from "./pages/sales-reps/sales-reps";
import {LandingPage} from "./pages/landing/landing";
import {PaginationComponent} from "./components/pagination-component/pagination-component";
import {AddProgramManagerModal} from "./modals/add-program-manager-modal/add-program-manager-modal";
import {SaveAsModalPage} from "./modals/save-as-modal/save-as-modal";
import {MomentPipe} from "./pipes/moment.pipe";
import {GroupManagerPage} from "./pages/program/tabs/program-details/group-manager/group-manager";
import {SelectGroupMembersModalPage} from "./modals/select-group-members-modal/select-group-members-modal";
import {EditGroupPage} from "./pages/program/tabs/program-details/group-manager/edit-group/edit-group";
import {PushNotificationModalPage} from "./modals/push-notification-modal/push-notification-modal";
import {ParticipantRosterPage} from "./pages/program/tabs/program-sales/participant-roster/participant-roster";
import {NewPagePage} from "./pages/newpage/newpage";

//Providers
import {ImageService} from "./utils/ImageService";
import {PurchaseCommitmentDiscrepancyPage} from "./pages/program/tabs/program-sales/purchase-commitment-discrepancy/purchase-commitment-discrepancy";

@NgModule({
    // Components(Pages), Pipes, Directive
    declarations: [
        AppComponent,
        HomePage,
        LanguageSelectComponent,
        AboutPage,
        NewPagePage,
        LoginPage,
        LoginCardComponent,
        CreateAccountCardComponent,
        ForgotPasswordCardComponent,
        PasswordResetCardComponent,
        OauthProviderComponent,
        AccountMenuPage,
        ChangePasswordPage,
        EditProfilePage,
        AddImageComponent,
        ProgramTabsPage,
        ProgramDetailsPage,
        ProgramKickoffComponent,
        ProgramSalesPage,
        ProgramScriptsPage,
        ProgramManagersCardComponent,
        ProgramManagerItemComponent,
        ParticipantDetailsPage,
        PhonePipe,
        OrderCommitmentPage,
        LeaderboardPage,
        SalesRepsPage,
        LandingPage,
        PaginationComponent,
        AddProgramManagerModal,
        SaveAsModalPage,
        MomentPipe,
        SaveAsModalPage,
        GroupManagerPage,
        SelectGroupMembersModalPage,
        EditGroupPage,
        PushNotificationModalPage,
        ParticipantRosterPage,
        PurchaseCommitmentDiscrepancyPage
    ],
    // Pages
    entryComponents: [
        AppComponent,
        HomePage,
        LoginPage,
        AboutPage,
        AccountMenuPage,
        ChangePasswordPage,
        EditProfilePage,
        ProgramTabsPage,
        ProgramDetailsPage,
        ProgramSalesPage,
        ProgramScriptsPage,
        ParticipantDetailsPage,
        OrderCommitmentPage,
        LeaderboardPage,
        SalesRepsPage,
        LandingPage,
        AddProgramManagerModal,
        SaveAsModalPage,
        GroupManagerPage,
        SelectGroupMembersModalPage,
        EditGroupPage,
        PushNotificationModalPage,
        ParticipantRosterPage,
        PurchaseCommitmentDiscrepancyPage
    ],
    // Providers
    providers: [
        ProgramInfoService,
        ImageService,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ],
    // Modules
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        CommonModule,
        ResponsiveModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        IonicModule.forRoot(AppComponent, {
            //// http://ionicframework.com/docs/v2/api/config/Config/
            mode: Constants.STYLE.MD,
            pageTransition: Constants.STYLE.MD,
            tabsPlacement: 'top',
            tabsHighlight: true,
            swipeBackEnabled: false
        }),
    ],
    // Main Component
    bootstrap: [IonicApp]
})
export class AppModule {
    constructor() {

    }
}

export function createTranslateLoader(http:HttpClient) {
    return new TranslateHttpLoader(http, '/i18n/', '.json');
}

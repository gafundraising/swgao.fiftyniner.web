import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, ViewController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {Constants} from "../../../../../both/Constants";
@Component({
    selector: "page-save-as-modal",
    templateUrl: "save-as-modal.html"
})
export class SaveAsModalPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public existingList:Array<any>;
    public selectedItemId:string;
    public itemTitle:string = Constants.EMPTY_STRING;
    public formGroup:FormGroup;
    public formControl:{
        title:AbstractControl
    };

    constructor(public nav:NavController,
                public params:NavParams,
                public viewCtrl:ViewController,
                public zone:NgZone,
                public translate:TranslateService,
                public fb:FormBuilder) {
        super();
    }

    ngOnInit() {
        this.existingList = this.params.data.existingList || [];
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));

        this.formGroup = this.fb.group({
            'title': [Constants.EMPTY_STRING, Validators.compose([
                // Validators.required
            ])]
        });

        this.formControl = {
            title: this.formGroup.controls['title']
        };
    }

    private clearSelected():void {
        this.selectedItemId = Constants.EMPTY_STRING;
    }

    private dismiss():void {
        this.viewCtrl.dismiss();
    }

    private save():void {
        let existingId:string;
        if (this.selectedItemId && !this.itemTitle) {
            existingId = this.selectedItemId;
        }
        let returnData:ISaveAsModalPageReturnData = {
            existingId: existingId,
            itemTitle: this.itemTitle
        };
        this.viewCtrl.dismiss(returnData);
    }
}

export interface ISaveAsModalPage {
    existingList:Array<any>
}

export interface ISaveAsModalPageReturnData {
    existingId:string,
    itemTitle:string
}
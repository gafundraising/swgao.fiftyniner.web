import {Component, OnInit, NgZone, Inject} from "@angular/core";
import {NavController, NavParams, ViewController, AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {ToastMessenger} from "../../utils/ToastMessenger";
@Component({
    selector: "page-add-program-manager-modal",
    templateUrl: "add-program-manager-modal.html"
})
export class AddProgramManagerModal extends MeteorComponent implements OnInit {
    public programManager:{
        programId?:string,
        email?:string,
        profile?:{
            name?:{
                given?:string,
                family?:string,
                display?:string
            }
        }
    } = {};
    public formGroup:FormGroup;
    public formControl:{
        givenName:AbstractControl,
        familyName:AbstractControl
    };


    constructor(public nav:NavController,
                public params:NavParams,
                public viewCtrl:ViewController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService,
                public fb:FormBuilder) {
        super();
    }

    ngOnInit() {
        this.formGroup = this.fb.group({
            'givenName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])],
            'familyName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])]
        });

        this.formControl = {
            givenName: this.formGroup.controls['givenName'],
            familyName: this.formGroup.controls['familyName']
        };

        Meteor.defer(() => {
            this.zone.run(() => {
                this.programManager = {
                    programId: Session.get(Constants.SESSION.PROGRAM_ID),
                    email: Constants.EMPTY_STRING,
                    profile: {
                        name: {
                            given: Constants.EMPTY_STRING,
                            family: Constants.EMPTY_STRING,
                            display: Constants.EMPTY_STRING
                        }
                    }
                };
                this.programManager.email = this.params.data.email;
            });
        });
    }

    private dismiss():void {
        this.viewCtrl.dismiss();
    }

    private inviteProgramManager():void {
        var self = this;
        if (this.formGroup.valid) {
            Meteor.call("inviteProgramManager", self.programManager, (error, result) => {
                if (error) {
                    console.error("inviteProgramManager() Error: ", error);
                    let alert = self.alertCtrl.create({
                        title: self.translate.instant("program-managers-card.errors.addProgramManager"),
                        message: error.reason || error.message || error,
                        buttons: [{text: self.translate.instant("general.ok")}]
                    });
                    alert.present();
                } else {
                    console.log("inviteProgramManager() result: ", result);
                    if (result.addedProgramManager) {
                        new ToastMessenger().toast({
                            type: "success",
                            title: self.translate.instant("program-managers-card.toasts.addedProgramManager"),
                            message: self.programManager.email
                        });

                        self.viewCtrl.dismiss();
                    }
                }
            });
        }
    }
}
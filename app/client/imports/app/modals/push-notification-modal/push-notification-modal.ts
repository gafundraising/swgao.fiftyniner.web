import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, ViewController, AlertController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {Constants} from "../../../../../both/Constants";
import {IPushNotification} from "../../../../../both/models/push-notification.model";
import {ToastMessenger} from "../../utils/ToastMessenger";
@Component({
    selector: "page-push-notification-modal",
    templateUrl: "push-notification-modal.html"
})
export class PushNotificationModalPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public notification:IPushNotification;
    public formGroup:FormGroup;
    public formControl:{
        title:AbstractControl,
        message:AbstractControl
    };

    constructor(public nav:NavController,
                public params:NavParams,
                public viewCtrl:ViewController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService,
                public fb:FormBuilder) {
        super();
    }

    ngOnInit() {
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));

        this.formGroup = this.fb.group({
            'title': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])],
            "message": [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])]
        });

        this.formControl = {
            title: this.formGroup.controls['title'],
            message: this.formGroup.controls['message']
        };

        this.notification = {
            title: Constants.EMPTY_STRING,
            message: Constants.EMPTY_STRING,
            action: Constants.PUSH_NOTIFICATION_ACTIONS.MESSAGE,
            userIds: this.params.data.userIds
        };
    }

    private dismiss():void {
        this.viewCtrl.dismiss();
    }

    private sendPushNotification():void {
        var self = this;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("sendPushNotification", {
            programId: self.params.data.programId,
            notification: self.notification
        }, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("Error sending push notification: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-push-notification-modal.errors.sendPushNotification"),
                    message: error.reason || error.message || error,
                    buttons: [{text: self.translate.instant("general.ok")}]
                });
                alert.present();
            } else {
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-push-notification-modal.toasts.sentPushNotification")
                });
                self.viewCtrl.dismiss();
            }
        });
    }
}

export interface IPushNotificationModalPage {
    type:string,
    userIds:Array<string>,
    name:string,
    programId:string
}

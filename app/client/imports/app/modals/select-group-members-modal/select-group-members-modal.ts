import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, ViewController, NavParams} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
@Component({
    selector: "page-select-group-members-modal",
    templateUrl: "select-group-members-modal.html"
})
export class SelectGroupMembersModalPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public unassignedParticipants:Array<Meteor.User>;

    constructor(public nav:NavController,
                public viewCtrl:ViewController,
                public params:NavParams,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.unassignedParticipants = this.params.data.unassignedParticipants;
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));
    }

    private dismiss():void {
        this.viewCtrl.dismiss();
    }

    private done():void {
        let selectedParticipantIds:Array<string> = [];
        this.unassignedParticipants.forEach((participant:Meteor.User) => {
            if (participant["isSelected"]) {
                selectedParticipantIds.push(participant._id);
            }
        });
        this.viewCtrl.dismiss({selectedParticipantIds: selectedParticipantIds});
    }
}
import {Constants} from "../../both/Constants";
Meteor.startup(function() {
    Session.setDefault(Constants.SESSION.SORT_PROGRAMS_DESCENDING, true);
    if (Meteor.settings.public["environment"] === Constants.ENVIRONMENT.PRODUCTION) {
        console.log = () => {
        };
    }
});
# Build
### Requirements
* Node.js
* Meteor

### Steps
* Install the dependencies

```
cd app/
npm install
```

* Build the app

```
npm run build
```

* Output will be `build/windows/server-bundle.tar.gz`

# Deployment

* Transfer the `server-bundle.tar.gz` to the GAWEBD01 `C:\Apps\Installation\GA\Portal\Web\PaymentApp\PublicSite`
* Remote desktop to GAWEBD01 and open a file explorer to the location above.
* Stop `Node.js` in the application pool in IIS
* Unzip the bundle
    * Right-click the file > 7-zip > Extrat Here
    * This will produce the file `server-bundle.tar`
* Extract the contents of the bundle
    * Double-click `server-bundle.tar` to open 7-zip
    * Double-click the `bundle` directory to open
    * Extract the contents of the `bundle` directory to the current path.  Should be `C:\Apps\Installation\GA\Portal\Web\PaymentApp\PublicSite\`

* Install the server dependencies

```
cd programs/server/
npm install
```

* Set the web.config
    * Copy `config\development\web.config` to the site root.
    
* Start the `Node.js` application pool in IIS

* Create zip file to be deployed in TEST and PROD
    * Select the following files and folders
        * config
        * programs
        * server
        * node_version.txt
        * main.js
        * README
        * star.json
    * Right-click > 7-zip > Add to "PaymentApp.7z"
    * Rename file to add the version 
        * Example: PaymentApp-v2_X_X_X
        * Version number can be found in the `config/development` in the `settings.json` or `web.config` files
        

# TEST and PROD

* Stop the `Node.js` application pool in IIS
* Open file explorer and navigate to `C:\Apps\Installation\GA\Portal\Web\PaymentApp\PublicSite`
* Open a new file explorer and navigate to `\\gawebd01\Apps\Installation\GA\Portal\Web\PaymentApp\PublicSite`
* Copy the PaymentApp-version bundle that was created.
* Unzip the file
    * Right-click > 7-zip > Extract Here
* Copy the `web.config` from the appropriate environment from the `config` folder
* Start the `Node.js` application pool in IIS
* Verify the deployment
    * Open Google Chrome browser 
        * Navigate to the appropriate URL for the environment being deployed `https://test.gametime.gafundraising.com`
        * Open the "About" page from the navigation menu
        * Verify the version number at the bottom of the page

